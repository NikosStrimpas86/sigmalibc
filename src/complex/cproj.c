#include <complex.h>
#include <math.h>

double complex cproj(double complex z)
{
    if (isinf(creal(z)) || isinf(cimag(z))) {
        return __SOLC_CMPLX(INFINITY, copysign(0.0, cimag(z)));
    }
    return z;
}

float complex cprojf(float complex z)
{
    if (isinf(crealf(z)) || isinf(cimag(z))) {
        return __SOLC_CMPLXF(INFINITY, copysignf(0.0f, cimagf(z)));
    }
    return z;
}

long double complex cprojl(long double complex z)
{
    if (isinf(creall(z)) || isinf(cimagl(z))) {
        return __SOLC_CMPLXL(INFINITY, copysignl(0.0l, cimagl(z)));
    }
    return z;
}
