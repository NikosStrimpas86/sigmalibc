#include <complex.h>
#include <complex_types.h>

double creal(double complex z)
{
    union _Double_complex_parts captured = { .number = z };
    return captured.as_array[0];
}

float crealf(float complex z)
{
    union _Float_complex_parts captured = { .number = z };
    return captured.as_array[0];
}

long double creall(long double complex z)
{
    union _Ldouble_complex_parts captured = { .number = z };
    return captured.as_array[0];
}
