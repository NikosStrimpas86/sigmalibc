#include <complex.h>
#include <complex_types.h>

double cimag(double complex z)
{
    union _Double_complex_parts captured = { .number = z };
    return captured.as_array[1];
}

float cimagf(float complex z)
{
    union _Float_complex_parts captured = { .number = z };
    return captured.as_array[1];
}

long double cimagl(long double complex z)
{
    union _Ldouble_complex_parts captured = { .number = z };
    return captured.as_array[1];
}
