#include <stdlib.h>

div_t div(int numer, int denom)
{
    div_t result_struct;
    result_struct.quot = numer / denom;
    result_struct.rem = numer % denom;
    return result_struct;
}

ldiv_t ldiv(long int numer, long int denom)
{
    ldiv_t result_struct;
    result_struct.quot = numer / denom;
    result_struct.rem = numer % denom;
    return result_struct;
}

lldiv_t lldiv(long long int numer, long long int denom)
{
    lldiv_t result_struct;
    result_struct.quot = numer / denom;
    result_struct.rem = numer % denom;
    return result_struct;
}
