#include <stdlib.h>

void* (bsearch)(const void* key, const void* base, size_t nmemb, size_t size, int (*compar)(const void*, const void*))
{
    const unsigned char* base_slice = (const unsigned char*)base;
    const unsigned char* middle_member;
    int comparison;

    while (nmemb > 0) {
        middle_member = base_slice + size * (nmemb / 2);
        comparison = compar(key, (const void*)middle_member);
        if (comparison < 0) {
            /* Search the lower subarray */
            nmemb /= 2;
        } else if (comparison > 0) {
            /* Search the higher subarray */
            base_slice = middle_member + size;
            nmemb -= nmemb / 2 + 1;
        } else {
            return (void*)middle_member;
        }
    }
    return NULL;
}
