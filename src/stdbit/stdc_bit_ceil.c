#include <stdbit.h>
#include <limits.h>

unsigned char stdc_bit_ceiluc(unsigned char value)
{
    unsigned char i;
    for (i = 0; i < UCHAR_WIDTH; i++) {
        if ((1 << i) >= value)
            break;
    }
    return (unsigned char)1 << i;
}

unsigned short stdc_bit_ceilus(unsigned short value)
{
    unsigned short i;
    for (i = 0; i < USHRT_WIDTH; i++) {
        if ((1 << i) >= value)
            break;
    }
    return (unsigned short)1 << i;
}

unsigned int stdc_bit_ceilui(unsigned int value)
{
    unsigned int i;
    for (i = 0u; i < UINT_WIDTH; i++) {
        if ((1u << i) >= value)
            break;
    }
    return 1u << i;
}

unsigned long stdc_bit_ceilul(unsigned long value)
{
    unsigned long i;
    for (i = 0ul; i < ULONG_WIDTH; i++) {
        if ((1ul << i) >= value)
            break;
    }
    return 1ul << i;
}

unsigned long long stdc_bit_ceilull(unsigned long long value)
{
    unsigned long long i;
    for (i = 0ull; i < ULLONG_WIDTH; i++) {
        if ((1ull << i) >= value)
            break;
    }
    return 1ull << i;
}

