#include <limits.h>
#include <stdbit.h>

/*
 * TODO: Verify the expected behaviour
 */

int stdc_first_leading_zerouc(unsigned char value)
{
    if (value == UCHAR_MAX)
        return 0;
    return stdc_leading_onesuc(value) + 1;
}

int stdc_first_leading_zerous(unsigned short value)
{
    if (value == USHRT_MAX)
        return 0;
    return stdc_leading_onesus(value) + 1;
}

int stdc_first_leading_zeroui(unsigned int value)
{
    if (value == UINT_MAX)
        return 0;
    return stdc_leading_onesui(value) + 1;
}

int stdc_first_leading_zeroul(unsigned long value)
{
    if (value == ULONG_MAX)
        return 0;
    return stdc_leading_onesul(value) + 1;
}

int stdc_first_leading_zeroull(unsigned long long value)
{
    if (value == ULLONG_MAX)
        return 0;
    return stdc_leading_onesull(value) + 1;
}

