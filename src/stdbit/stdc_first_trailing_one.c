#include <stdbit.h>

int stdc_first_trailing_oneuc(unsigned char value)
{
    if (value == 0)
        return 0;
    return stdc_trailing_zerosuc(value) + 1;
}

int stdc_first_trailing_oneus(unsigned short value)
{
    if (value == 0)
        return 0;
    return stdc_trailing_zerosus(value) + 1;
}

int stdc_first_trailing_oneui(unsigned int value)
{
    if (value == 0u)
        return 0;
    return stdc_trailing_zerosui(value) + 1;
}

int stdc_first_trailing_oneul(unsigned long value)
{
    if (value == 0ul)
        return 0;
    return stdc_trailing_zerosul(value) + 1;
}

int stdc_first_trailing_oneull(unsigned long long value)
{
    if (value == 0ull)
        return 0;
    return stdc_trailing_zerosull(value) + 1;
}
