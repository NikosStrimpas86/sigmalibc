#include <limits.h>
#include <stdbit.h>

int stdc_bit_widthuc(unsigned char value)
{
    return (int)(sizeof(value) * CHAR_BIT) - stdc_leading_zerosuc(value);
}

int stdc_bit_widthus(unsigned short value)
{
    return (int)(sizeof(value) * CHAR_BIT) - stdc_leading_zerosus(value);
}

int stdc_bit_widthui(unsigned int value)
{
    return (int)(sizeof(value) * CHAR_BIT) - stdc_leading_zerosui(value);
}

int stdc_bit_widthul(unsigned long value)
{
    return (int)(sizeof(value) * CHAR_BIT) - stdc_leading_zerosul(value);
}

int stdc_bit_widthull(unsigned long long value)
{
    return (int)(sizeof(value) * CHAR_BIT) - stdc_leading_zerosull(value);
}
