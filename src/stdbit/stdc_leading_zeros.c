#include <limits.h>
#include <stdbit.h>

/*
 * TODO: Implement more efficient algorithms for common cases.
 *       For unsigned int, unsigned long and unsigned long long it would
 *       be nice to use intrinsics.
 */

int stdc_leading_zerosuc(unsigned char value)
{
#if defined(__has_builtin) && __has_builtin(__builtin_clz)
    if (value == (unsigned char)0)
        return sizeof(unsigned char) * CHAR_BIT;
    return __builtin_clz(value) - (int)(sizeof(unsigned int) * CHAR_BIT - (sizeof(unsigned char) * CHAR_BIT));
#else
    for (int i = (sizeof(unsigned char) * CHAR_BIT) - 1; i >= 0; i--) {
        if (value & (1 << i)) {
            return (sizeof(unsigned char) * CHAR_BIT - i - 1);
        }
    }
    return sizeof(unsigned char) * CHAR_BIT;
#endif
}

int stdc_leading_zerosus(unsigned short value)
{
#if defined(__has_builtin) && __has_builtin(__builtin_clz)
    if (value == (unsigned short)0)
        return sizeof(unsigned char) * CHAR_BIT;
    return __builtin_clz(value) - (int)(sizeof(unsigned int) * CHAR_BIT - (sizeof(unsigned short) * CHAR_BIT));
#else
    for (int i = (sizeof(unsigned short) * CHAR_BIT) - 1; i >= 0; i--) {
        if (value & (1 << i)) {
            return (sizeof(unsigned short) * CHAR_BIT - i - 1);
        }
    }
    return sizeof(unsigned short) * CHAR_BIT;
#endif
}

int stdc_leading_zerosui(unsigned int value)
{
#if defined(__has_builtin) && __has_builtin(__builtin_clz)
    if (value == 0u)
        return sizeof(unsigned int) * CHAR_BIT;
    return __builtin_clz(value);
#else
#    if UINT_WIDTH == 32
    if (value == 0u)
        return sizeof(unsigned int) * CHAR_BIT;

    int leading = 0;
    if ((value & 0xffff0000u) == 0u) {
        leading += 16;
        value <<= 16u;
    }
    if ((value & 0xff000000u) == 0u) {
        leading += 8;
        value <<= 8u;
    }
    if ((value & 0xf0000000u) == 0u) {
        leading += 4;
        value <<= 4u;
    }
    if ((value & 0xc0000000u) == 0u) {
        leading += 2;
        value <<= 2u;
    }
    if ((value & 0x80000000u) == 0u) {
        leading += 1;
    }
    return leading;
#    else
    for (int i = (sizeof(unsigned int) * CHAR_BIT) - 1; i >= 0; i--) {
        if (value & (1u << i)) {
            return (sizeof(unsigned int) * CHAR_BIT - i - 1);
        }
    }
    return sizeof(unsigned int) * CHAR_BIT;
#    endif
#endif
}

int stdc_leading_zerosul(unsigned long value)
{
#if defined(__has_builtin) && __has_builtin(__builtin_clzl)
    if (value == 0ul)
        return sizeof(unsigned long) * CHAR_BIT;
    return __builtin_clzl(value);
#else
#    if ULONG_WIDTH == 32
    if (value == 0ul)
        return sizeof(unsigned long) * CHAR_BIT;

    int leading = 0;
    if ((value & 0xffff0000ul) == 0ul) {
        leading += 16;
        value <<= 16ul;
    }
    if ((value & 0xff000000ul) == 0ul) {
        leading += 8;
        value <<= 8ul;
    }
    if ((value & 0xf0000000ul) == 0ul) {
        leading += 4;
        value <<= 4ul;
    }
    if ((value & 0xc0000000ul) == 0ul) {
        leading += 2;
        value <<= 2ul;
    }
    if ((value & 0x80000000ul) == 0ul) {
        leading += 1;
    }
    return leading;
#    elif ULONG_WIDTH == 64
    if (value == 0ul)
        return sizeof(unsigned long) * CHAR_BIT;

    int leading;
    if ((value & 0xffffffff00000000ul) == 0ul) {
        leading += 32;
        value <<= 32ul;
    }
    if ((value & 0xffff000000000000ul) == 0ul) {
        leading += 16;
        value <<= 16ul;
    }
    if ((value & 0xff00000000000000ul) == 0ul) {
        leading += 8;
        value <<= 8ul;
    }
    if ((value & 0xf000000000000000ul) == 0ul) {
        leading += 4;
        value <<= 4ul;
    }
    if ((value & 0xc000000000000000ul) == 0ul) {
        leading += 2;
        value <<= 2ul;
    }
    if ((value & 0x8000000000000000ul) == 0ul) {
        leading += 1;
    }
    return leading;
#    else
    for (int i = (sizeof(unsigned long) * CHAR_BIT) - 1; i >= 0; i--) {
        if (value & (1ul << i)) {
            return (sizeof(unsigned long) * CHAR_BIT - i - 1);
        }
    }
    return sizeof(unsigned long) * CHAR_BIT;
#    endif
#endif
}

int stdc_leading_zerosull(unsigned long long value)
{
#if defined(__has_builtin) && __has_builtin(__builtin_clzll)
    if (value == 0ull)
        return sizeof(unsigned long long) * CHAR_BIT;
    return __builtin_clzll(value);
#else
#    if ULLONG_WIDTH == 64
    if (value == 0ull)
        return sizeof(unsigned long long) * CHAR_BIT;

    int leading;
    if ((value & 0xffffffff00000000ull) == 0ull) {
        leading += 32;
        value <<= 32ull;
    }
    if ((value & 0xffff000000000000ull) == 0ull) {
        leading += 16;
        value <<= 16ull;
    }
    if ((value & 0xff00000000000000ull) == 0ull) {
        leading += 8;
        value <<= 8ull;
    }
    if ((value & 0xf000000000000000ull) == 0ull) {
        leading += 4;
        value <<= 4ull;
    }
    if ((value & 0xc000000000000000ull) == 0ull) {
        leading += 2;
        value <<= 2ull;
    }
    if ((value & 0x8000000000000000ull) == 0ull) {
        leading += 1;
    }
    return leading;
#    else
    for (int i = (sizeof(unsigned long long) * CHAR_BIT) - 1; i >= 0; i--) {
        if (value & (1ull << i)) {
            return (sizeof(unsigned long long) * CHAR_BIT - i - 1);
        }
    }
    return sizeof(unsigned long long) * CHAR_BIT;
#    endif
#endif
}
