#include <limits.h>
#include <stdbit.h>

int stdc_count_onesuc(unsigned char value)
{
#if defined(__has_builtin) && __has_builtin(__builtin_popcount)
    return __builtin_popcount(value);
#else
    int counter = 0;
    unsigned char const width = sizeof(unsigned char) * CHAR_BIT;
    for (unsigned char i = 0; i < width; i++) {
        if (value & ((unsigned char)1 << i))
            counter++;
    }

    return counter;
#endif
}

int stdc_count_onesus(unsigned short value)
{
#if defined(__has_builtin) && __has_builtin(__builtin_popcount)
    return __builtin_popcount(value);
#else
    int counter = 0;
    unsigned short const width = sizeof(unsigned short) * CHAR_BIT;
    for (unsigned short i = 0; i < width; i++) {
        if (value & ((unsigned short)1 << i))
            counter++;
    }

    return counter;
#endif
}

int stdc_count_onesui(unsigned int value)
{
#if defined(__has_builtin) && __has_builtin(__builtin_popcount)
    return __builtin_popcount(value);
#else
    int counter = 0;
    unsigned int const width = sizeof(unsigned int) * CHAR_BIT;
    for (unsigned int i = 0u; i < width; i++) {
        if (value & (1u << i))
            counter++;
    }

    return counter;
#endif
}

int stdc_count_onesul(unsigned long value)
{
#if defined(__has_builtin) && __has_builtin(__builtin_popcountl)
    return __builtin_popcountl(value);
#else
    int counter = 0;
    unsigned long const width = sizeof(unsigned long) * CHAR_BIT;
    for (unsigned long i = 0ul; i < width; i++) {
        if (value & (1ul << i))
            counter++;
    }

    return counter;
#endif
}

int stdc_count_onesull(unsigned long long value)
{
#if defined(__has_builtin) && __has_builtin(__builtin_popcountll)
    return __builtin_popcountll(value);
#else
    int counter = 0;
    unsigned long long const width = sizeof(unsigned long long) * CHAR_BIT;
    for (unsigned long long i = 0ull; i < width; i++) {
        if (value & (1ull << i))
            counter++;
    }

    return counter;
#endif
}
