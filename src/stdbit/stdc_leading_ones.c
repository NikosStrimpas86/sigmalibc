#include <stdbit.h>
#include <limits.h>

int stdc_leading_onesuc(unsigned char value)
{
#if UCHAR_WIDTH == 8
    unsigned char comp = (~value) & 0xff;
    return stdc_leading_zerosuc(comp);
#else
    return stdc_leading_zerosuc(~value);
#endif
}

int stdc_leading_onesus(unsigned short value)
{
#if USHRT_WIDTH == 16
    unsigned short comp = (~value) & 0xffff;
    return stdc_leading_zerosus(comp);
#else
    return stdc_leading_zerosus(~value);
#endif
}

int stdc_leading_onesui(unsigned int value)
{
    return stdc_leading_zerosui(~value);
}

int stdc_leading_onesul(unsigned long value)
{
    return stdc_leading_zerosul(~value);
}

int stdc_leading_onesull(unsigned long long value)
{
    return stdc_leading_zerosull(~value);
}
