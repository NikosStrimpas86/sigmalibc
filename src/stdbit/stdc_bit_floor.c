#include <stdbit.h>
#include <limits.h>

unsigned char stdc_bit_flooruc(unsigned char value)
{
    if (value == 0)
        return 0;
    return ((unsigned char)1) << (sizeof(unsigned char) * CHAR_BIT - 1 - stdc_leading_zerosuc(value));
}

unsigned short stdc_bit_floorus(unsigned short value)
{
    if (value == 0)
        return 0;
    return ((unsigned short)1) << (sizeof(unsigned short) * CHAR_BIT - 1 - stdc_leading_zerosus(value));
}

unsigned int stdc_bit_floorui(unsigned int value)
{
    if (value == 0u)
        return 0u;
    return 1u << (sizeof(unsigned int) * CHAR_BIT - 1 - stdc_leading_zerosui(value));
}

unsigned long stdc_bit_floorul(unsigned long value)
{
    if (value == 0ul)
        return 0ul;
    return 1ul << (sizeof(unsigned long) * CHAR_BIT - 1 - stdc_leading_zerosul(value));
}

unsigned long long stdc_bit_floorull(unsigned long long value)
{
    if (value == 0ull)
        return 0ull;
    return 1ull << (sizeof(unsigned long long) * CHAR_BIT - 1 - stdc_leading_zerosull(value));
}

