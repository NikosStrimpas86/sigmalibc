#include <limits.h>
#include <stdbit.h>

int stdc_trailing_zerosuc(unsigned char value)
{
#if defined(__has_builtin) && __has_builtin(__builtin_ctz)
    if (value == (unsigned char)0)
        return sizeof(unsigned char) * CHAR_BIT;
    return __builtin_ctz(value);
#else
    for (int i = 0; i < (int)(sizeof(unsigned char) * CHAR_BIT); i++) {
        if (value & (1 << i)) {
            return i;
        }
    }
    return sizeof(unsigned char) * CHAR_BIT;
#endif
}

int stdc_trailing_zerosus(unsigned short value)
{
#if defined(__has_builtin) && __has_builtin(__builtin_ctz)
    if (value == (unsigned short)0)
        return sizeof(unsigned short) * CHAR_BIT;
    return __builtin_ctz(value);
#else
    for (int i = 0; i < (int)(sizeof(unsigned short) * CHAR_BIT); i++) {
        if (value & (1 << i)) {
            return i;
        }
    }
    return sizeof(unsigned short) * CHAR_BIT;
#endif
}

int stdc_trailing_zerosui(unsigned int value)
{
#if defined(__has_builtin) && __has_builtin(__builtin_ctz)
    if (value == 0u)
        return sizeof(unsigned int) * CHAR_BIT;
    return __builtin_ctz(value);
#else
#    if UINT_WIDTH == 32
    if (value == 0u)
        return sizeof(unsigned int) * CHAR_BIT;

    int trailing = 0;
    if ((value & 0x0000ffffu) == 0u) {
        trailing += 16;
        value >>= 16u;
    }
    if ((value & 0x000000ffu) == 0u) {
        trailing += 8;
        value >>= 8u;
    }
    if ((value & 0x0000000fu) == 0u) {
        trailing += 4;
        value >>= 4u;
    }
    if ((value & 0x00000003u) == 0u) {
        trailing += 2;
        value >>= 2u;
    }
    if ((value & 0x00000001u) == 0u) {
        trailing += 1;
    }
    return trailing;
#    else
    for (int i = 0; i < (int)(sizeof(unsigned int) * CHAR_BIT); i++) {
        if (value & (1u << i)) {
            return i;
        }
    }
    return sizeof(unsigned int) * CHAR_BIT;
#    endif
#endif
}

int stdc_trailing_zerosul(unsigned long value)
{
#if defined(__has_builtin) && __has_builtin(__builtin_ctzl)
    if (value == 0ul)
        return sizeof(unsigned long) * CHAR_BIT;
    return __builtin_ctzl(value);
#else
#    if ULONG_WIDTH == 32
    if (value == 0ul)
        return sizeof(unsigned long) * CHAR_BIT;

    int trailing = 0;
    if ((value & 0x0000fffful) == 0ul) {
        trailing += 16;
        value >>= 16ul;
    }
    if ((value & 0x000000fful) == 0ul) {
        trailing += 8;
        value >>= 8ul;
    }
    if ((value & 0x0000000ful) == 0ul) {
        trailing += 4;
        value >>= 4ul;
    }
    if ((value & 0x00000003ul) == 0ul) {
        trailing += 2;
        value >>= 2ul;
    }
    if ((value & 0x00000001ul) == 0ul) {
        trailing += 1;
    }
    return trailing;
#    elif ULONG_WIDTH == 64
    if (value == 0ul)
        return sizeof(unsigned long) * CHAR_BIT;

    int trailing = 0;
    if ((value & 0x00000000fffffffful) == 0ul) {
        trailing += 32;
        value >>= 32ul;
    }
    if ((value & 0x000000000000fffful) == 0ul) {
        trailing += 16;
        value >>= 16ul;
    }
    if ((value & 0x00000000000000fful) == 0ul) {
        trailing += 8;
        value >>= 8ul;
    }
    if ((value & 0x000000000000000ful) == 0ul) {
        trailing += 4;
        value >>= 4ul;
    }
    if ((value & 0x0000000000000003ul) == 0ul) {
        trailing += 2;
        value >>= 2ul;
    }
    if ((value & 0x0000000000000001ul) == 0ul) {
        trailing += 1;
    }
    return trailing;
#    else
    for (int i = 0; i < (int)(sizeof(unsigned long) * CHAR_BIT); i++) {
        if (value & (1ul << i)) {
            return i;
        }
    }
    return sizeof(unsigned long) * CHAR_BIT;
#    endif
#endif
}

int stdc_trailing_zerosull(unsigned long long value)
{
#if defined(__has_builtin) && __has_builtin(__builtin_ctzll)
    if (value == 0ull)
        return sizeof(unsigned long long) * CHAR_BIT;
    return __builtin_ctzll(value);
#else
#    if ULLONG_WIDTH == 64
    if (value == 0ull)
        return sizeof(unsigned long long) * CHAR_BIT;

    int trailing = 0;
    if ((value & 0x00000000ffffffffull) == 0ull) {
        trailing += 32;
        value >>= 32ull;
    }
    if ((value & 0x000000000000ffffull) == 0ull) {
        trailing += 16;
        value >>= 16ull;
    }
    if ((value & 0x00000000000000ffull) == 0ull) {
        trailing += 8;
        value >>= 8ull;
    }
    if ((value & 0x000000000000000full) == 0ull) {
        trailing += 4;
        value >>= 4ul;
    }
    if ((value & 0x0000000000000003ull) == 0ull) {
        trailing += 2;
        value >>= 2ull;
    }
    if ((value & 0x0000000000000001ull) == 0ull) {
        trailing += 1;
    }
    return trailing;
#    else
    for (int i = 0; i < (int)(sizeof(unsigned long long) * CHAR_BIT); i++) {
        if (value & (1ull << i)) {
            return i;
        }
    }
    return sizeof(unsigned long long) * CHAR_BIT;
#    endif
#endif
}
