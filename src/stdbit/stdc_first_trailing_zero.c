#include <stdbit.h>
#include <limits.h>

int stdc_first_trailing_zerouc(unsigned char value)
{
    if (value == UCHAR_MAX)
        return 0;
    return stdc_trailing_onesuc(value) + 1;
}

int stdc_first_trailing_zerous(unsigned short value)
{
    if (value == USHRT_MAX)
        return 0;
    return stdc_trailing_onesus(value) + 1;
}

int stdc_first_trailing_zeroui(unsigned int value)
{
    if (value == UINT_MAX)
        return 0;
    return stdc_trailing_onesui(value) + 1;
}

int stdc_first_trailing_zeroul(unsigned long value)
{
    if (value == ULONG_MAX)
        return 0;
    return stdc_trailing_onesul(value) + 1;
}

int stdc_first_trailing_zeroull(unsigned long long value)
{
    if (value == ULLONG_MAX)
        return 0;
    return stdc_trailing_onesull(value) + 1;
}
