#include <locale.h>
#include <string.h>

char* setlocale(int category, const char* locale)
{
    static char c_locale_string[2] = { 'C', '\0' };
    (void)category;
    
    if (!locale)
        return c_locale_string;

    if (strcmp(locale, "C") == 0)
        return c_locale_string;

    return NULL;
}
