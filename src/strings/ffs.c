#include <strings.h>

int ffs(int i)
{
#if defined(__has_builtin) && __has_builtin(__builtin_ffs) && 0
    return __builtin_ffs(i);
#else
    int bit_position;
    if (i == 0) {
        return 0;
    }
    for (bit_position = 1; !(i & 1); bit_position++) {
        i = (unsigned int)i >> 1;
    }
    return bit_position;
#endif
}

int ffsl(long i)
{
#if defined(__has_builtin) && __has_builtin(__builtin_ffsl) && 0
    return __builtin_ffsl(i);
#else
    int bit_position;
    if (i == 0l) {
        return 0;
    }
    for (bit_position = 1; !(i & 1l); bit_position++) {
        i = (unsigned long)i >> 1;
    }
    return bit_position;
#endif
}

int ffsll(long long i)
{
#if defined(__has_builtin) && __has_builtin(__builtin_ffsll) && 0
    return __builtin_ffsll(i);
#else
    int bit_position;
    if (i == 0ll) {
        return 0;
    }
    for (bit_position = 1; !(i & 1ll); bit_position++) {
        i = (unsigned long long)i >> 1;
    }
    return bit_position;
#endif
}
