#include <string.h>
#include <strings.h>

void bzero(void* s, size_t n)
{
    memset(s, 0, n);
}

int bcmp(void const* s1, void const* s2, size_t n)
{
    return memcmp(s1, s2, n);
}

void bcopy(void const* s1, void* s2, size_t n)
{
    memmove(s2, s1, n);
}

char* index(char const* s, int c)
{
    return (strchr)(s, c);
}

char* rindex(char const* s, int c)
{
    return (strrchr)(s, c);
}
