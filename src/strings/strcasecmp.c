#include <strings.h>
#include <ctype.h>

static inline char ignore_case(char c)
{
    if (isalpha(c)) {
        return (char)tolower(c);
    }
    return c;
}

int strcasecmp(char const* s1, char const* s2)
{
    size_t i = 0;
    for (; ignore_case(s1[i]) == ignore_case(s2[i]); i++) {
        if (s1[i] == 0) {
            return 0;
        }
    }
    return ignore_case(s1[i]) < ignore_case(s2[i]) ? -1 : 1;
}
