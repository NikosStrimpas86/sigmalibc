#include <strings.h>

int fls(int i)
{
    int bit_position;
    if (i == 0) {
        return 0;
    }
    for (bit_position = 1; i != 1; bit_position++) {
        i = (unsigned int)i >> 1;
    }

    return bit_position;
}

int flsl(long i)
{
    int bit_position;
    if (i == 0l) {
        return 0;
    }
    for (bit_position = 1; i != 1l; bit_position++) {
        i = (unsigned long)i >> 1;
    }

    return bit_position;
}

int flsll(long long i)
{
    int bit_position;
    if (i == 0ll) {
        return 0;
    }
    for (bit_position = 1; i != 1ll; bit_position++) {
        i = (unsigned long long)i >> 1;
    }

    return bit_position;
}
