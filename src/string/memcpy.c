#include <string.h>

void* memcpy(void* restrict s1, const void* restrict s2, size_t n)
{
    if (s1 == s2) {
        return s1;
    }
    unsigned char* s1_object_memory_chunk = (unsigned char*) s1;
    unsigned char const* s2_object_memory_chunk = (unsigned char const*) s2;
    for (size_t i = 0; i < n; i++) {
        *s1_object_memory_chunk = *s2_object_memory_chunk++;
        s1_object_memory_chunk++;
    }
    return s1;
}
