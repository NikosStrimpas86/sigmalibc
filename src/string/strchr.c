#include <string.h>

char* (strchr)(const char* s, int c)
{
    size_t length = strlen(s);
    for (size_t i = 0; i <= length; i++) {
        if (s[i] == (const char)c) {
            return (char*)(&s[i]);
        }
    }
    return NULL;
}
