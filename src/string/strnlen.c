#include <string.h>

size_t strnlen(const char* s, size_t maxlen)
{
    const char* iterator = s;
    for (; (maxlen--) && (*iterator != '\0'); iterator++);

    return iterator - s;
}
