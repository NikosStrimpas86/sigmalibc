#include <string.h>

void* memrchr(void const* s, int c, size_t n)
{
    unsigned char const* buf = s;
    unsigned char ch = (unsigned char)c;
    for (; n != 0; n--) {
        unsigned char const* position = buf + n - 1;
        if (*position == ch)
            return (unsigned char*)position;
    }

    return NULL;
}
