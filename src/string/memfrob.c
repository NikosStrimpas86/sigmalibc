#include <string.h>

void* memfrob(void* s, size_t n)
{
    char* ds = (char*)s;
    for (size_t i = 0; i < n; i++)
        ds[i] ^= 42;

    return s;
}
