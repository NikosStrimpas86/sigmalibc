#include <string.h>

int strcmp(const char* s1, const char* s2)
{
    for (size_t i = 0, j = 0; s1[i] == s2[i] && s1[i]; i++, j++);
    return *(unsigned char const*)s1 - *(unsigned char const*)s2;
}
