#include <string.h>

char* strncpy(char* restrict s1, const char* restrict s2, size_t n)
{
    size_t counter;
    for (counter = 0; counter < n && s1[counter] != '\0'; counter++) {
        s1[counter] = s2[counter];
    }
    for (; counter < n; counter++) {
        s1[counter] = '\0';
    }

    return s1;
}
