#include <stdint.h>
#include <string.h>

void* memmove(void* s1, const void* s2, size_t n)
{
    if ((uintptr_t)s1 < (uintptr_t)s2) {
        return memcpy(s1, s2, n);
    } else {
        unsigned char* s1_iterator = s1;
        unsigned char const* s2_iterator = s2;
        for (size_t i = n; i > 0; i--) {
            s1_iterator[i - 1] = s2_iterator[i - 1];
        }
    }
    return s1;
}
