#include <string.h>

void* memccpy(void* restrict s1, void const* restrict s2, int c, size_t n)
{
    void const* found_c = memchr(s2, c, n);

    if (found_c)
        return mempcpy(s1, s2, (size_t)((unsigned char*)found_c - (unsigned char*)s2 + 1));

    memcpy(s1, s2, n);
    return NULL;
}
