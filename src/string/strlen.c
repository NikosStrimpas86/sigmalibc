#include <string.h>

size_t strlen(const char* s)
{
    size_t len = 0;
    for (char const* iter = s; *iter; iter++)
        len++;
    return len;
}
