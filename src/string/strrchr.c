#include <string.h>

char* (strrchr)(const char* s, int c)
{
    size_t length = strlen(s);
    for (size_t i = 0; i <= length; i++) {
        if (s[length - i] == (const char)c) {
            return (char*)(&s[length - i]);
        }
    }
    return NULL;
}
