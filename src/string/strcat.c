#include <string.h>

char* strcat(char* restrict s1, const char* restrict s2)
{
    /* Create an iterator for s1 */
    char* s1_iterator = s1;

    /* Find the end of s1 */
    for (; *s1_iterator; s1_iterator++);

    /*
     * By the end of the loop this will be the state of the memory:
     *       ________
     *      |  'f'  |
     *      ---------
     *      |  'o'  |
     *      ---------
     *      |  'o'  |
     *      ---------
     *      |  '\0' |   <---- s1_iterator
     *      ---------
     *      |   ?   |
     *      ---------
     *      |   ?   |
     *      ---------
     *      |___?___|
     *      
     */


    /* Append the string character to character after the end of s1 */
    for (const char* s2_iterator = s2; *s2_iterator; s2_iterator++) {
        *s1_iterator = *s2_iterator;
        s1_iterator++;
    }
    /* Terminate the final string */
    *s1_iterator = '\0';

    return s1;
}
