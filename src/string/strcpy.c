#include <string.h>

char* strcpy(char* restrict s1, const char* restrict s2) 
{
    /* Save the starting point of s1 */
    char* s1_starting_point = s1;
    /*
     * While iterating each character in s1 and s2,
     * assign each character from s2 in s1
     * All while the two strings do not happen to end
     */
    while((*s1++ = *s2++) != '\0');
    /* Not the GNU way but it works! */
    return s1_starting_point;
}
