#include <string.h>

static void* (*const volatile fptr)(void*, int, size_t) = memset;

void explicit_bzero(void* s, size_t n)
{
    (void)fptr(s, 0, n);
}
