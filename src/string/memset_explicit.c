#include <string.h>

#ifndef _SOLC_EXPERIMENTAL
static void* (*const volatile memset_ptr)(void*, int, size_t) = memset;
#endif

void* memset_explicit(void* s, int c, size_t n)
{
#ifdef _SOLC_EXPERIMENTAL
    void* (*const volatile memset_ptr)(void*, int, size_t) = memset;
    return memset_ptr(s, c, n);
#else
    return memset_ptr(s, c, n);
#endif
}
