#include <string.h>

void* mempcpy(void* restrict s1, void const* restrict s2, size_t n)
{
    return (void*)((unsigned char*)memcpy(s1, s2, n) + n);
}
