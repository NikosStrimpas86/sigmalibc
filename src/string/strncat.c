#include <string.h>

char* strncat(char* restrict s1, const char* restrict s2, size_t n)
{
    size_t counter = 0;
    char* s1_iterator = s1;
    for (; *s1_iterator; s1_iterator++);

    for (const char* s2_iterator = s2; counter < n && (*s2_iterator); s2_iterator++, counter++) {
        *s1_iterator = *s2_iterator;
        s1_iterator++;
    }
    /*
     * TODO: Test this a little bit further
     */
    for (; counter < n; counter++) {
        *s1_iterator++ = '\0';
    }

    return s1;
}
