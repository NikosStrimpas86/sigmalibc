#include <string.h>

void* (memchr)(const void* s, int c, size_t n)
{
    unsigned char ch = (unsigned char) c;
    const unsigned char* s_iterator = (const unsigned char*) s;
    for (size_t i = 0; i < n; i++) {
        if (*s_iterator == ch) {
            return (void*) s_iterator;
        }
        s_iterator++;
    }
    return NULL;
}
