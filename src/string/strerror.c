#include <errno.h>
#include <string.h>

char* strerror(int errnum)
{
    char const* s;
    switch (errnum) {
    case ESUCCESS:
        s = "Success";
        break;
    case EPERM:
        s = "Operation not permitted";
        break;
    case ENOENT:
        s = "No such file or directory";
        break;
    case ESRCH:
        s = "No such process";
        break;
    case EINTR:
        s = "Interrupted function";
        break;
    case EIO:
        s = "I/O error";
        break;
    case ENXIO:
        s = "No such device or address";
        break;
    case E2BIG:
        s = "Argument list too long";
        break;
    case ENOEXEC:
        s = "Executable file format error";
        break;
    case EBADF:
        s = "Bad file descriptor";
        break;
    case ECHILD:
        s = "No child processes";
        break;
    case EWOULDBLOCK: /* EAGAIN */
        s = "Resource unavailable, try again";
        break;
    case ENOMEM:
        s = "Not enough space";
        break;
    case EACCES:
        s = "Permission denied";
        break;
    case EFAULT:
        s = "Bad address";
        break;
    case EBUSY:
        s = "Device or resource busy";
        break;
    case EEXIST:
        s = "File exists";
        break;
    case EXDEV:
        s = "Cross-device link";
        break;
    case ENODEV:
        s = "No such device";
        break;
    case ENOTDIR:
        s = "Not a directory or a symbolic link to a directory";
        break;
    case EISDIR:
        s = "Is a directory";
        break;
    case EINVAL:
        s = "Invalid argument";
        break;
    case ENFILE:
        s = "Too many files open in system";
        break;
    case EMFILE:
        s = "File descriptor value too large";
        break;
    case ENOTTY:
        s = "Inappropriate I/O control operation";
        break;
    case ETXTBSY:
        s = "Text file busy";
        break;
    case EFBIG:
        s = "File too large";
        break;
    case ENOSPC:
        s = "No space left on device";
        break;
    case ESPIPE:
        s = "Invalid seek";
        break;
    case EROFS:
        s = "Read-only file system";
        break;
    case EMLINK:
        s = "Too many links";
        break;
    case EPIPE:
        s = "Broken pipe";
        break;
    case EDOM:
        s = "Mathematics argument out of domain of function";
        break;
    case ERANGE:
        s = "Result too large";
        break;
    case ENOMSG:
        s = "No message of the desired type";
        break;
    case EIDRM:
        s = "Identifier removed";
        break;
    case EDEADLK:
        s = "Resource deadlock would occur";
        break;
    case ENOLCK:
        s = "No locks available";
        break;
    case ENOSTR:
        s = "Not a STREAM";
        break;
    case ENODATA:
        s = "No message is available on the STREAM head read queue";
        break;
    case ETIME:
        s = "Stream ioctl() timeout";
        break;
    case ENOSR:
        s = "No STREAM resources";
        break;
    case ENOLINK:
        s = "Reserved (ENOLINK)";
        break;
    case EPROTO:
        s = "Protocol error";
        break;
    case EMULTIHOP:
        s = "Reserved (EMULTIHOP)";
        break;
    case EBADMSG:
        s = "Bad message";
        break;
    case ENOSYS:
        s = "Functionality not supported";
        break;
    case ENOTEMPTY:
        s = "Directory not empty";
        break;
    case ENAMETOOLONG:
        s = "Filename too long";
        break;
    case ELOOP:
        s = "Too many levels of symbolic links";
        break;
    case EOPNOTSUPP:
        s = "Operation not supported on socket";
        break;
    case ECONNRESET:
        s = "Connection reset";
        break;
    case ENOBUFS:
        s = "No buffer space available";
        break;
    case EAFNOSUPPORT:
        s = "Address family not supported";
        break;
    case EPROTOTYPE:
        s = "Protocol wrong type for socket";
        break;
    case ENOTSOCK:
        s = "Not a socket";
        break;
    case ENOPROTOOPT:
        s = "Protocol not available";
        break;
    case ECONNREFUSED:
        s = "Connection refused";
        break;
    case EADDRINUSE:
        s = "Address in use";
        break;
    case ECONNABORTED:
        s = "Connection aborted";
        break;
    case ENETUNREACH:
        s = "Network unreachable";
        break;
    case ENETDOWN:
        s = "Network is down";
        break;
    case ETIMEDOUT:
        s = "Connection timed out";
        break;
    case EHOSTUNREACH:
        s = "Host is unreachable";
        break;
    case EINPROGRESS:
        s = "Operation in progress";
        break;
    case EALREADY:
        s = "Connection already in progress";
        break;
    case EDESTADDRREQ:
        s = "Destination address required";
        break;
    case EMSGSIZE:
        s = "Message too large";
        break;
    case EPROTONOSUPPORT:
        s = "Protocol not supported";
        break;
    case EADDRNOTAVAIL:
        s = "Address not available";
        break;
    case ENETRESET:
        s = "Connection aborted by network";
        break;
    case EISCONN:
        s = "Socket is connected";
        break;
    case ENOTCONN:
        s = "The socket is not connected";
        break;
    case EDQUOT:
        s = "Reserved (EDQUOT)";
        break;
    case ESTALE:
        s = "Reserved (ESTALE)";
        break;
    case ENOTSUP:
        s = "Not supported";
        break;
    case EILSEQ:
        s = "Illegal byte sequence";
        break;
    case EOVERFLOW:
        s = "Value too large to be stored in data type";
        break;
    case ECANCELED:
        s = "Operation canceled";
        break;
    case ENOTRECOVERABLE:
        s = "State not recoverable";
        break;
    case EOWNERDEAD:
        s = "Previous owner died";
        break;
    case EMAXERRNO:
        s = "The highest errno + 1, no need to worry";
        break;
    default:
        s = "Unknown error";
        break;
    }

    return (char*)s;
}
