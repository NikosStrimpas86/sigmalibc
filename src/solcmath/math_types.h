#ifndef SOLC_MATH_TYPES_H
#define SOLC_MATH_TYPES_H

#include <float.h>
#include <float_info.h>
#include <stdint.h>

/*
 * TODO: Make this file not look hideous
 */

union _Float_extract {
    float number;
    uint32_t representation;
};

union _Double_extract {
    double number;
    uint64_t representation;
};

union _Ldouble_extract {
#if defined(SOLC_LONG_DOUBLE_IS_IEEE754_64_BITS)
    long double number;
    uint64_t representation;
#elif defined(SOLC_LONG_DOUBLE_IS_IEEE754_EXTENDED_80_BITS)
    long double number;
    struct {
        uint64_t mantissa;
        uint16_t sign_and_exponent;
    } representation;
#elif defined(SOLC_LONG_DOUBLE_IS_IEEE754_128_BITS)
    long double number;
    uint64_t representation[2];
#else
    long double number;
    uint64_t representation[2];
#endif
};

#ifdef __STDC_IEC_60559_DFP__

/* TODO: Define extract unions for DecimalNx types */

union _Decimal32_extract {
    _Decimal32 number;
    uint32_t representation;
};

union _Decimal64_extract {
    _Decimal64 number;
    uint64_t representation;
};

union _Decimal128_extract {
    _Decimal128 number;
    uint64_t representation[2];
};

#endif /* __STDC_IEC_60559_DFP__ */

#ifdef __STDC_IEC_60559_BFP__

/* TODO: Define extract unions for FloatNx types */

union _Float16_extract {
    _Float16 number;
    uint16_t representation;
};

union _Float32_extract {
    _Float32 number;
    uint32_t representation;
};

union _Float64_extract {
    _Float64 number;
    uint64_t representation;
};

union _Float128_extract {
    _Float128 number;
    uint64_t representation[2];
};

#endif /* __STDC_IEC_60559_BFP__ */

static inline void __solc_float_evaluate(float val)
{
    [[maybe_unused]] volatile float force_store;
    force_store = val;
}

static inline void __solc_double_evaluate(double val)
{
    [[maybe_unused]] volatile double force_store;
    force_store = val;
}

static inline void __solc_ldouble_evaluate(long double val)
{
    [[maybe_unused]] volatile long double force_store;
    force_store = val;
}

#ifdef __STDC_IEC_60559_DFP__

static inline void __solc_decimal32_evaluate(_Decimal32 val)
{
    volatile _Decimal32 force_store;
    force_store = val;
}

static inline void __solc_decimal64_evaluate(_Decimal64 val)
{
    volatile _Decimal64 force_store;
    force_store = val;
}

static inline void __solc_decimal128_evaluate(_Decimal128 val)
{
    volatile _Decimal128 force_store;
    force_store = val;
}

#endif /* __STDC_IEC_60559_DFP__ */

#ifdef __STDC_IEC_60559_BFP__

static inline void __solc_float16_evaluate(_Float16 val)
{
    volatile _Float16 force_store;
    force_store = val;
}

static inline void __solc_float32_evaluate(_Float32 val)
{
    volatile _Float32 force_store;
    force_store = val;
}

static inline void __solc_float64_evaluate(_Float64 val)
{
    volatile _Float64 force_store;
    force_store = val;
}

static inline void __solc_float128_evaluate(_Float128 val)
{
    volatile _Float128 force_store;
    force_store = val;
}


#endif /* __STDC_IEC_60559_BFP__ */

#if (defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 202311L)) || (defined(__cplusplus) && (__cplusplus >= 201103L))
static_assert(sizeof(float) == sizeof(union _Float_extract), "Extract union could not be defined properly");
static_assert(sizeof(double) == sizeof(union _Double_extract), "Extract union could not be defined properly");
static_assert(sizeof(long double) == sizeof(union _Ldouble_extract), "Extract union could not be defined properly");
#elif defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 201112L) && (__STDC_VERSION__ < 202311L)
_Static_assert(sizeof(float) == sizeof(union _Float_extract), "Extract union could not be defined properly");
_Static_assert(sizeof(double) == sizeof(union _Double_extract), "Extract union could not be defined properly");
_Static_assert(sizeof(long double) == sizeof(union _Ldouble_extract), "Extract union could not be defined properly");
#endif

#endif /* SOLC_MATH_TYPES_H */
