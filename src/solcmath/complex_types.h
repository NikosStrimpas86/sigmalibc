#ifndef SOLC_COMPLEX_TYPES_H
#define SOLC_COMPLEX_TYPES_H

union _Float_complex_parts {
    float _Complex number;
    float as_array[2];
};

union _Double_complex_parts {
    double _Complex number;
    double as_array[2];
};

union _Ldouble_complex_parts {
    long double _Complex number;
    long double as_array[2];
};

#endif /* SOLC_COMPLEX_TYPES_H */
