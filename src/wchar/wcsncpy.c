#include <wchar.h>

wchar_t* wcsncpy(wchar_t* restrict s1, const wchar_t* restrict s2, size_t n)
{
    size_t counter;
    for (counter = 0; counter < n && s1[counter] != L'\0'; counter ++) {
        s1[counter] = s2[counter];
    }
    for (; counter < n; counter++) {
        s1[counter] = L'\0';
    }

    return s1;
}
