#include <wchar.h>

wchar_t* wcpcpy(wchar_t* restrict ws1, const wchar_t* restrict ws2)
{
    return wcscpy(ws1, ws2) + wcslen(ws2);
}
