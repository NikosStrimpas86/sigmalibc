#include <wchar.h>
#include <string.h>

wchar_t* wmempcpy(wchar_t* s1, wchar_t const* s2, size_t n)
{
    return (wchar_t*)mempcpy(s1, s2, n * sizeof(wchar_t));
}
