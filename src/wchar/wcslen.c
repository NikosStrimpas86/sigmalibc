#include <wchar.h>

size_t wcslen(const wchar_t* s)
{
    size_t len = 0;
    for (wchar_t const* iter = s; *iter; iter++)
        len++;
    return len;
}
