#include <wchar.h>

wchar_t* wcscat(wchar_t* restrict s1, const wchar_t* restrict s2)
{
    wchar_t* s1_iterator = s1;
    for (; *s1_iterator; s1_iterator++);
    for (const wchar_t* s2_iterator = s2; *s2_iterator; s2_iterator++) {
        *s1_iterator = *s2_iterator;
        s1_iterator++;
    }
    *s1_iterator = L'\0';

    return s1;
}
