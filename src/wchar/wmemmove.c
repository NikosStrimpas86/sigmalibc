#include <stdint.h>
#include <wchar.h>

wchar_t* wmemmove(wchar_t* s1, const wchar_t* s2, size_t n)
{
    if ((uintptr_t)s1 < (uintptr_t)s2) {
        return wmemcpy(s1, s2, n);
    } else {
        for (size_t i = n; i > 0; i--) {
            s1[i - 1] = s2[i - 1];
        }
    }
    return s1;
}
