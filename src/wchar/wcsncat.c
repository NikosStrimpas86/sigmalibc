#include <wchar.h>

wchar_t* wcsncat(wchar_t* restrict s1, const wchar_t* restrict s2, size_t n)
{
    size_t counter = 0;
    wchar_t* s1_iterator = s1;
    for (; *s1_iterator; s1_iterator++);

    for (const wchar_t* s2_iterator = s2; counter < n && (*s2_iterator); s2_iterator++) {
        *s1_iterator = *s2_iterator;
        s1_iterator++;
    }
    /*
     * TODO: Test this a little bit further
     */
    for (; counter < n; counter++) {
        *s1_iterator++ = L'\0';
    }

    return s1;
}
