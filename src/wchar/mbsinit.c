#include <wchar.h>

int mbsinit(const mbstate_t* ps)
{
    if (!ps) {
        return 1;
    }

    if (ps->_bytes != 0 && ps->_state != 0) {
        return 0;
    }

    return 1;
}
