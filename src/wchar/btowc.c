#include <wchar.h>
#include <stdio.h>

wint_t btowc(int c)
{
    if ((c == EOF) || (c > 0x80))
        return WEOF;
    return c;
}
