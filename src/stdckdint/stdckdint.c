#include <limits.h>
#include <stdckdint.h>

#ifdef _SOLC_USE_PORTABLE_IMPLEMENTATION

union _Solc_llong_extract {
    long long as_llong;
    unsigned long long as_ullong;
};

// FIXME: Use modulo for rollover

static inline bool __solc_ckd_castullull(unsigned long long* to, unsigned long long from)
{
    *to = from;
    return false;
}

static inline bool __solc_ckd_castullul(unsigned long* to, unsigned long long from)
{
    *to = (unsigned long)from;
#    if ULONG_WIDTH < ULLONG_WIDTH
    return from > (unsigned long long)ULONG_MAX;
#    else
    return false;
#    endif
}

static inline bool __solc_ckd_castullui(unsigned int* to, unsigned long long from)
{
    *to = (unsigned int)from;
#    if UINT_WIDTH < ULLONG_WIDTH
    return from > (unsigned long long)UINT_MAX;
#    else
    return false;
#    endif
}

static inline bool __solc_ckd_castullus(unsigned short* to, unsigned long long from)
{
    *to = (unsigned short)from;
#    if USHRT_WIDTH < ULLONG_WIDTH
    return from > (unsigned long long)USHRT_MAX;
#    else
    return false;
#    endif
}

static inline bool __solc_ckd_castulluc(unsigned char* to, unsigned long long from)
{
    *to = (unsigned char)from;
#    if UCHAR_WIDTH < ULLONG_WIDTH
    return from > (unsigned char)UCHAR_MAX;
#    else
    return false;
#    endif
}

static inline bool __solc_ckd_castullsll(long long* to, unsigned long long from)
{
    if (from <= (unsigned long long)LLONG_MAX) {
        *to = (long long)from;
        return false;
    }
    from -= (unsigned long long)LLONG_MAX + 1ull;
    *to = LLONG_MIN + (long long)from;
    return true;
}

static inline bool __solc_ckd_castullsl(long* to, unsigned long long from)
{
    if (from <= (unsigned long long)LONG_MAX) {
        *to = (long)from;
        return false;
    }
#    if LONG_WIDTH < ULLONG_WIDTH
    from -= (unsigned long long)LONG_MAX + 1ull;
    while (from > ((unsigned long long)ULONG_MAX + 1ull)) {
        from -= (unsigned long long)ULONG_MAX + 1ull;
    }
    *to = LONG_MIN + (long)from;
    return true;
#    else
    from -= (unsigned long long)LONG_MAX + 1ull;
    *to = LONG_MIN + from;
    return true;
#    endif
}

static inline bool __solc_ckd_castullsi(int* to, unsigned long long from)
{
    if (from <= (unsigned long long)INT_MAX) {
        *to = (int)from;
        return false;
    }
#    if INT_WIDTH < ULLONG_WIDTH
    from -= (unsigned long long)INT_MAX + 1ull;
    while (from > ((unsigned long long)UINT_MAX + 1ull)) {
        from -= (unsigned long long)UINT_MAX + 1ull;
    }
    *to = INT_MIN + (int)from;
    return true;
#    else
    from -= (unsigned long long)INT_MAX + 1ull;
    *to = INT_MIN + from;
    return true;
#    endif
}

static inline bool __solc_ckd_castullss(short* to, unsigned long long from)
{
    if (from <= (unsigned long long)SHRT_MAX) {
        *to = (short)from;
        return false;
    }
#    if SHRT_WIDTH < ULLONG_WIDTH
    from -= (unsigned long long)SHRT_MAX + 1ull;
    while (from > ((unsigned long long)USHRT_MAX + 1ull)) {
        from -= (unsigned long long)USHRT_MAX + 1ull;
    }
    *to = SHRT_MIN + (short)from;
    return true;
#    else
    from -= (unsigned long long)SHRT_MAX + 1ull;
    *to = SHRT_MIN + from;
    return true;
#    endif
}

static inline bool __solc_ckd_castullsc(signed char* to, unsigned long long from)
{
    if (from <= (unsigned long long)SCHAR_MAX) {
        *to = (signed char)from;
        return false;
    }
#    if SCHAR_WIDTH < ULLONG_WIDTH
    from -= (unsigned long long)SCHAR_MAX + 1ull;
    while (from > ((unsigned long long)SCHAR_MAX + 1ull)) {
        from -= (unsigned long long)UCHAR_MAX + 1ull;
    }
    *to = SCHAR_MIN + (signed char)from;
    return true;
#    else
    from -= (unsigned long long)SCHAR_MAX + 1ull;
    *to = SCHAR_MIN + from;
    return true;
#    endif
}

static inline bool __solc_ckd_castsllull(unsigned long long* to, long long from)
{
    *to = from;
    return from < 0ll;
}

static inline bool __solc_ckd_castsllul(unsigned long* to, long long from)
{
    *to = (unsigned long)from;
#    if ULONG_WIDTH < LLONG_WIDTH
    return (from < 0ll) || (from > (long long)ULONG_MAX);
#    else
    return from < 0ll;
#    endif
}

static inline bool __solc_ckd_castsllui(unsigned int* to, long long from)
{
    *to = (unsigned int)from;
#    if UINT_WIDTH < LLONG_WIDTH
    return (from < 0ll) || (from > (long long)UINT_MAX);
#    else
    return from < 0ll;
#    endif
}

static inline bool __solc_ckd_castsllus(unsigned short* to, long long from)
{
    *to = (unsigned short)from;
#    if USHRT_WIDTH < LLONG_WIDTH
    return (from < 0ll) || (from > (long long)USHRT_MAX);
#    else
    return from < 0ll;
#    endif
}

static inline bool __solc_ckd_castslluc(unsigned char* to, long long from)
{
    *to = (unsigned char)from;
#    if UCHAR_WIDTH < LLONG_WIDTH
    return (from < 0ll) || (from > (long long)UCHAR_MAX);
#    else
    return from < 0ll;
#    endif
}

static inline bool __solc_ckd_castsllsll(long long* to, long long from)
{
    *to = from;
    return false;
}

static inline bool __solc_ckd_castsllsl(long* to, long long from)
{
#    if LONG_WIDTH < LLONG_WIDTH
    if ((from <= (long long)LONG_MAX) && (from >= (long long)LONG_MIN)) {
        *to = (long)from;
        return false;
    }
#        if (LLONG_MAX - 2) <= LONG_MAX // TODO: This may be -1 instead of -2
#            error "Type system does not permit modulo hack"
#        endif
    long long rollover_quotient = from % ((long long)LONG_MAX + 1LL);
    long long number_of_rollovers = from / ((long long)LONG_MAX + 1LL);
    if (from > 0ll) {
        if (number_of_rollovers & 1LL)
            *to = (long)((long long)LONG_MIN + rollover_quotient);
        else
            *to = (long)(0LL + rollover_quotient);
    } else {
        if (number_of_rollovers & 1LL) {
            long long volatile const adjusted_quotient = rollover_quotient + 1LL;
            *to = (long)((long long)LONG_MAX + adjusted_quotient);
        } else
            *to = (long)(0LL + rollover_quotient);
    }
    return true;
#    else
    *to = (long)from;
    return false;
#    endif
}

static bool __solc_ckd_castsllsi(int* to, long long from)
{
#    if INT_WIDTH < LLONG_WIDTH
    if ((from >= (long long)INT_MIN) && (from <= (long long)INT_MAX)) {
        *to = (int)from;
        return false;
    }
#        if (LLONG_MAX - 2) <= INT_MAX
#            error "Type system does not permit modulo hack"
#        endif
    long long rollover_quotient = from % ((long long)INT_MAX + 1LL);
    long long number_of_rollovers = from / ((long long)INT_MAX + 1LL);
    if (from > 0ll) {
        if (number_of_rollovers & 1LL)
            *to = (int)((long long)INT_MIN + rollover_quotient);
        else
            *to = (int)(0LL + rollover_quotient);
    } else {
        if (number_of_rollovers & 1LL) {
            long long volatile const adjusted_quotient = rollover_quotient + 1LL;
            *to = (int)((long long)INT_MAX + adjusted_quotient);
        } else
            *to = (int)(0LL + rollover_quotient);
    }
    return true;
#    else
    *to = (int)from;
    return false;
#    endif
}

static bool __solc_ckd_castsllss(short* to, long long from)
{
#    if SHRT_WIDTH < LLONG_WIDTH
    if ((from >= (long long)SHRT_MIN) && (from <= (long long)SHRT_MAX)) {
        *to = (short)from;
        return false;
    }
#        if (LLONG_MAX - 2) <= SHRT_MAX
#            error "Type system does not permit modulo hack"
#        endif
    long long rollover_quotient = from % ((long long)SHRT_MAX + 1LL);
    long long number_of_rollovers = from / ((long long)SHRT_MAX + 1LL);
    if (from > 0ll) {
        if (number_of_rollovers & 1LL)
            *to = (short)((long long)SHRT_MIN + rollover_quotient);
        else
            *to = (short)(0LL + rollover_quotient);
    } else {
        if (number_of_rollovers & 1LL) {
            long long volatile const adjusted_quotient = rollover_quotient + 1LL;
            *to = (short)((long long)SHRT_MAX + adjusted_quotient);
        } else
            *to = (short)(0LL + rollover_quotient);
    }
    return true;
#    else
    *to = (short)from;
    return false;
#    endif
}

static bool __solc_ckd_castsllsc(signed char* to, long long from)
{
    // TODO: Test this further
#    if SCHAR_WIDTH < LLONG_WIDTH
    if ((from >= (long long)SCHAR_MIN) && (from <= (long long)SCHAR_MAX)) {
        *to = (signed char)from;
        return false;
    }
#        if (LLONG_MAX - 2) <= SCHAR_MAX
#            error "Type system does not permit modulo hack"
#        endif
    long long rollover_quotient = from % ((long long)SCHAR_MAX + 1LL);
    long long number_of_rollovers = from / ((long long)SCHAR_MAX + 1LL);
    if (from > 0ll) {
        if (number_of_rollovers & 1LL)
            *to = (signed char)((long long)SCHAR_MIN + rollover_quotient);
        else
            *to = (signed char)(0LL + rollover_quotient);
    } else {
        if (number_of_rollovers & 1LL) {
            long long volatile const adjusted_quotient = rollover_quotient + 1LL;
            *to = (signed char)((long long)SCHAR_MAX + adjusted_quotient);
        } else
            *to = (signed char)(0LL + rollover_quotient);
    }
    return true;
#    else
    *to = (signed char)from;
    return false;
#    endif
}

static inline bool __solc_ckd_cast_mapperull(void* res, enum _Solc_ckd_res_info info, unsigned long long from)
{
    bool overflow_flag = false;
    switch (info) {
    case _Solc_ckd_res_info_uc:
        overflow_flag = __solc_ckd_castulluc(res, from);
        break;
    case _Solc_ckd_res_info_us:
        overflow_flag = __solc_ckd_castullus(res, from);
        break;
    case _Solc_ckd_res_info_ui:
        overflow_flag = __solc_ckd_castullui(res, from);
        break;
    case _Solc_ckd_res_info_ul:
        overflow_flag = __solc_ckd_castullul(res, from);
        break;
    case _Solc_ckd_res_info_ull:
        overflow_flag = __solc_ckd_castullull(res, from);
        break;
    case _Solc_ckd_res_info_sc:
        overflow_flag = __solc_ckd_castullsc(res, from);
        break;
    case _Solc_ckd_res_info_ss:
        overflow_flag = __solc_ckd_castullss(res, from);
        break;
    case _Solc_ckd_res_info_si:
        overflow_flag = __solc_ckd_castullsi(res, from);
        break;
    case _Solc_ckd_res_info_sl:
        overflow_flag = __solc_ckd_castullsl(res, from);
        break;
    case _Solc_ckd_res_info_sll:
        overflow_flag = __solc_ckd_castullsll(res, from);
        break;
    default:
        break;
    }
    return overflow_flag;
}

static inline unsigned long long __solc_ckd_llabsull(long long j)
{
    if (j == LLONG_MIN) {
        union _Solc_llong_extract capture = { .as_llong = j };
        /* C23 only supports 2's complement. We negate the number manually; */
        unsigned long long result = ~capture.as_ullong;
        return ++result;
    }
    return j < 0ll ? ((unsigned long long)(-j)) : ((unsigned long long)j);
}

static inline bool __solc_ckd_cast_mappersll(void* res, enum _Solc_ckd_res_info info, long long from)
{
    bool overflow_flag = false;
    switch (info) {
    case _Solc_ckd_res_info_sc:
        overflow_flag = __solc_ckd_castsllsc(res, from);
        break;
    case _Solc_ckd_res_info_ss:
        overflow_flag = __solc_ckd_castsllss(res, from);
        break;
    case _Solc_ckd_res_info_si:
        overflow_flag = __solc_ckd_castsllsi(res, from);
        break;
    case _Solc_ckd_res_info_sl:
        overflow_flag = __solc_ckd_castsllsl(res, from);
        break;
    case _Solc_ckd_res_info_sll:
        overflow_flag = __solc_ckd_castsllsll(res, from);
        break;
    case _Solc_ckd_res_info_uc:
        overflow_flag = __solc_ckd_castslluc(res, from);
        break;
    case _Solc_ckd_res_info_us:
        overflow_flag = __solc_ckd_castsllus(res, from);
        break;
    case _Solc_ckd_res_info_ui:
        overflow_flag = __solc_ckd_castsllui(res, from);
        break;
    case _Solc_ckd_res_info_ul:
        overflow_flag = __solc_ckd_castsllul(res, from);
        break;
    case _Solc_ckd_res_info_ull:
        overflow_flag = __solc_ckd_castsllull(res, from);
        break;
    default:
        break;
    }
    return overflow_flag;
}

bool __solc_ckd_addullull(void* res, enum _Solc_ckd_res_info info, unsigned long long a, unsigned long long b)
{
    bool overflow_from_addition = (ULLONG_MAX - a) < b;
    unsigned long long temp_res = a + b;
    bool overflow_from_cast = __solc_ckd_cast_mapperull(res, info, temp_res);
    return overflow_from_addition || overflow_from_cast;
}

bool __solc_ckd_addsllsll(void* res, enum _Solc_ckd_res_info info, long long a, long long b)
{
    bool overflow_from_addition = false;
    long long temp_res = 0ll;
    if ((a > 0ll) && (b > 0ll)) { // FIXME: Could use xor here
        // TODO: maybe make use of the ull ull variant
        long long const volatile available_range = LLONG_MAX - a;
        if (available_range < b) {
            overflow_from_addition = true;
            long long const volatile leftover = b - available_range - 1ll;
            // NOTE: Let's hope compilers won't reorder the following addition, because then it will overflow
            temp_res = LLONG_MIN + leftover;
        } else {
            overflow_from_addition = false;
            temp_res = a + b;
        }
    } else if ((a < 0ll) && (b < 0ll)) {
        if (a == LLONG_MIN) {
            overflow_from_addition = true;
            long long const volatile leftover = b + 1ll;
            temp_res = LLONG_MAX + leftover;
        } else if (b == LLONG_MIN) {
            overflow_from_addition = true;
            long long const volatile leftover = a + 1ll;
            temp_res = LLONG_MAX + leftover;
        } else {
            long long const volatile available_range = LLONG_MIN - a;
            if (available_range > b) {
                overflow_from_addition = true;
                long long const volatile leftover = b - available_range + 1ll;
                temp_res = LLONG_MAX + leftover;
            } else {
                overflow_from_addition = false;
                temp_res = a + b;
            }
        }
    } else {
        temp_res = a + b;
        overflow_from_addition = false;
    }
    bool overflow_from_cast = __solc_ckd_cast_mappersll(res, info, temp_res);
    return overflow_from_addition || overflow_from_cast;
}

bool __solc_ckd_subullull(void* res, enum _Solc_ckd_res_info info, unsigned long long a, unsigned long long b)
{
    bool overflow_from_subtraction = a < b;
    unsigned long long temp_res = a - b;
    bool overflow_from_cast = __solc_ckd_cast_mapperull(res, info, temp_res);
    return overflow_from_subtraction || overflow_from_cast;
}

bool __solc_ckd_mulullull(void* res, enum _Solc_ckd_res_info info, unsigned long long a, unsigned long long b)
{
    if ((a == 0ull) || (b == 0ull))
        return __solc_ckd_cast_mapperull(res, info, 0ull);

    bool overflow_from_multiplication = (ULLONG_MAX / b) < a;
    unsigned long long temp_res = a * b;
    bool overflow_from_cast = __solc_ckd_cast_mapperull(res, info, temp_res);
    return overflow_from_multiplication || overflow_from_cast;
}

#endif

void __solc__internal_ckd_dummy_extern_function_do_not_touch(void)
{
    (void)0;
}
