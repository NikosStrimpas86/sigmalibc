#include <internal/defines/null.h>
#include <search.h>

/*
 * TODO: Ensure POSIX compatibility, maybe change _node to qelem.
 */

struct _node {
    struct _node* _next;
    struct _node* _prev;
};

void insque(void* element, void* pred)
{
    struct _node* new_node = element;
    struct _node* existing_node = pred;
    if (pred == NULL) {
        new_node->_next = NULL;
        new_node->_prev = NULL;
        return;
    }
    struct _node* initial_next = existing_node->_next;
    new_node->_next = existing_node->_next;
    new_node->_prev = existing_node;
    existing_node->_next = new_node;
    if (initial_next != NULL) {
        initial_next->_prev = new_node;
    }
}

void remque(void* element)
{
    struct _node* leaving_element = element;
    if (leaving_element == NULL) {
        return;
    }
    if (leaving_element->_next != NULL) {
        leaving_element->_next->_prev = leaving_element->_prev;
    }
    if (leaving_element->_prev != NULL) {
        leaving_element->_prev->_next = leaving_element->_next;
    }
    /* TODO: Investigate whether to actually disconnect the current node */
#if 0
    leaving_element->_next = NULL;
    leaving_element->_prev = NULL;
#endif
}
