#include <math.h>

/* FIXME: Calculate the result as if there was infinite precision */
float fadd(double x, double y)
{
    return (float)(x + y);
}

/* FIXME: Calculate the result as if there was infinite precision */
float faddl(long double x, long double y)
{
    return (float)(x + y);
}

/* FIXME: Calculate the result as if there was infinite precision */
double daddl(long double x, long double y)
{
    return (double)(x + y);
}

#ifdef __STDC_IEC_60559_DFP__

/* FIXME: Calculate the result as if there was infinite precision */
_Decimal32 d32addd64(_Decimal64 x, _Decimal64 y)
{
    return (_Decimal32)(x + y);
}

/* FIXME: Calculate the result as if there was infinite precision */
_Decimal32 d32addd128(_Decimal128 x, _Decimal128 y)
{
    return (_Decimal32)(x + y);
}

/* FIXME: Calculate the result as if there was infinite precision */
_Decimal64 d64addd128(_Decimal128 x, _Decimal128 y)
{
    return (_Decimal64)(x + y);
}

#endif /* __STDC_IEC_60559_DFP__ */
