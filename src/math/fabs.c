#include <math.h>
#include <math_types.h>

double fabs(double x)
{
    union _Double_extract captured = { .number = x };
    captured.representation &= 0x7fffffffffffffff;

    return captured.number;
}

float fabsf(float x)
{
    union _Float_extract captured = { .number = x };
    captured.representation &= 0x7fffffff;

    return captured.number;
}

long double fabsl(long double x)
{
#if defined(SOLC_LONG_DOUBLE_IS_IEEE754_64_BITS)
    _Static_assert(sizeof(long double) == 8);
    union _Ldouble_extract captured = { .number = x };
    captured.representation &= 0x7fffffffffffffff;

    return captured.number;
#elif defined(SOLC_LONG_DOUBLE_IS_IEEE754_EXTENDED_80_BITS)

#    if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 201112L)
    _Static_assert(sizeof(long double) >= 10, "There's a problem with long double");
    _Static_assert(sizeof(long double) <= 16, "There's a problem with long double");
#    endif
    union _Ldouble_extract captured = { .number = x };
    captured.representation.sign_and_exponent &= 0x7fff;

    return captured.number;
#else
    if (x == -0.0L)
        return +0.0L;
    return (x < 0) ? -x : x;
#endif
}

#ifdef __STDC_IEC_60559_DFP__

_Decimal32 fabsd32(_Decimal32 x)
{
    union _Decimal32_extract captured = { .number = x };
    captured.representation &= 0x7fffffff;

    return captured.number;
}

_Decimal64 fabsd64(_Decimal64 x)
{
    union _Decimal64_extract captured = { .number = x };
    captured.representation &= 0x7fffffffffffffff;

    return captured.number;
}

_Decimal128 fabsd128(_Decimal128 x)
{
    union _Decimal128_extract captured = { .number = x };
    captured.representation[1] &= 0x7fffffffffffffff;

    return captured.number;
}

#endif /* __STDC_IEC_60559_DFP__ */

#ifdef __STDC_IEC_60559_BFP__

_Float16 fabsf16(_Float16 x)
{
    union _Float16_extract captured = { .number = x };
    captured.representation &= 0x7fff;

    return captured.number;
}

_Float32 fabsf32(_Float32 x)
{
    union _Float32_extract captured = { .number = x };
    captured.representation &= 0x7fffffff;

    return captured.number;
}

_Float64 fabsf64(_Float64 x)
{
    union _Float64_extract captured = { .number = x };
    captured.representation &= 0x7fffffffffffffff;

    return captured.number;
}

_Float128 fabsf128(_Float128 x)
{
    union _Float128_extract captured = { .number = x };
    captured.representation[1] &= 0x7fffffffffffffff

    return captured.number;
}

#endif /* __STDC_IEC_60559_BFP__ */
