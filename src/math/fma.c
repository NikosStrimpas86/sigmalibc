#include <math.h>

/* FIXME: Emulate the instruction fma */
double fma(double x, double y, double z)
{
    /* TODO: Add more ways to detect x86 */
#if (defined(__x86_64__) || defined(__x86_64) || defined(__amd64__) || defined(__amd64) || defined(_M_AMD64)) && defined(__FMA__) && __FMA__ && defined(__GNUC__)
    __asm__("vfmadd132sd %1, %2, %0"
            : "+x"(x)
            : "x"(y), "x"(z));
    return x;
#else
    /* Works just fine most of the times */
    /* TODO: Implement using pair floating point arithmetic */
    return x * y + z;
#endif
}

/* FIXME: Emulate the instruction fma */
float fmaf(float x, float y, float z)
{
#if (defined(__x86_64__) || defined(__x86_64) || defined(__amd64__) || defined(__amd64) || defined(_M_AMD64)) && defined(__FMA__) && __FMA__ && defined(__GNUC__)
    __asm__("vfmadd132ss %1, %2, %0"
            : "+x"(x)
            : "x"(y), "x"(z));
    return x;
#else
    double xy = (double)x * y;
    return (float)(xy + z);
#endif
}

/* FIXME: Emulate the instruction fma */
long double fmal(long double x, long double y, long double z)
{
    return x * y + z;
}

#ifdef __STDC_IEC_60559_DFP__

/* FIXME: Emulate the instruction fma */
_Decimal32 fmad32(_Decimal32 x, _Decimal32 y, _Decimal32 z)
{
    return x * y + z;
}

/* FIXME: Emulate the instruction fma */
_Decimal64 fmad64(_Decimal64 x, _Decimal64 y, _Decimal64 z)
{
    return x * y + z;
}

/* FIXME: Emulate the instruction fma */
_Decimal128 fmad128(_Decimal128 x, _Decimal128 y, _Decimal128 z)
{
    return x * y + z;
}

#endif /* __STDC_IEC_60559_DFP__ */
