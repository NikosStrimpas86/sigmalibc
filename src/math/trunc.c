#include <math.h>
#include <math_types.h>

/*
 * TODO: Make this file not look hideous
 */

double trunc(double x)
{
    union _Double_extract captured = { .number = x };
    uint64_t sign = captured.representation & 0x8000000000000000;
    uint64_t exponent = captured.representation >> 52 & UINT64_C(0x7ff);
    int64_t true_exponent = (int64_t)exponent - 1023;
    uint64_t fraction = captured.representation & UINT64_C(0xffffffffffffff);
    uint64_t mask = UINT64_C(0xffffffffffffffff);
    union _Double_extract result;

    if (isnan(x) || isinf(x) || iszero(x))
        return x;

    if (true_exponent >= 52)
        return x;

    if (true_exponent >= 0) {
        mask >>= true_exponent + 12;
        fraction &= (~mask);
    } else {
        fraction = 0;
        exponent = 0;
    }

    result.representation = sign | (exponent << 52) | fraction;
    return result.number;
}

float truncf(float x)
{
    union _Float_extract captured = { .number = x };
    uint32_t sign = captured.representation & UINT32_C(0x80000000);
    uint32_t exponent = captured.representation >> 23 & UINT32_C(0xff);
    int32_t true_exponent = (int32_t)exponent - 127;
    uint32_t fraction = captured.representation & UINT32_C(0x7fffff);
    uint32_t mask = UINT32_C(0xffffffff);
    union _Float_extract result;

    /* Special cases */
    if (isnan(x) || isinf(x) || iszero(x))
        return x;

    /* Logic */

    if (true_exponent >= 23) {
        /* No digits with weight < 0 */
        return x;
    }
    if (true_exponent >= 0) {
        /* Common case */
        mask >>= true_exponent + 9;
        fraction &= (~mask);
    } else {
        /* Numbers < 1 */
        fraction = 0;
        exponent = 0;
    }

    /* Combining the data/results */
    result.representation = sign | (exponent << 23) | fraction;

    return result.number;
}

long double truncl(long double x)
{
#if defined(SOLC_LONG_DOUBLE_IS_IEEE754_64_BITS)
    union _Ldouble_extract captured = { .number = x };
    uint64_t sign = captured.representation & 0x8000000000000000;
    uint64_t exponent = captured.representation >> 52 & UINT64_C(0x7ff);
    int64_t true_exponent = (int64_t)exponent - 1023;
    uint64_t fraction = captured.representation & UINT64_C(0xffffffffffffff);
    uint64_t mask = UINT64_C(0xffffffffffffffff);
    union _Ldouble_extract result;

    if (isnan(x) || isinf(x) || iszero(x))
        return x;

    if (true_exponent >= 52)
        return x;

    if (true_exponent >= 0) {
        mask >>= true_exponent + 12;
        fraction &= (~mask);
    } else {
        fraction = 0;
        exponent = 0;
    }

    result.representation = sign | (exponent << 52) | fraction;
    return result.number;
#elif defined(SOLC_LONG_DOUBLE_IS_IEEE754_EXTENDED_80_BITS)
    /*
     * FIXME (Possibly): Bugs due to special cases in IEEE754 extended format (Haven't found one yet)
     */
    union _Ldouble_extract captured = { .number = x };
    uint16_t sign = captured.representation.sign_and_exponent & UINT16_C(0x8000);
    uint32_t exponent = captured.representation.sign_and_exponent & UINT32_C(0x7fff);
    int32_t true_exponent = (int32_t)exponent - 16383;
    uint64_t fraction = captured.representation.mantissa;
    uint64_t mask = UINT64_C(0xffffffffffffffff) >> 1;
    union _Ldouble_extract result;

    if (isnan(x) || isinf(x) || iszero(x))
        return x;

    if (true_exponent >= 63)
        return x;

    if (true_exponent >= 0) {
        mask >>= true_exponent;
        fraction &= (~mask);
    } else {
        fraction = 0;
        exponent = 0;
    }

    result.representation.sign_and_exponent = sign | exponent;
    result.representation.mantissa = fraction;
    return result.number;
#endif
}
