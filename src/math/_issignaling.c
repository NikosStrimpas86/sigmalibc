#include <math.h>
#include <math_types.h>

int _issignaling(double x)
{
    union _Double_extract captured = { .number = x };
    return (captured.representation & 0x7ff8000000000000) == 0x7ff8000000000000;
}

int _issignalingf(float x)
{
    union _Float_extract captured = { .number = x };
    return (captured.representation & 0x7fc00000) == 0x7fc00000;
}

int _issignalingl(long double x)
{
#if defined(SOLC_LONG_DOUBLE_IS_IEEE754_64_BITS)
    union _Ldouble_extract captured = { .number = x };
    return (captured.representation & 0x7ff8000000000000) == 0x7ff8000000000000;

#elif defined(SOLC_LONG_DOUBLE_IS_IEEE754_EXTENDED_80_BITS)
    union _Ldouble_extract captured = { .number = x };
    return ((captured.representation.sign_and_exponent & 0x7fff) == 0x7fff
        && (captured.representation.mantissa & 0xc000000000000000) == 0x8000000000000000
        && (captured.representation.mantissa & 0x3fffffffffffffff));
#endif
}
