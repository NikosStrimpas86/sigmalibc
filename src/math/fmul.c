#include <math.h>

/* FIXME: Calculate the result as if there was infinite precision */
float fmul(double x, double y)
{
    return (float)(x * y);
}

/* FIXME: Calculate the result as if there was infinite precision */
float fmull(long double x, long double y)
{
    return (float)(x * y);
}

/* FIXME: Calculate the result as if there was infinite precision */
double dmull(long double x, long double y)
{
    return (double)(x * y);
}

#ifdef __STDC_IEC_60559_DFP__

/* FIXME: Calculate the result as if there was infinite precision */
_Decimal32 d32muld64(_Decimal64 x, _Decimal64 y)
{
    return (_Decimal32)(x * y);
}

/* FIXME: Calculate the result as if there was infinite precision */
_Decimal32 d32muld128(_Decimal128 x, _Decimal128 y)
{
    return (_Decimal32)(x * y);
}

/* FIXME: Calculate the result as if there was infinite precision */
_Decimal64 d64muld128(_Decimal128 x, _Decimal128 y)
{
    return (_Decimal64)(x * y);
}

#endif /* __STDC_IEC_60559_DFP__ */
