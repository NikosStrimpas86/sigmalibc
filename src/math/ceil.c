#include <math.h>

/*
 * TODO: Investigate whether w should be volatile or not
 */

double ceil(double x)
{
    if (isnan(x) || isinf(x) || iszero(x))
        return x;
    double w = trunc(x);
    return w < x ? ++w : w;
}

float ceilf(float x)
{
    if (isnan(x) || isinf(x) || iszero(x))
        return x;
    float w = truncf(x);
    return w < x ? ++w : w;
}

long double ceill(long double x)
{
    if (isnan(x) || isinf(x) || iszero(x))
        return x;
    long double w = truncl(x);
    return w < x ? ++w : w;
}
