#include <math.h>

/* FIXME: Calculate the result as if there was infinite precision */
float ffma(double x, double y, double z)
{
    return ((float)((x * y) + z));
}

/* FIXME: Calculate the result as if there was infinite precision */
float ffmal(long double x, long double y, long double z)
{
    return ((float)((x * y) + z));
}

/* FIXME: Calculate the result as if there was infinite precision */
double dfmal(long double x, long double y, long double z)
{
    return ((double)((x * y) + z));
}

#ifdef __STDC_IEC_60559_DFP__

/* FIXME: Calculate the result as if there was infinite precision */
_Decimal32 d32fmad64(_Decimal64 x, _Decimal64 y, _Decimal64 z)
{
    return ((_Decimal32)((x * y) + z));
}

/* FIXME: Calculate the result as if there was infinite precision */
_Decimal32 d32fmad128(_Decimal128 x, _Decimal128 y, _Decimal128 z)
{
    return ((_Decimal32)((x * y) + z));
}

/* FIXME: Calculate the result as if there was infinite precision */
_Decimal64 d64fmad128(_Decimal128 x, _Decimal128 y, _Decimal128 z)
{
    return ((_Decimal64)((x * y) + z));
}

#endif /* __STDC_IEC_60559_DFP__ */
