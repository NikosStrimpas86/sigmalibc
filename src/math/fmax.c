#include <math.h>

double fmax(double x, double y)
{
    if (isnan(x))
        return y;
    if (isnan(y))
        return x;
    int sign_of_x = signbit(x);
    int sign_of_y = signbit(y);
    if (sign_of_x != sign_of_y)
        return sign_of_x ? y : x;
    return x > y ? x : y;
}

float fmaxf(float x, float y)
{
    if (isnan(x))
        return y;
    if (isnan(y))
        return x;
    int sign_of_x = signbit(x);
    int sign_of_y = signbit(y);
    if (sign_of_x != sign_of_y)
        return sign_of_x ? y : x;
    return x > y ? x : y;
}

long double fmaxl(long double x, long double y)
{
    if (isnan(x))
        return y;
    if (isnan(y))
        return x;
    int sign_of_x = signbit(x);
    int sign_of_y = signbit(y);
    if (sign_of_x != sign_of_y)
        return sign_of_x ? y : x;
    return x > y ? x : y;
}

/*
 * TODO: Implement the _DecimalN and _FloatN versions of this function.
 *       We will need isnan for _Decimal for this, and the spec doesn't
 *       really specify if that exists.
 */
