#include <math.h>

/* FIXME: Implement these right */

double nan(const char* tagp)
{
    (void)tagp;
    return NAN;
}

float nanf(const char* tagp)
{
    (void)tagp;
    return NAN;
}

long double nanl(const char* tagp)
{
    (void)tagp;
    return NAN;
}

#ifdef __STDC_IEC_60559_DFP__
_Decimal32 nand32(const char* tagp)
{
    (void)tagp;
    return DEC_NAN;
}

_Decimal64 nand64(const char* tagp)
{
    (void)tagp;
    return DEC_NAN;
}

_Decimal128 nand128(const char* tagp)
{
    (void)tagp;
    return DEC_NAN;
}
#endif /* __STDC_IEC_60559_DFP__ */
