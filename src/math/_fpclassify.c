#include <math.h>
#include <math_types.h>

/*
 * This is a hideous file, but it's supposed to be private
 */

int _fpclassify(double x)
{
    union _Double_extract captured = { .number = x };
    uint64_t exponent = (captured.representation >> 52) & 0x00000000000007ff;
    if (!exponent) {
        return captured.representation & 0x000fffffffffffff ? FP_SUBNORMAL : FP_ZERO;
    }
    if (exponent == 0x00000000000007ff) {
        return captured.representation & 0x000fffffffffffff ? FP_NAN : FP_INFINITE;
    }
    return FP_NORMAL;
}

int _fpclassifyf(float x)
{
    union _Float_extract captured = { .number = x };
    uint32_t exponent = (captured.representation >> 23) & 0x000000ff;
    if (!exponent) {
        return captured.representation & 0x007fffff ? FP_SUBNORMAL : FP_ZERO;
    }
    if (exponent == 0x000000ff) {
        return captured.representation & 0x007fffff ? FP_NAN : FP_INFINITE;
    }
    return FP_NORMAL;
}

int _fpclassifyl(long double x)
{
#if defined(SOLC_LONG_DOUBLE_IS_IEEE754_64_BITS)
    union _Ldouble_extract captured = { .number = x };
    uint64_t exponent = (captured.representation >> 52) & 0x00000000000007ff;
    if (!exponent) {
        return captured.representation & 0x000fffffffffffff ? FP_SUBNORMAL : FP_ZERO;
    }
    if (exponent == 0x00000000000007ff) {
        return captured.representation & 0x000fffffffffffff ? FP_NAN : FP_INFINITE;
    }
    return FP_NORMAL;

#elif defined(SOLC_LONG_DOUBLE_IS_IEEE754_EXTENDED_80_BITS)
#    if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 201112L)
    _Static_assert(sizeof(long double) >= 10, "There's a problem with long double");
    _Static_assert(sizeof(long double) <= 16, "There's a problem with long double");
#    endif
    union _Ldouble_extract captured = { .number = x };
    uint16_t exponent = captured.representation.sign_and_exponent & 0x7fff;
    uint64_t integer_part = captured.representation.mantissa >> 63;
    if (!exponent && !integer_part) {
        return captured.representation.mantissa ? FP_SUBNORMAL : FP_ZERO;
    }
    /* FIXME: Do more testing in the case of unnormal numbers (integer part = 0) */
    if (exponent == 0x7fff) {
        /*
         * FIXME: There are special cases here defined by the integer part and the digit with weight 2^-1.
         *        These special cases lead to infinity, nan, snan, pseudo infinity, pseudo nan, pseudo snan,
         *        and indefinite values.
         */
        return captured.representation.mantissa & 0x7fffffffffffffff ? FP_NAN : FP_INFINITE;
    }
    return FP_NORMAL;
#endif
}
