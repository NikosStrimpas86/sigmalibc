#include <math.h>
#include <math_types.h>
#include <errno.h>

/* TODO: Fix the pragma situation */

int ilogb(double x)
{
    if (iszero(x)) {
#if math_errhandling & MATH_ERRNO
        errno = EDOM;
#endif
#if math_errhandling & MATH_ERREXCEPT
        feraiseexcept(FE_INVALID);
#endif
        return FP_ILOGB0;
    }
    if (isnan(x)) {
#if math_errhandling & MATH_ERRNO
        errno = EDOM;
#endif
#if math_errhandling & MATH_ERREXCEPT
        feraiseexcept(FE_INVALID);
#endif
        return FP_ILOGBNAN;
    }
    if (isinf(x)) {
#if math_errhandling & MATH_ERRNO
        errno = EDOM;
#endif
#if math_errhandling & MATH_ERREXCEPT
        feraiseexcept(FE_INVALID);
#endif
        return INT_MAX;
    }

    union _Double_extract captured = { .number = x };
    uint64_t exp = (captured.representation >> 52) & UINT64_C(0x7ff);
    int32_t uexp = (int64_t)exp - 1023;
    return uexp;
}

int ilogbf(float x)
{
    if (iszero(x)) {
#if math_errhandling & MATH_ERRNO
        errno = EDOM;
#endif
#if math_errhandling & MATH_ERREXCEPT
        feraiseexcept(FE_INVALID);
#endif
        return FP_ILOGB0;
    }
    if (isnan(x)) {
#if math_errhandling & MATH_ERRNO
        errno = EDOM;
#endif
#if math_errhandling & MATH_ERREXCEPT
        feraiseexcept(FE_INVALID);
#endif
        return FP_ILOGBNAN;
    }
    if (isinf(x)) {
#if math_errhandling & MATH_ERRNO
        errno = EDOM;
#endif
#if math_errhandling & MATH_ERREXCEPT
        feraiseexcept(FE_INVALID);
#endif
        return INT_MAX;
    }

    union _Float_extract captured = { .number = x };
    uint32_t exp = (captured.representation >> 23) & UINT32_C(0xff);
    int32_t uexp = (int32_t)exp - 127;
    return uexp;
}

int ilogbl(long double x)
{
#if defined(SOLC_LONG_DOUBLE_IS_IEEE754_64_BITS)
    if (iszero(x)) {
#if math_errhandling & MATH_ERRNO
        errno = EDOM;
#endif
#if math_errhandling & MATH_ERREXCEPT
        feraiseexcept(FE_INVALID);
#endif
        return FP_ILOGB0;
    }
    if (isnan(x)) {
#if math_errhandling & MATH_ERRNO
        errno = EDOM;
#endif
#if math_errhandling & MATH_ERREXCEPT
        feraiseexcept(FE_INVALID);
#endif
        return FP_ILOGBNAN;
    }
    if (isinf(x)) {
#if math_errhandling & MATH_ERRNO
        errno = EDOM;
#endif
#if math_errhandling & MATH_ERREXCEPT
        feraiseexcept(FE_INVALID);
#endif
        return INT_MAX;
    }

    union _Ldouble_extract captured = { .number = x };
    uint64_t exp = (captured.representation >> 52) & UINT64_C(0x7ff);
    int32_t uexp = (int64_t)exp - 1023;
    return uexp;
#elif defined(SOLC_LONG_DOUBLE_IS_IEEE754_EXTENDED_80_BITS)
    if (iszero(x)) {
#if math_errhandling & MATH_ERRNO
        errno = EDOM;
#endif
#if math_errhandling & MATH_ERREXCEPT
        feraiseexcept(FE_INVALID);
#endif
        return FP_ILOGB0;
    }
    if (isnan(x)) {
#if math_errhandling & MATH_ERRNO
        errno = EDOM;
#endif
#if math_errhandling & MATH_ERREXCEPT
        feraiseexcept(FE_INVALID);
#endif
        return FP_ILOGBNAN;
    }
    if (isinf(x)) {
#if math_errhandling & MATH_ERRNO
        errno = EDOM;
#endif
#if math_errhandling & MATH_ERREXCEPT
        feraiseexcept(FE_INVALID);
#endif
        return INT_MAX;
    }

    union _Ldouble_extract captured = { .number = x };
    uint16_t exp = (captured.representation.sign_and_exponent & 0x7fff) & 0xffff;
    int32_t uexp = (int32_t)exp - 16383;
    return uexp;
#endif
}
