#include <math.h>
#include <math_types.h>

int _signbit(double x)
{
#if defined(__has_builtin) && __has_builtin(__builtin_signbit)
    return __builtin_signbit(x);
#else
    union _Double_extract captured = { .number = x };
    return (captured.representation >> 63);
#endif
}

int _signbitf(float x)
{
#if defined(__has_builtin) && __has_builtin(__builtin_signbitf)
    return __builtin_signbitf(x);
#else
    union _Float_extract captured = { .number = x };
    return (captured.representation >> 31);
#endif
}

int _signbitl(long double x)
{
#if defined(__has_builtin) && __has_builtin(__builtin_signbitl)
    return __builtin_signbitl(x);
#else
#    if defined(SOLC_LONG_DOUBLE_IS_IEEE754_64_BITS)
    union _Ldouble_extract captured = { .number = x };
    return (captured.representation >> 31);
#    elif defined(SOLC_LONG_DOUBLE_IS_IEEE754_EXTENDED_80_BITS)
    union _Ldouble_extract captured = { .number = x };
    return (captured.representation.sign_and_exponent >> 15);
#elif defined(define SOLC_LONG_DOUBLE_IS_IEEE754_128_BITS)
    union _Ldouble_extract captured = { .number = x };
    return (captured.representation[1] >> 63)
#    else
    /*
     * FIXME: This doesn't work for -0.0l and raises FE_INVALID in case of NaN
     */
    return x >= 0.0l;
#    endif
#endif
}

#ifdef __STDC_IEC_60559_DFP__

int _signbitd32(_Decimal32 x)
{
#if defined(__has_builtin) && __has_builtin(__builtin_signbitd32)
    return __builtin_signbitd32(x);
#else
    union _Decimal32_extract captured = { .number = x };
    return (captured.representation >> 31);
#endif
}

int _signbitd64(_Decimal64 x)
{
#if defined(__has_builtin) && __has_builtin(__builtin_signbitd64)
    return __builtin_signbitd64(x);
#else
    union _Decimal64_extract captured = { .number = x };
    return (captured.representation >> 63);
#endif
}

int _signbitd128(_Decimal128 x)
{
#if defined(__has_builtin) && __has_builtin(__builtin_signbitd128)
    return __builtin_signbitd128(x);
#else
    union _Decimal128_extract captured = { .number = x };
    return (captured.representation[1] >> 63);
#endif
}

#endif /* __STDC_IEC_60559_DFP__ */

#ifdef __STDC_IEC_60559_BFP__

int _signbitf16(_Float16 x)
{
    union _Float16_extract captured = { .number x };
    return ((captured.representation & 0xffff) >> 15);
}

int _signbitf32(_Float32 x)
{
    union _Float32_extract captured = { .number x };
    return (captured.representation >> 31);
}

int _signbitf64(_Float64 x)
{
    union _Float64_extract captured = { .number x };
    return (captured.representation >> 63);
}

int _signbitf128(_Float128 x)
{
    union _Float128_extract captured = { .number x };
    return (captured.representation[1] >> 63);
}

#endif /* __STDC_IEC_60559_BFP__ */
