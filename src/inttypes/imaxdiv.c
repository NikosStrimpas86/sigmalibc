#include <inttypes.h>

imaxdiv_t imaxdiv(intmax_t numer, intmax_t denom)
{
    imaxdiv_t result_struct;
    result_struct.quot = numer / denom;
    result_struct.rem = numer % denom;
    return result_struct;
}
