# Standard Operating Library for C or in short SOLC
My own implementation of the C standard library based on the ISO/IEC 9899:2023 draft, published in 1st of April 2023.
The draft can be found [here](https://www.open-std.org/JTC1/SC22/WG14/www/docs/n3096.pdf)