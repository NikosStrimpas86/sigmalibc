#ifndef SOLC_FENV_H
#define SOLC_FENV_H

#include <sys/cdefs.h>

#define __STDC_VERSION_FENV_H__ 202111L

__BEGIN_DECLS

typedef unsigned short int fexcept_t;

#define FE_INVALID               0x01
#define FE_DIVBYZERO             0x04
#define FE_OVERFLOW              0x08
#define FE_UNDERFLOW             0x10
#define FE_INEXACT               0x20

#define FE_ALL_EXCEPT            (FE_INVALID | FE_DIVBYZERO | FE_OVERFLOW | FE_UNDERFLOW | FE_INEXACT)

#define FE_DOWNWARD              0
#define FE_TONEAREST             1
#define FE_TONEARESTFROMZERO     2
#define FE_TOWARDZERO            3
#define FE_UPWARD                4

#define FE_DEC_DOWNWARD          0
#define FE_DEC_TONEAREST         1
#define FE_DEC_TONEARESTFROMZERO 2
#define FE_DEC_TOWARDZERO        3
#define FE_DEC_UPWARD            4

int feraiseexcept(int excepts);

__END_DECLS

#endif /* SOLC_FENV_H */
