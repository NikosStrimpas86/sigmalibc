#ifndef SOLC_UCHAR_H
#define SOLC_UCHAR_H

#include <internal/types/mbstate_t.h>
#include <internal/types/size_t.h>
#include <stdint.h>
#include <sys/cdefs.h>

__BEGIN_DECLS

/* clang-format off */
#if defined(__STDC_VERSION__) && (__STDC_VERSION__ > 201710L) && !defined(__cpp_char8_t)
#   ifndef __SOLC_CHAR8_T_DEFINED
#   define __SOLC_CHAR8_T_DEFINED
typedef unsigned char char8_t;
#   endif /* __SOLC_CHAR8_T_DEFINED */
#endif /* not C2x and C++ char8_t */

#ifndef __SOLC_CHAR16_T_DEFINED
#define __SOLC_CHAR16_T_DEFINED
typedef uint_least16_t char16_t;
#endif /* __SOLC_CHAR16_T_DEFINED */

#ifndef __SOLC_CHAR32_T_DEFINED
#define __SOLC_CHAR32_T_DEFINED
typedef uint_least32_t char32_t;
#endif /* __SOLC_CHAR32_T_DEFINED */
/* clang-format on */

#if (defined(__STDC_VERSION__) && (__STDC_VERSION__ > 201710L)) || defined(__cpp_char8_t)
size_t mbrtoc8(char8_t* restrict pc8, const char* restrict s, size_t n, mbstate_t* restrict ps);
size_t c8rtomb(char* restrict s, char8_t c8, mbstate_t* restrict ps);
#endif /* C2x or C++ char8_t */
size_t mbrtoc16(char16_t* restrict pc16, const char* restrict s, size_t n, mbstate_t* restrict ps);
size_t c16rtomb(char* restrict s, char16_t c16, mbstate_t* restrict ps);
size_t mbrtoc32(char32_t* restrict pc32, const char* restrict s, size_t n, mbstate_t* restrict ps);
size_t c32rtomb(char* restrict s, char32_t c32, mbstate_t* restrict ps);

__END_DECLS

#endif /* SOLC_UCHAR_H */
