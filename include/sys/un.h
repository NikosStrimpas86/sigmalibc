#ifndef SOLC_UN_H
#define SOLC_UN_H

/*
 * FIXME: This header should probably pull sa_family_t from somewhere else
 */
#include <sys/socket.h>

#define SUN_LEN_MIN 92
#define SUN_LEN_MAX 108

struct socaddr_un {
    sa_family_t sun_family;
    char sun_path[SUN_LEN_MAX];
};

#endif /* SOLC_UN_H */
