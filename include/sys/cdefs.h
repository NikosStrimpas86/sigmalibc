#ifndef SOLC_CDEFS_H
#define SOLC_CDEFS_H

/*
 * TODO: Add more stuff here, and add the DECLS macros everywhere needed.
 */

#ifdef __cplusplus
#    ifndef __BEGIN_DECLS
#        define __BEGIN_DECLS extern "C" {
#        define __END_DECLS   }
#    endif /* __BEGIN_DECLS */
#else
#    ifndef __BEGIN_DECLS
#        define __BEGIN_DECLS
#        define __END_DECLS
#    endif /* __BEGIN_DECLS */
#endif     /* __cplusplus */

#ifdef __P
#    undef __P
#endif
#define __P(parameters) parameters

#endif /* SOLC_CDEFS_H */
