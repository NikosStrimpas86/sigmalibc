#ifndef SOLC_WCTYPE_H
#define SOLC_WCTYPE_H

#include <internal/types/wint_t.h>
#include <sys/cdefs.h>

__BEGIN_DECLS

/* clang-format off */
#ifndef __SOLC_WCTRANS_T_DEFINED
#define __SOLC_WCTRANS_T_DEFINED
typedef int wctrans_t;
#endif /* __SOLC_WCTRANS_T_DEFINED */

#ifndef __SOLC_WCTYPE_T_DEFINED
#define __SOLC_WCTYPE_T_DEFINED
typedef int wctype_t;
#endif /* __SOLC_WCTYPE_T_DEFINED */
/* clang-format on */
/* TODO: Find a nice way to define WEOF. */

#ifndef WEOF
#    if __WINT_WIDTH__ == 32
#        define WEOF (0xffffffffu)
#    elif __WINT_WIDTH__ == 16
#        define WEOF (0xffff)
#    endif
#endif

int iswalnum(wint_t wc);
int iswalpha(wint_t wc);
int iswblank(wint_t wc);
int iswcntrl(wint_t wc);
int iswdigit(wint_t wc);
int iswgraph(wint_t wc);
int iswlower(wint_t wc);
int iswprint(wint_t wc);
int iswpunct(wint_t wc);
int iswspace(wint_t wc);
int iswupper(wint_t wc);
int iswxdigit(wint_t wc);
int iswctype(wint_t wc, wctype_t desc);
wctype_t wctype(const char* property);
wint_t towlower(wint_t wc);
wint_t towupper(wint_t wc);
wint_t towctrans(wint_t wc, wctrans_t desc);
wctrans_t wctrans(const char* property);

__END_DECLS

#endif /* SOLC_WCTYPE_H */
