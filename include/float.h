#ifndef SOLC_FLOAT_H
#define SOLC_FLOAT_H

/*
 * 7.7 Characteristics of floating types <float.h>
 *
 * 1. The header <float.h> defines several macros that expand to various limits and parameters of
 *    the real floating types.
 * 2. The macros, their meanings, and the constraints (or restrictions) on their values are listed
 *    in 5.2.4.2.2 and 5.2.4.2.3. A summary is given in Annex E.
 */

/*
 * 5.2.4.2.2 Characteristics of floating types <float.h>
 *
 * 1.  The characteristics of floating types are defined in terms of a model that describes a
 *     representation of floating-point numbers and values that provide information about an
 *     implementation's floating-point arithmetic. An implementation that defines
 *     __STDC_IEC_60559_BFP__ or __STDC_IEC_559__ shall implement floating point types and
 *     arithmetic conforming to IEC 60559 as specified in Annex F. An implementation that defines
 *     __STDC_IEC_60559_COMPLEX__ or __STDC_IEC_559_COMPLEX__ shall implement complex types and
 *     arithmetic conforming to IEC 60559 as specified in Annex G.
 * 2.  The following parameters are used to define the model for each floating-point type:
 *       s   sign (+/-1)
 *       b   base or radix of exponent representation (an integer > 1)
 *       e   exponent (an integer between a minimum e_min and a maximum e_max)
 *       p   precision (the number of base-b digits in the significand)
 *       f_k nonnegative integers less than b (the significand digits)
 *     For each floating-point type, the parameters, b, p, e_min and e_max are fixed constants.
 * 3.  For each floating-point type, a floating-point number (x) is defined by the following model:
 *                      p
 *       x = s * b^e * SUM f_k * b^-k, e_min <= e <= e_max
 *                     k=1
 * 4.  Floating types shall be able to represent zero (all f_k==0) and all normalized floating-point
 *     numbers (f1 > 0 and all possible k digits and e exponents result in values representable in
 *     type). In addition, floating types may be able to contain other kinds of floating-point
 *     numbers, such as negative zero, subnormal floating-point numbers (x!=0, e=e_min, f_1=0) and
 *     unnormalized floating-point numbers (x!=0, e>e_min, f_1=0) and values that are not
 *     floating-point numbers, such as infinities and NaNs. An NaN is a value signifying Not-a-Number.
 *     A quiet NaN propagates though almost every arithmetic operation without raising a
 *     floating-point exception; a signaling NaN generally raises a floating point exception when
 *     occuring as an arithmetic operand.
 * 5.  An implementation may give zero and values that are not floating-point numbers (such as
 *     infinities and NaNs) a sign or may leave them unsigned. Whenever such values are unsigned,
 *     any requirement in this document to retrieve the sign shall produce an unspecified sign, and
 *     any requirement to set the sign shall be ignored.
 * 6.  An implementation may prefer particular representations of values that have multiple
 *     representations in a floating type, 6.2.6.1 not withstanding. The preferred representations
 *     of a floating type, including unique representations of values in the type, are called canonical.
 *     A floating type may also contain non-canonical representations, for example, redundant
 *     representations of some or all of its values, or representations that are extraneous to the
 *     floating-point model. Typically, floating-point operations deliver results with canonical
 *     representations. IEC 60559 operations deliver results with canonical representations, unless
 *     specified otherwise.
 * 7.  The minimum range of representable values for a floating type is the most negative finite
 *     floating-point number representable in that type through the most positive finite
 *     floating-point number representable in that type. In addition, if negative infinity is
 *     representable in a type, the range of that type is extended to all negative real numbers;
 *     likewise, if positive infinity is representable in a type, the range of that type is extended
 *     to all positive real numbers.
 * 8.  The accuracy of the floating-point operations (+, -, *, /) and of the library functions in
 *     <math.h> and <complex.h> that return floating-point results is implementation-defined, as is
 *     the accuracy of the conversion between floating-point internal representations and string
 *     representations by the library functions in <stdio.h>, <stdlib.h> and <wchar.h>. The
 *     implementation may state that the accuracy is unknown. Decimal floating-point operations have
 *     stricter requirements.
 * 9.  All integer values in the <float.h> header, except FLT_ROUNDS, shall be constant expressions
 *     suitable for use in #if preprocessing directives; all floating values shall be constant
 *     expressions. All except CR_DECIMAL_DIG, DECIMAL_DIG, DEC_EVAL_METHOD, FLT_EVAL_METHOD, FLT_RADIX,
 *     and FLT_ROUNDS have separate names for all floating-point types. The floating-point model
 *     representation is provided for all values except DEC_EVAL_METHOD, FLT_EVAL_METHOD and FLT_ROUNDS.
 * 10. The remainder of this subclause specifies characteristics of standard floating types.
 * 11. The rounding model for floating-point addition for standard floating types is characterized by
 *     the implementation-defined value of FLT_ROUNDS. Evaluation of FLT_ROUNDS correctly refrects
 *     any execution-time change of rounding mode through the function fesetround in <fenv.h>
 *
 *      -1  indeterminable
 *       0  toward zero
 *       1  to nearest, ties to even
 *       2  toward positive infinity
 *       3  toward negative infinity
 *       4  to nearest, ties away from zero
 *     All other values for FLT_ROUNDS characterize implementation-defined rounding behavior.
 * 12. Whether a type matches an IEC 60559 format (and perhaps, operations) is characterized by the
 *     implementation-defined balues of FLT_IS_IEC_60559, DBL_IS_IEC_60559, and LDBL_IS_IEC_60559
 *     (this does not imply conformance to Annex F):
 *       0  type does not match an IEC 60559 format
 *       1  type matches an IEC 60559 format
 *       2  type matches an IEC 60559 format and operations
 * 13. The values of floating type yielded by operators subject to the usual arithmetic conversions,
 *     including the values yielded by the implicit conversion of operands, and the values of floating
 *     constants are evaluated to a format whose range and precision may be greater than required by
 *     the type. Such a format is called an evaluation format. In all cases, assignment and cast
 *     operators yield values in the format of the type. The extent to which evaluations formats are
 *     used is characteerized by the value of FLT_EVAL_METHOD.
 *      -1  indeterminable;
 *       0  evaluate all operations and constants just to the range and precision of the type;
 *       1  evaluate operations and constants of type float and double to the range and precision of
 *          the double type, evaluate long double operations and constants to the range and precision
 *          of the long double type;
 *       2  evaluate all operations and constants to the range and precision of the long double type.
 *     All other negative values for FLT_EVAL_METHOD characterize implementation-defined behavior.
 *     The value of FLT_EVAL_METHOD does not characterize values returned by function calls (see 6.8.6.4, F.6).
 * 14. The presence or absence of subnormal numbers is characterized by the implementation-defined
 *     values of FLT_HAS_SUBNORM, DBL_HAS_SUBNORM, and LDBL_HAS_SUBNORM:
 *      -1  indeterminable
 *       0  absent (type does not support subnormal numbers)
 *       1  present (type does support subnormal numbers)
 *
 */

int __solc_flt_rounds();
#define FLT_ROUNDS (__solc_flt_rounds())

#ifdef __FLT_IS_IEC_60559__
#    define FLT_IS_IEC_60559 __FLT_IS_IEC_60559__
#endif
#ifdef __DBL_IS_IEC_60559__
#    define DBL_IS_IEC_60559 __DBL_IS_IEC_60559__
#endif
#ifdef __LDBL_IS_IEC_60559__
#    define LDBL_IS_IEC_60559 __LDBL_IS_IEC_60559__
#endif

#define FLT_EVAL_METHOD __FLT_EVAL_METHOD__

#ifdef __FLT_HAS_DENORM__
#    define FLT_HAS_SUBNORM __FLT_HAS_DENORM__
#else
#    define FLT_HAS_SUBNORM 1
#endif

/*
 * 15. The signaling NaN macros
 * -    FLT_SNAN
 * -    DBL_SNAN
 * -    LDBL_SNAN
 *     each is defined if and only if the respective type contains signaling NaNs. They expand to a
 *     constant expression of the respective type representing a signaling NaN. If an optional unary
 *     + or - operator followed by a signaling NaN macro is used as the initializer for initializing
 *     an object of the same type that has static or thread-local storage duration, the object is
 *     initialized with a signaling NaN value.
 * 16. The macro
 * -    INFINITY
 *     expands to a constant expression of type float representing positive or unsigned infinity, if
 *     available; else to a positive constant of type float that overflows at translation time.
 * 17. The macro
 * -    NAN
 *     is defined if and only if the implementation supports quiet NaNs for the float type. It expands
 *     to a constant expression of type float representing a quiet NaN.
 */
/*
 *
 *
 * #define FLT_SNAN
 * #define DBL_SNAN
 * #define LDBL_SNAN
 *
 * #define INFINITY
 * #define NAN
 */

#if defined(__STDC_VERSION__) && (__STDC_VERSION__ > 201710L)
#    define FLT_SNAN  __builtin_nansf("")
#    define DBL_SNAN  __builtin_nans("")
#    define LDBL_SNAN __builtin_nansl("")

#    define INFINITY  __builtin_inff()
#    define NAN       __builtin_nanf("")
#endif

/*
 * 18. The values given in the following list shall be replaced by constant expressions with
 *     implementation-defined values that are greater or equal in magnitude (absolute value) to those
 *     shown, with the same sign:
 *      - radix of exponent representation, b
 *      -   FLT_RADIX   2
 */
#ifdef __FLT_RADIX__
#    define FLT_RADIX __FLT_RADIX__
#else
#    define FLT_RADIX 2
#endif
/*
 *      - number of base-FLT_RADIX digits in the floating-point significand, p
 *      -   FLT_MANT_DIG
 *      -   DBL_MANT_DIG
 *      -   LDBL_MANT_DIG
 */
#ifdef __FLT_MANT_DIG__
#    define FLT_MANT_DIG __FLT_MANT_DIG__
#else
#    define FLT_MANT_DIG 24
#endif
#ifdef __DBL_MANT_DIG__
#    define DBL_MANT_DIG __DBL_MANT_DIG__
#else
#    define DBL_MANT_DIG 53
#endif
#ifdef __LDBL_MANT_DIG__
#    define LDBL_MANT_DIG __LDBL_MANT_DIG__
#else
#    define LDBL_MANT_DIG 64
#endif

/*
 *      - number of decimal digits, n, such that any floating-point number with p radix b digits can
 *        be rounded to a floating-point number with n decimal digits and back again without change
 *        to the value,
 *          / p * log_10(b)           if b is a power of 10
 *          \ ceil(1 + p * log_10(b)) otherwise
 *      -   FLT_DECIMAL_DIG     6
 *      -   DBL_DECIMAL_DIG     10
 *      -   LDBL_DECIMAL_DIG    10
 */
#ifdef __FLT_DECIMAL_DIG__
#    define FLT_DECIMAL_DIG __FLT_DECIMAL_DIG__
#else
/* FLT_DECIMAL_DIG = ceil(1 + 24 * log10(2)) = 9 */
#    define FLT_DECIMAL_DIG 9
#endif
#ifdef __DBL_DECIMAL_DIG__
#    define DBL_DECIMAL_DIG __DBL_DECIMAL_DIG__
#else
/* DBL_DECIMAL_DIG = ceil(1 + 53 * log10(2)) = 17 */
#    define DBL_DECIMAL_DIG 17
#endif
#ifdef __LDBL_DECIMAL_DIG__
#    define LDBL_DECIMAL_DIG __LDBL_DECIMAL_DIG__
#else
/* LDBL_DECIMAL_DIG = ceil(1 + 64 * log10(2)) = 21 */
#    define LDBL_DECIMAL_DIG 21
#endif

/*
 *      - number of decimal digits, n, such that any floating-point number in the widest of the
 *        supported floating types and supported IEC 60559 encodings with p_max radix b digits can
 *        be rounded to a floating-point number with n decimal digits and back again without change
 *        to the value,
 *          / p_max * log_10(b)           if b is power of 10
 *          \ ceil(1 + p_max * log_10(b)) otherwise
 *      -   DECIMAL_DIG     10
 */
#ifdef __DECIMAL_DIG__
#    define DECIMAL_DIG __DECIMAL_DIG__
#else
#    define DECIMAL_DIG 21 /* ??? */
#endif
/* This is an obsolescent feature, see 7.31.8. */

/*
 *      - number of decimal digits, q, such that any floating-point number with q decimal digits can
 *        be rounded into floating-point number with p radix b digits and back again without change
 *        to the q decimal digits,
 *          / p * log_10(b)              if b is a power of 10
 *          \ floor((p - 1) * log_10(b)) otherwise
 *      -   FLT_DIG     6
 *      -   DBL_DIG     10
 *      -   LDBL_DIG    10
 */
#ifdef __FLT_DIG__
#    define FLT_DIG __FLT_DIG__
#else
#    define FLT_DIG 6
#endif
#ifdef __DBL_DIG__
#    define DBL_DIG __DBL_DIG__
#else
#    define DBL_DIG 15
#endif
#ifdef __LDBL_DIG__
#    define LDBL_DIG __LDBL_DIG__
#else
#    define LDBL_DIG 18
#endif

/*
 *      - minimum negative integer such that FLT_RADIX raised to one less than that power is
 *        normalized floating-point number, e_min
 *      -   FLT_MIN_EXP
 *      -   DBL_MIN_EXP
 *      -   LDBL_MIN_EXP
 */
#ifdef __FLT_MIN_EXP__
#    define FLT_MIN_EXP __FLT_MIN_EXP__
#else
#    define FLT_MIN_EXP (-125)
#endif
#ifdef __DBL_MIN_EXP__
#    define DBL_MIN_EXP __DBL_MIN_EXP__
#else
#    define DBL_MIN_EXP (-1021)
#endif
#ifdef __LDBL_MIN_EXP__
#    define LDBL_MIN_EXP __LDBL_MIN_EXP__
#else
#    define LDBL_MIN_EXP (-16381)
#endif

/*
 *      - minimum negative integer such that 10 raised to that power is in the range of normalized
 *        floating-point numbers, ceil(log_10(b^(e_min - 1))
 *      -   FLT_MIN_10_EXP      -37
 *      -   DBL_MIN_10_EXP      -37
 *      -   LDBL_MIN_10_EXP     -37
 */
#ifdef __FLT_MIN_10_EXP__
#    define FLT_MIN_10_EXP __FLT_MIN_10_EXP__
#else
#    define FLT_MIN_10_EXP (-37)
#endif
#ifdef __DBL_MIN_10_EXP__
#    define DBL_MIN_10_EXP __DBL_MIN_10_EXP__
#else
#    define DBL_MIN_10_EXP (-307)
#endif
#ifdef __LDBL_MIN_10_EXP__
#    define LDBL_MIN_10_EXP __LDBL_MIN_10_EXP__
#else
#    define LDBL_MIN_10_EXP (-4931)
#endif

/*
 *      - maximum integer such that FLT_RADIX raised one less than that power is a representable
 *        finite floating-point number, e_max
 *      -   FLT_MAX_EXP
 *      -   DBL_MAX_EXP
 *      -   LDBL_MAX_EXP
 */
#ifdef __FLT_MAX_EXP__
#    define FLT_MAX_EXP __FLT_MAX_EXP__
#else
#    define FLT_MAX_EXP 128
#endif
#ifdef __DBL_MAX_EXP__
#    define DBL_MAX_EXP __DBL_MAX_EXP__
#else
#    define DBL_MAX_EXP 1024
#endif
#ifdef __LDBL_MAX_EXP__
#    define LDBL_MAX_EXP __LDBL_MAX_EXP__
#else
#    define LDBL_MAX_EXP 16384
#endif

/*
 *      - maximum integer such that 10 raised to that power is in the range of representable finite
 *        floating-point numbers, floor(log_10((1 - b^(-p))b^e_max))
 *      -   FLT_MAX_10_EXP      +37
 *      -   DBL_MAX_10_EXP      +37
 *      -   LDBL_MAX_10_EXP     +37
 */
#ifdef __FLT_MAX_10_EXP__
#    define FLT_MAX_10_EXP __FLT_MAX_10_EXP__
#else
#    define FLT_MAX_10_EXP 38
#endif
#ifdef __DBL_MAX_10_EXP__
#    define DBL_MAX_10_EXP __DBL_MAX_10_EXP__
#else
#    define DBL_MAX_10_EXP 308
#endif
#ifdef __LDBL_MAX_10_EXP__
#    define LDBL_MAX_10_EXP __LDBL_MAX_10_EXP__
#else
#    define LDBL_MAX_10_EXP 4932
#endif

/*
 * 19. The values given in the following list shall be replaced by constant expressions with
 *     implementation-defined values that are greater than or equal to those shown:
 *      - maximum representable finite floating-point number; if that number is normalized, its value
 *        is (1 - b^(-p)) * b^e_max
 *      -   FLT_MAX             1E+37
 *      -   DBL_MAX             1E+37
 *      -   LDBL_MAX            1E+37
 */
#ifdef __FLT_MAX__
#    define FLT_MAX __FLT_MAX__
#else
#    define FLT_MAX (3.40282346638528859811704183484516925e+38f)
#endif
#ifdef __DBL_MAX__
#    define DBL_MAX __DBL_MAX__
#else
#    define DBL_MAX (1.79769313486231570814527423731704357e+308)
#endif
#ifdef __LDBL_MAX__
#    define LDBL_MAX __LDBL_MAX__
#else
#    define LDBL_MAX (1.18973149535723176502126385303097021e+4932L)
#endif

/*
 *      - maximum normalized floating-point number, (1 - b^(-p)) * b^e_max
 *      -   FLT_NORM_MAX        1E+37
 *      -   DBL_NORM_MAX        1E+37
 *      -   LDBL_NORM_MAX       1E+37
 */
#ifdef __FLT_NORM_MAX__
#    define FLT_NORM_MAX __FLT_NORM_MAX__
#else
#    define FLT_NORM_MAX (3.40282346638528859811704183484516925e+38f)
#endif
#ifdef __DBL_NORM_MAX__
#    define DBL_NORM_MAX __DBL_NORM_MAX__
#else
#    define DBL_NORM_MAX (1.79769313486231570814527423731704357e+308)
#endif
#ifdef __LDBL_NORM_MAX__
#    define LDBL_NORM_MAX __LDBL_NORM_MAX__
#else
#    define LDBL_NORM_MAX (1.18973149535723176502126385303097021e+4932L)
#endif

/*
 * 20. The values given in hte following list shall be replaced by constant expressions with
 *     implementation-defined (positive) values that are less than or equal to those shown:
 *      - the difference between 1 and the least normalized value greater than 1 that is
 *        representable in the given floating-point type, b^(1 - p)
 *      -   FLT_EPSILON         1E-5
 *      -   DBL_EPSILON         1E-9
 *      -   LDBL_EPSILON        1E-9
 */
#ifdef __FLT_EPSILON__
#    define FLT_EPSILON __FLT_EPSILON__
#else
#    define FLT_EPSILON (1.19209289550781250000000000000000000e-7f)
#endif
#ifdef __DBL_EPSILON__
#    define DBL_EPSILON __DBL_EPSILON__
#else
#    define DBL_EPSILON (2.220446049250313080847263336181640625e-16)
#endif
#ifdef __LDBL_EPSILON__
#    define LDBL_EPSILON __LDBL_EPSILON__
#else
#    define LDBL_EPSILON (1.08420217248550443400745280086994171142578125e-19L)
#endif

/*
 *      - minimum normalized positive floating-point number, b^e_min - 1
 *      -   FLT_MIN             1E-37
 *      -   DBL_MIN             1E-37
 *      -   LDBL_MIN            1E-37
 */
#ifdef __FLT_MIN__
#    define FLT_MIN __FLT_MIN__
#else
#    define FLT_MIN (1.17549435082228750796873653722224568e-38f)
#endif
#ifdef __DBL_MIN__
#    define DBL_MIN __DBL_MIN__
#else
#    define DBL_MIN (2.22507385850720138309023271733240406e-308)
#endif
#ifdef __LDBL_MIN__
#    define LDBL_MIN __LDBL_MIN__
#else
#    define LDBL_MIN (3.36210314311209350626267781732175260e-4932L)
#endif

/*
 *      - minimum positive floating-point number
 *      -   FLT_TRUE_MIN        1E-37
 *      -   FLT_TRUE_MIN        1E-37
 *      -   FLT_TRUE_MIN        1E-37
 */
#ifdef __FLT_DENORM_MIN__
#    define FLT_TRUE_MIN __FLT_DENORM_MIN__
#else
#    define FLT_TRUE_MIN (1.40129846432481707092372958328991613e-45f)
#endif
#ifdef __DBL_DENORM_MIN__
#    define DBL_TRUE_MIN __DBL_DENORM_MIN__
#else
#    define DBL_TRUE_MIN (4.94065645841246544176568792868221372e-324)
#endif
#ifdef __LDBL_DENORM_MIN__
#    define LDBL_TRUE_MIN __LDBL_DENORM_MIN__
#else
#    define LDBL_TRUE_MIN (3.64519953188247460252840593361941982e-4951L)
#endif

/*
 * Recommended practice
 *
 * 21. Conversion between real floating type and decimal character sequence with at most T_DECIMAL_DIG
 *     digits should be correctly rounded, where T is the macros prefix for the type. This assures
 *     conversion from real floating type to decimal character sequence with T_DECIMAL_DIG digits and
 *     back, using to-nearest rounding, is the identity function.
 * 22. EXAMPLE 1 The following describes an artificial floating-point representation that meets the
 *     minimum requirements of this document and the appropriate values in a <float.h> header for
 *     type float:
 *                          6
 *          x = s * 16^e * SUM f_k * 16^(-k),     -31 <= e <= +32
 *                         k=1
 *      -   FLT_RADIX                            16
 *      -   FLT_MANT_DIG                          6
 *      -   FLT_EPSILON             9.53674316E-07F
 *      -   FLT_DECIMAL_DIG                       9
 *      -   FLT_DIG                               6
 *      -   FLT_MIN_EXP                         -31
 *      -   FLT_MIN                 2.93873588E-39F
 *      -   FLT_MIN_10_EXP                      -38
 *      -   FLT_MAX_EXP                          32
 *      -   FLT_MAX                 3.40282347E+38F
 *      -   FLT_MAX_10_EXP                      +38
 *
 * 23. EXAMPLE 2 The following describes floating-point representations that also meet the
 *     requirements for single-precision and double-precision numbers in IEC 60559, and the
 *     appropriate values in a <float.h> header for types float and double:
 *                           24
 *          x_f = s * 2^e * SUM f_k * 2^(-k),   -125 <= e <= +128
 *                          k=1
 *                           53
 *          x_d = s * 2^3 * SUM f_k * 2^(-k),   -1021 <= e <= +1024
 *                          k=1
 *      - FLT_IS_IEC_60559                        2
 *      - FLT_RADIX                               2
 *      - FLT_MANT_DIG                           24
 *      - FLT_EPSILON               1.19209290E-07F // decimal constant
 *      - FLT_EPSILON                      0X1P-23F // hex constant
 *      - FLT_DECIMAL_DIG                         9
 *      - FLT_DIG                                 6
 *      - FLT_MIN_EXP                          -125
 *      - FLT_MIN                   1.17549435E-38F // decimal constant
 *      - FLT_MIN                         0X1P-126F // hex constant
 *      - FLT_TRUE_MIN              1.40129846E-45F // decimal constant
 *      - FLT_TRUE_MIN                    0X1P-149F // hex constant
 *      - FLT_HAS_SUBNORM                         1
 *      - FLT_MIN_10_EXP                        -37
 *      - FLT_MAX_EXP                          +128
 *      - FLT_MAX                   3.40282347E+38F // decimal constant
 *      - FLT_MAX                   0X1.fffffeP127F // hex constant
 *      - FLT_MAX_10_EXP                        +38
 *      - DBL_IS_IEC_60559                        2
 *      - DBL_EPSILON        2.2204460492503131E-16 // decimal constant
 *      - DBL_EPSILON                       0X1P-52 // hex constant
 *      - DBL_DECIMAL_DIG                        17
 *      - DBL_DIG                                15
 *      - DBL_MIN_EXP                         -1021
 *      - DBL_MIN           2.2250738585072014E-308 // decimal constant
 *      - DBL_MIN                         0X1P-1022 // hex constant
 *      - DBL_TRUE_MIN      4.9406564584124654E-324 // decimal constant
 *      - DBL_TRUE_MIN                    0X1P-1074 // hex constant
 *      - DBL_HAS_SUBNORM                         1
 *      - DBL_MIN_10_EXP                       -307
 *      - DBL_MAX_EXP                         +1024
 *      - DBL_MAX          1.79769331348623157E+308 // decimal constant
 *      - DBL_MAX            0X1.fffffffffffffP1023 // hex constant
 *      - DBL_MAX_10_EXP                       +308
 *
 * Forward references: conditional includsion (6.10.1), predefined macro names (6.10.8), complex
 * arithmetic <complex.h> (7.3>, extended multibyte and wide character utilities <wchar.h> (7.29),
 * floating-point environment <fenv.h> (7.6), general utilities <stdlib.h> (7.22), input/output
 * <stdio.h> (7.21), mathematics <math.h> (7.12), IEC 60559 floating-point arithmetic (Annex F),
 * IEC 60559-compatible complex arithmetic (Annex G).
 */

/*
 * 5.2.4.2.3 Characteristics of decimal floating types in <float.h>
 *
 * 1. This subclause specifies macros in <float.h> that provide characteristics of decimal floating
 *    types in terms of the model presented in 5.2.4.2.2. An implementation that does not support
 *    decimal floating types shall not provide these macros. The prefixes DEC32_, DEC64_, and DEC128_
 *    denote the types _Decimal32, _Decimal64, and _Decimal128 respectively.
 * 2. DEC_EVAL_METHOD is the decimal floating-point analog of FLT_EVAL_METHOD (5.2.4.2.2). Its
 *    implementation-defined value characterizes the use of evaluation formats for decimal floating
 *    types:
 *     -1   indeterminable;
 *      0   evaluate all operations and constants just to the range and precision of the type;
 *      1   evaluate all operations and constants of type _Decimal32 and _Decimal64 to the range and
 *          precision of the _Decimal64 type, evaluate _Decimal128 operations and constants to the
 *          range and precision of the _Decimal128 type;
 *      2   evaluate all operations and constants to the range and precision of the _Decimal128 type.
 */
#define DEC_EVAL_METHOD __DEC_EVAL_METHOD__

/*
 * 3.  The decimal signaling NaN macros
 * -     D32_SNAN
 * -     D64_SNAN
 * -     D128_SNAN
 *     each expands to a constant expression of the respective decimal floating type representing a
 *     signaling NaN. If an optional unary + or - operator followed byy a signaling NaN macro is used
 *     for initializing an object of the same type that has static or thread-local storage duration,
 *     the object is initialized with a signaling NaN value.
 * 4.  The macro
 * -     DEC_INFINITY
 *     expands to a constant expression of type _Decimal32 representing positive infinity.
 * 5.  The macro
 * -     DEC_NAN
 *     expands to a constant expression of type _Decimal32 representing a quiet NaN.
 */
/*
 *
 * #define D32_SNAN
 * #define D64_SNAN
 * #define D128_SNAN
 *
 * #define DEC_INFINITY
 * #define DEC_NAN
 */

#if defined(__STDC_VERSION__) && (__STDC_VERSION__ > 201710L)
#    define D32_SNAN     __builtin_nansd32("")
#    define D64_SNAN     __builtin_nansd64("")
#    define D128_SNAN    __builtin_nansd128("")

#    define DEC_INFINITY __builtin_inf32()
#    define DEC_NAN      __builtin_nanf("")
#endif

/*
 * 6.  The integer values given in the following lists shall be replaced by constant expression
 *     suitable for use in #if preprocessing directives:
 *      - radix of exponent representation, b(=10)
 *        For the standard floating types, this value is implementation-defined and is specified by
 *        the macro FLT_RADIX. For the decimal floating types there is no corresponding macro, since
 *        the value 10 is an inherent property of the types. Wherever FLT_RADIX appears in a
 *        description of a function that has versions that operate on decimal floating types, it is
 *        noted that for the decimal floating-point versions the value used is implicitly 10, rather
 *        than FLT_RADIX.
 *      - numbers of digits in the coefficient
 *      -    DEC32_MANT_DIG      7
 *      -    DEC64_MANT_DIG      16
 *      -    DEC128_MANT_DIG     34
 */
#ifdef __DEC32_MANT_DIG__
#    define DEC32_MANT_DIG __DEC32_MANT_DIG__
#else
#    define DEC32_MANT_DIG 7
#endif
#ifdef __DEC64_MANT_DIG__
#    define DEC64_MANT_DIG __DEC64_MANT_DIG__
#else
#    define DEC64_MANT_DIG 16
#endif
#ifdef __DEC128_MANT_DIG__
#    define DEC128_MANT_DIG __DEC128_MANT_DIG__
#else
#    define DEC128_MANT_DIG 34
#endif

/*
 *      -    DEC32_MIN_EXP       -94
 *      -    DEC64_MIN_EXP       -382
 *      -    DEC128_MIN_EXP      -6142
 */
#ifdef __DEC32_MIN_EXP__
#    define DEC32_MIN_EXP __DEC32_MIN_EXP__
#else
#    define DEC32_MIN_EXP (-94)
#endif
#ifdef __DEC64_MIN_EXP__
#    define DEC64_MIN_EXP __DEC64_MIN_EXP__
#else
#    define DEC64_MIN_EXP (-382)
#endif
#ifdef __DEC128_MIN_EXP__
#    define DEC128_MIN_EXP __DEC128_MIN_EXP__
#else
#    define DEC128_MIN_EXP (-6142)
#endif

/*
 *      -    DEC32_MAX_EXP       97
 *      -    DEC64_MAX_EXP       385
 *      -    DEC128_MAX_EXP      6145
 */
#ifdef __DEC32_MAX_EXP__
#    define DEC32_MAX_EXP __DEC32_MAX_EXP__
#else
#    define DEC32_MAX_EXP 97
#endif
#ifdef __DEC64_MAX_EXP__
#    define DEC64_MAX_EXP __DEC64_MAX_EXP__
#else
#    define DEC64_MAX_EXP 385
#endif
#ifdef __DEC128_MAX_EXP__
#    define DEC128_MAX_EXP __DEC128_MAX_EXP__
#else
#    define DEC128_MAX_EXP 6145
#endif

/*
 *      - maximum representable finite decimal floating-point numbers (there are 6, 15 and 33 9's
 *        after the decimal points respectively)
 *      -    DEC32_MAX           9.999999E96DF
 *      -    DEC64_MAX           9.999999999999999E384DD
 *      -    DEC128_MAX          9.999999999999999999999999999999999E6144D
 */
#ifdef __DEC32_MAX__
#    define DEC32_MAX __DEC32_MAX__
#else
#    define DEC32_MAX (9.999999e96DF)
#endif
#ifdef __DEC64_MAX__
#    define DEC64_MAX __DEC64_MAX__
#else
#    define DEC64_MAX (9.999999999999999E384DD)
#endif
#ifdef __DEC128_MAX__
#    define DEC128_MAX __DEC128_MAX__
#else
#    define DEC128_MAX (9.999999999999999999999999999999999E6144DL)
#endif

/*
 *      - the difference between 1 and the least value greater than 1 that is representable in the
 *        given floating type
 *      -    DEC32_EPSILON       1E-6DF
 *      -    DEC64_EPSILON       1E-15DD
 *      -    DEC128_EPSILON      1E-33DL
 */
#ifdef __DEC32_EPSILON__
#    define DEC32_EPSILON __DEC32_EPSILON__
#else
#    define DEC32_EPSILON (1e-6DF)
#endif
#ifdef __DEC64_EPSILON__
#    define DEC64_EPSILON __DEC64_EPSILON__
#else
#    define DEC64_EPSILON (1e-15DD)
#endif
#ifdef __DEC128_EPSILON__
#    define DEC128_EPSILON __DEC128_EPSILON__
#else
#    define DEC128_EPSILON (1e-33DL)
#endif

/*
 *      - minimum normalized positive decimal floating-point number
 *      -    DEC32_MIN       1E-95DF
 *      -    DEC64_MIN       1E-383DD
 *      -    DEC128_MIN      1E-6143Dl
 */
#ifdef __DEC32_MIN__
#    define DEC32_MIN __DEC32_MIN__
#else
#    define DEC32_MIN (1e-95DF)
#endif
#ifdef __DEC64_MIN__
#    define DEC64_MIN __DEC64_MIN__
#else
#    define DEC64_MIN (1e-383DD)
#endif
#ifdef __DEC128_MIN__
#    define DEC128_MIN __DEC128_MIN__
#else
#    define DEC128_MIN (1e-6143DL)
#endif

/*
 *      - minimum positive subnormal decimal floating-point number
 *      -    DEC32_TRUE_MIN  0.000001E-95DF
 *      -    DEC64_TRUE_MIN  0.000000000000001E-383D
 *      -    DEC128_TRUE_MIN 0.000000000000000000000000000000001E-6143D
 */
#ifdef __DEC32_SUBNORMAL_MIN__
#    define DEC32_TRUE_MIN __DEC32_SUBNORMAL_MIN__
#else
#    define DEC32_TRUE_MIN (0.000001e-95DF)
#endif
#ifdef __DEC64_SUBNORMAL_MIN__
#    define DEC64_TRUE_MIN __DEC64_SUBNORMAL_MIN__
#else
#    define DEC64_TRUE_MIN (0.000000000000001E-383DD)
#endif
#ifdef __DEC128_SUBNORMAL_MIN__
#    define DEC128_TRUE_MIN __DEC128_SUBNORMAL_MIN__
#else
#    define DEC128_TRUE_MIN (0.000000000000000000000000000000001E-6143DL)
#endif

/*
 * 7.  For decimal floating-point arithmetic, it is often convenient to consider an alternative
 *     equivalent model where the significand is represented with integer rather than fraction digits.
 *     With s, b, e, p, and f_k as defined in 5.2.4.2.2, a floating point number x is defined by the
 *     model:
 *                                p
 *           x = s * b^(e - p) * SUM f_k * b^(p - k)
 *                               k=1
 * 8.  With b fixed to 10, a decimal floating-point number x is thus:
 *                                 p
 *           x = s * 10^(e - p) * SUM f_k * 10^(p - k)
 *                                k=1
 *     The quantum exponent is q = e - p and the coefficient is c = f_1f_2...f_p, which is a integer
 *     between 0 and 10^(p - 1), inclusive. Thus, x = s * c * 10^q is represented by the triple of
 *     integers (s, c, q). The quantum of x is 10^q, which is the value of a unit in hte last place of
 *     the coefficient.
 *                                  Quantum exponent ranges
 *      ___________________________________________________________________________
 *     | Type                             | _Decimal32 | _Decimal64 | _Decimal128 |
 *     |__________________________________|____________|____________|_____________|
 *     | Maximum Quantum Exponent (q_max) |     90     |     369    |     6111    |
 *     | Minimum Quantum Exponent (q_min) |    -101    |    -398    |    -6176    |
 *     |__________________________________|____________|____________|_____________|
 * 9.  For binary floating-point arithmetic following IEC 60559, representations in hte model described
 *     in 5.2.4.2.2 that have the same numerical value are indistinguishable in the arithmetic.
 *     However, for decimal floating-point arithmetic, representations that have the same numerical
 *     value but different quantum exponents, e.g., (+1, 10, -1) representing 1.0 and (+1, 100, -2)
 *     representing 1.00, are distinguishable. To facilitate exact fixed-point calculation, operation
 *     results that are of decimal floating type have a preferred quantum exponent, as specified in
 *     IEC 60559, which is determined by the quantum exponents of the operands if they have decimal
 *     floating types (or by specific rules for conversions from other types). The table below gives
 *     rules for determining preferred quantum exponents for results of IEC 60559 operations, and for
 *     other operations specified in this document. When exact, these operations produce a result with
 *     their preferred quantum exponent, or as close to it as possible within the limitations of the
 *     type. When inexact, these operations produce a result with the least possible quantum exponent.
 *     For example, the preferred quantum exponent for addition is the minimum of the quantum exponents
 *     of the operands. Hence (+1, 123, -2) + (+1, 4000, -3) = (+1, 5230, -3) or 1.23 + 4.000 = 5.230.
 * 10. The following table, shows, for each operation delivering a result in decimal floating-point
 *     format, how the preferred quantum exponents of the operands, Q(x), Q(y), etc., determine the
 *     preferred quantum exponent of the operation result.
 *
 *      __________________________________________________________________________________
 *     | Operation                              | Preferred quantum exponent of result   |
 *     |________________________________________|________________________________________|
 *     | roundeven, round, trunc, ceil, floor,  | max(Q(x),0)                            |
 *     | rint, nearbyint                        |                                        |
 *     |----------------------------------------|----------------------------------------|
 *     |nextup, nextdown, nextafter, nexttoward | least possible                         |
 *     |----------------------------------------|----------------------------------------|
 *     | remainder                              | min(Q(x), Q(y))                        |
 *     |----------------------------------------|----------------------------------------|
 *     | fmin, fmax, fminimum, fmaximum,        | Q(x) if x gives the result, Q(y) if y  |
 *     | fminimum_mag, fmaximum_mag,            | gives the result                       |
 *     | fminimum_num, fmaximum_num,            |                                        |
 *     | fminimum_mag_num, fminimum_mag_num     |                                        |
 *     |----------------------------------------|----------------------------------------|
 *     | scalbn, scalbln                        | Q(x) + n                               |
 *     |----------------------------------------|----------------------------------------|
 *     | ldexp                                  | Q(x) + p                               |
 *     |----------------------------------------|----------------------------------------|
 *     | logb                                   | 0                                      |
 *     |----------------------------------------|----------------------------------------|
 *     | +, d32add, d64add                      | min(Q(x), Q(y))                        |
 *     |----------------------------------------|----------------------------------------|
 *     | -, d32sub, d64sub                      | min(Q(x), Q(y))                        |
 *     |----------------------------------------|----------------------------------------|
 *     | *, d32mul, d64mul                      | Q(x) + Q(y)                            |
 *     |----------------------------------------|----------------------------------------|
 *     | /, d32div, d64div                      | Q(x) - Q(y)                            |
 *     |----------------------------------------|----------------------------------------|
 *     | sqrt, d32sqrt, d64sqrt                 | floor(Q(x) / 2)                        |
 *     |----------------------------------------|----------------------------------------|
 *     | fma, d32fma, d64fma                    | min(Q(x) + Q(y), Q(z))                 |
 *     |----------------------------------------|----------------------------------------|
 *     | conversion from integer type           | 0                                      |
 *     |----------------------------------------|----------------------------------------|
 *     | exact conversion from non-decimal      | 0                                      |
 *     | floating type                          |                                        |
 *     |----------------------------------------|----------------------------------------|
 *     | inexact conversion from non-decimal    | least possible                         |
 *     | floating type                          |                                        |
 *     |----------------------------------------|----------------------------------------|
 *     | conversion between decimal floating    | Q(x)                                   |
 *     | types                                  |                                        |
 *     |----------------------------------------|----------------------------------------|
 *     | *cx returned by canonicalize           | Q(*x)                                  |
 *     |----------------------------------------|----------------------------------------|
 *     | strto, wcsto, scanf, floating          | see 7.22.1.6                           |
 *     | constants of decimal floating type     |                                        |
 *     |----------------------------------------|----------------------------------------|
 *     | -(x), +(x)                             | Q(x)                                   |
 *     |----------------------------------------|----------------------------------------|
 *     | fabs                                   | Q(x)                                   |
 *     |----------------------------------------|----------------------------------------|
 *     | copysign                               | Q(x)                                   |
 *     |----------------------------------------|----------------------------------------|
 *     | quantize                               | Q(y)                                   |
 *     |----------------------------------------|----------------------------------------|
 *     | quantum                                | Q(x)                                   |
 *     |----------------------------------------|----------------------------------------|
 *     | *encptr returned by encodedec,         | Q(*xptr)                               |
 *     | encodedbin                             |                                        |
 *     |----------------------------------------|----------------------------------------|
 *     | *xptr returned by decodedec,           | Q(*encptr)                             |
 *     | decodebin                              |                                        |
 *     |----------------------------------------|----------------------------------------|
 *     | fmod                                   | min(Q(x), Q(y))                        |
 *     |----------------------------------------|----------------------------------------|
 *     | fdim                                   | min(Q(x), Q(y)) if x > y, 0 if x <= y  |
 *     |----------------------------------------|----------------------------------------|
 *     | cbrt                                   | floor(Q(x) / 3)                        |
 *     |----------------------------------------|----------------------------------------|
 *     | hypot                                  | min(Q(x), Q(y))                        |
 *     |----------------------------------------|----------------------------------------|
 *     | pow                                    | floor(y * Q(x))                        |
 *     |----------------------------------------|----------------------------------------|
 *     | modf                                   | Q(value)                               |
 *     |----------------------------------------|----------------------------------------|
 *     | *iptr returned by modf                 | max(Q(value), 0)                       |
 *     |----------------------------------------|----------------------------------------|
 *     | frexp                                  | Q(value) if value = 0, -(length of     |
 *     |                                        | coefficient of value) otherwise        |
 *     |----------------------------------------|----------------------------------------|
 *     | *res returned by setpayload,           | 0 if pl does not represent a valid     |
 *     | setpayloadsig                          | payload, not applicable otherwise (NaN |
 *     |                                        | returned)                              |
 *     |----------------------------------------|----------------------------------------|
 *     | getpayload                             | 0 if *x is NaN, unspecified otherwise  |
 *     |----------------------------------------|----------------------------------------|
 *     | compoundn                              | floor(n * min(0, Q(x)))                |
 *     |----------------------------------------|----------------------------------------|
 *     | pown                                   | floor(n * Q(x))                        |
 *     |----------------------------------------|----------------------------------------|
 *     | powr                                   | floor(y * Q(x))                        |
 *     |----------------------------------------|----------------------------------------|
 *     | rootn                                  | floor(Q(x) / n)                        |
 *     |----------------------------------------|----------------------------------------|
 *     | rsqrt                                  | -floor(Q(x) / 2)                       |
 *     |----------------------------------------|----------------------------------------|
 *     | transcendental functions               | 0                                      |
 *     |________________________________________|________________________________________|
 *     A function family listed in the table above indicates the functions for all decimal floating
 *     types, where the function family is represented by the name of the functions without a suffix.
 *     For example, ceil indicates the functions ceild32, ceild64, and ceild128.
 * Forward references: extended multibyte and wide character utilities <wchar.h>(7.29), floating-point
 * environment <fenv.h> (7.6), general utilities <stdlib.h> (7.22), input/output <stdio.h> (7.21),
 * mathematics <math.h> (7.12), type-generic mathematics <tgmath.h> (7.25), IEC 60559 floating-point
 * arithmetic (Annex F).
 */
#endif /* SOLC_FLOAT_H */
