#ifndef SOLC_CTYPE_H
#define SOLC_CTYPE_H

#include <sys/cdefs.h>

__BEGIN_DECLS
/*
 * 7.4 Character handling <ctype.h>
 *
 * 1. The header <ctype.h> declares several functions useful for classifying and mapping characters.
 *    In all cases the argument is an int, the value of which shall be representable as an
 *    unsigned char or shall equal the value of the macro EOF. If the argument has any other value,
 *    the behavior is undefined.
 * 2. The behaviour of these functions is affected by the current locale. Those functions that have
 *    locale-specific aspects only when not in the "C" locale are noted below.
 * 3. The term printing character refers to a member of a locale-specific set of characters, each of
 *    which occupies one printing position on a display device; the term control character refers to
 *    a member of a locale-specific set of characters that are not printing characters. All letters
 *    and digits are printing characters.
 *    Forward references: EOF (7.21.1), localization (7.11).
 */

/*
 * 7.4.1 Character classification functions
 *
 * 1. The functions in this subclause return nonzero (true) if and only if the value of the
 *    argument c conforms to that in the description of the function.
 */

/*
 * 7.4.1.1 The isalnum function
 *  Synopsis
 * 1. ________________________
 *    | #include <ctype.h>   |
 *    | int isalnum(int c);  |
 *    |______________________|
 *  Description
 * 2. The isalnum function tests for any character for which isalpha or isdigit is true.
 */
int isalnum(int c);
/*
 * 7.4.1.2 The isalpha function
 *  Synopsis
 * 1. ________________________
 *    | #include <ctype.h>   |
 *    | int isalpha(int c);  |
 *    |______________________|
 *  Description
 * 2. The isalpha function tests for any character for which issupper or islower is true, or any
 *    character that is one of a locale-specific set of alphabetic characters for which none of
 *    iscntrl, isdigit, ispunct, or isspace is true. In the "C" locale, isalpha returns true only
 *    for the characters which issupper or islower is true.
 */
int isalpha(int c);
/*
 * 7.4.1.3 The isblank function
 *  Synopsis
 * 1. ________________________
 *    | #include <ctype.h>   |
 *    | int isblank(int c);  |
 *    |______________________|
 *  Description
 * 2. The isblank function tests for any character that is a standard blank character or is one of a
 *    locale specific set of characters for which isspace is true and that is used to separate words
 *    within a line of text. The standard blank characters are the following: space (' '), and
 *    horizontal tab ('\t'). In the "C" locale, isblank returns true only for the standard blank
 *    characters.
 */
int isblank(int c);
/*
 * 7.4.1.4 The iscntrl function
 *  Synopsis
 * 1. ________________________
 *    | #include <ctype.h>   |
 *    | int iscntrl(int c);  |
 *    |______________________|
 *  Description
 * 2. The iscntrl function tests for any control character.
 */
int iscntrl(int c);
/*
 * 7.4.1.5 The isdigit function
 *  Synopsis
 * 1. ________________________
 *    | #include <ctype.h>   |
 *    | int isdigit(int c);  |
 *    |______________________|
 *  Description
 * 2. The isdigit function tests for any decimal-digit character (as defined in 5.2.1).
 */
int isdigit(int c);
/*
 * 7.4.1.6 The isgraph function
 *  Synopsis
 * 1. ________________________
 *    | #include <ctype.h>   |
 *    | int isgraph(int c);  |
 *    |______________________|
 *  Description
 * 2. The isgraph function tests for any printing character except space (' ').
 */
int isgraph(int c);
/*
 * 7.4.1.7 The islower function
 *  Synopsis
 * 1. ________________________
 *    | #include <ctype.h>   |
 *    | int islower(int c);  |
 *    |______________________|
 *  Description
 * 2. The islower function tests for any character that is a lowercase letter or is one of a
 *    locale-specific set of characters for which none of iscntrl, isdigit, ispunct, or isspace is
 *    true. In the "C" locale, islower returns true only for the lowercase letters (as defined in 5.2.1).
 */
int islower(int c);
/*
 * 7.4.1.8 The isprint function
 *  Synopsis
 * 1. ________________________
 *    | #include <ctype.h>   |
 *    | int isprint(int c);  |
 *    |______________________|
 *  Description
 * 2. The isprint function tests for any printing character including space (' ').
 */
int isprint(int c);
/*
 * 7.4.1.9 The ispunct function
 *  Synopsis
 * 1. ________________________
 *    | #include <ctype.h>   |
 *    | int ispunct(int c);  |
 *    |______________________|
 *  Description
 * 2. The ispunct function tests for any printing character that is one of a locale-specific set of
 *    punctuation characters for which neither isspace nor isalnum is true. In the "C" locale, ispunct
 *    returns true for every printing character for which neither isspace nor isalnum is true.
 */
int ispunct(int c);
/*
 * 7.4.1.10 The isspace function
 *  Synopsis
 * 1. ________________________
 *    | #include <ctype.h>   |
 *    | int isspace(int c);  |
 *    |______________________|
 *  Description
 * 2. The isspace function tests for any character that is a standard white-space character or is one
 *    of a locale-specific set of characters for which isalnum is false. The standard white-space
 *    characters are the following: space (' '), form feed ('\f'), new-line ('\n'), carriage return
 *    ('\r'), horizontal tab ('\t'), and vertical tab ('\v'). In the "C" locale, isspace returns true
 *    only for the standard white-space characters.
 */
int isspace(int c);
/*
 * 7.4.1.11 The isupper function
 *  Synopsis
 * 1. ________________________
 *    | #include <ctype.h>   |
 *    | int isupper(int c);  |
 *    |______________________|
 *  Description
 * 2. The isupper function tests for any character that is an uppercase letter or is one of a
 *    locale-specific set of characters for which none of iscntrl, isdigit, ispunct, or isspace is
 *    true. In the "C" locale, isupper returns true only for the uppercase letters (as defined in 5.2.1).
 */
int isupper(int c);
/*
 * 7.4.1.12 The isxdigit function
 *  Synopsis
 * 1. ________________________
 *    | #include <ctype.h>   |
 *    | int isxdigit(int c); |
 *    |______________________|
 *  Description
 * 2. The isxdigit function tests for any hexadecimal-digit character (as defined in 6.4.4.1).
 */
int isxdigit(int c);

/*
 * 7.4.2 Character case mapping functions
 */

/**
 * 7.4.2.1 The tolower function
 *  Synopsis
 * 1. ________________________
 *    | #include <ctype.h>   |
 *    | int tolower(int c);  |
 *    |______________________|
 *  Description
 * 2. The tolower function converts an uppercase letter to a corresponding lowercase letter.
 *  Returns
 * 3. If the argument is a character for which isupper is true and there are one or more corresponding
 *    characters, as specified by the current locale, for which islower is true, the tolower function
 *    returns one of the corresponding characters (always the same one for any given locale); otherwise,
 *    the argument is returned unchanged.
 */
int tolower(int c);
/*
 * 7.4.2.2 The toupper function
 *  Synopsis
 * 1. ________________________
 *    | #include <ctype.h>   |
 *    | int toupper(int c);  |
 *    |______________________|
 *  Description
 * 2. The toupper function converts a lowercase letter to a corresponding uppercase letter.
 *  Returns
 * 3. If the argument is a character for which islower is true and there are one or more corresponding
 *    characters, as specified by the current locale, for which isupper is true, the toupper function
 *    returns one of the corresponding characters (always the same one for any given locale); otherwise,
 *    the argument is returned unchanged.
 */
int toupper(int c);

/* TODO implement and check for locale specific things */

/* POSIX extensions */

/* The <ctype.h> header shall define the following as macros: */

/* https://pubs.opengroup.org/onlinepubs/9699919799.2018edition/functions/_toupper.html# */
#define _toupper(c) (((c) >= 'a' && (c) <= 'z') ? ((c) & (~0x20)) : (c))
/* https://pubs.opengroup.org/onlinepubs/9699919799.2018edition/functions/_tolower.html# */
#define _tolower(c) (((c) >= 'A' && (c) <= 'Z') ? ((c) | 0x20) : (c))
/* https://pubs.opengroup.org/onlinepubs/9699919799.2018edition/functions/isascii.html# */
int isascii(int c);
/* https://pubs.opengroup.org/onlinepubs/9699919799.2018edition/functions/toascii.html# */
int toascii(int c);

__END_DECLS

#endif /* SOLC_CTYPE_H */
