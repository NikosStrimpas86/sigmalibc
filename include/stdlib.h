#ifndef SOLC_STDLIB_H
#define SOLC_STDLIB_H

#include <sys/cdefs.h>
#include <internal/defines/qualifier_preserving.h>

#define __STDC_VERSION_STDLIB_H__ 202111L /* no one seems to define this though */
#include <internal/defines/null.h>
#include <internal/types/size_t.h>
#include <internal/types/wchar_t.h>
/* extension of 32V */
#include <alloca.h>
/* Microsoft extension */
#define _countof(array) (sizeof(array) / sizeof(array[0]))

__BEGIN_DECLS

typedef struct {
    int quot;
    int rem;
} div_t;

typedef struct {
    long int quot;
    long int rem;
} ldiv_t;

typedef struct {
    long long int quot;
    long long int rem;
} lldiv_t;

#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0

#define RAND_MAX 0x7fff

/* #define MB_CUR_MAX (size_t)**something**  // TODO: find its value */

double atof(const char* nptr);

int atoi(const char* nptr);
long int atol(const char* nptr);
long long int atoll(const char* nptr);

int strfromd(char* restrict s, size_t n, const char* restrict format, double fp);
int strfromf(char* restrict s, size_t n, const char* restrict format, float fp);
int strfroml(char* restrict s, size_t n, const char* restrict format, long double fp);
#ifdef __STDC_IEC_60559_DFP__
int strfrom32(char* restrict s, size_t n, const char* restrict format, _Decimal32 fp);
int strfrom64(char* restrict s, size_t n, const char* restrict format, _Decimal64 fp);
int strfrom128(char* restrict s, size_t n, const char* restrict format, _Decimal128 fp);
#endif /* __STDC_IEC_60559_DFP__ */

double strtod(const char* restrict nptr, char** restrict endptr);
float strtof(const char* restrict nptr, char** restrict endptr);
long double strtold(const char* restrict nptr, char** restrict endptr);
#ifdef __STDC_IEC_60559_DFP__
_Decimal32 strtod32(const char* restrict nptr, char** restrict endptr);
_Decimal64 strtod64(const char* restrict nptr, char** restrict endptr);
_Decimal128 strtod128(const char* restrict nptr, char** restrict endptr);
#endif /* __STDC_IEC_60559_DFP__ */

long int strtol(const char* restrict nptr, char** restrict endptr, int base);
long long int strtoll(const char* restrict nptr, char** restrict endptr, int base);
unsigned long int strtoul(const char* restrict nptr, char** restrict endptr, int base);
unsigned long long int strtoull(const char* restrict nptr, char** restrict endptr, int base);

int rand(void);
void srand(unsigned int seed);

void* aligned_alloc(size_t alignment, size_t size);
void* calloc(size_t nmemb, size_t size);
void free(void* ptr);
void* malloc(size_t size);
void* realloc(void* ptr, size_t size);

_Noreturn void abort(void);
int atexit(void (*func)(void));
int at_quick_exit(void (*func)(void));
_Noreturn void exit(int status);
_Noreturn void _Exit(int status);
char* getenv(const char* name);
_Noreturn void quick_exit(int status);
int system(const char* string);

void* bsearch(const void* key, const void* base, size_t nmemb, size_t size, int (*compar)(const void*, const void*));
#if ((!defined(__cplusplus)) && (defined(__STDC_VERSION__) && __STDC_VERSION__ > 201710L))
#define bsearch(key, base, nmemb, size, compar) _BINARY_SEARCH_QP(void, bsearch, (key), (base), (nmemb), (size), (compar))
#endif
void qsort(void* base, size_t nmemb, size_t size, int (*compar)(const void*, const void*));

int abs(int j);
long int labs(long int j);
long long int llabs(long long int j);

div_t div(int numer, int denom);
ldiv_t ldiv(long int numer, long int denom);
lldiv_t lldiv(long long int numer, long long int denom);

int mblen(const char* s, size_t n);
int mbtowc(wchar_t* restrict pwc, const char* restrict s, size_t n);
int wctomb(char* s, wchar_t wc);

size_t mbstowcs(wchar_t* restrict pwcs, const char* restrict s, size_t n);
size_t wcstombs(char* restrict s, const wchar_t* restrict pwcs, size_t n);

__END_DECLS

#endif /* SOLC_STDLIB_H */
