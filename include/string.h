#ifndef SOLC_STRING_H
#define SOLC_STRING_H

#include <internal/defines/null.h>
#include <internal/defines/qualifier_preserving.h>
#include <internal/types/size_t.h>
#include <sys/cdefs.h>

__BEGIN_DECLS

void* memcpy(void* restrict s1, const void* restrict s2, size_t);
void* memccpy(void* restrict s1, const void* restrict s2, int c, size_t n);
void* memmove(void* s1, const void* s2, size_t n);
char* strcpy(char* restrict s1, const char* restrict s2);
char* strncpy(char* restrict s1, const char* restrict s2, size_t n);
char* strdup(const char* s);
char* strndup(const char* s, size_t size);
char* strcat(char* restrict s1, const char* restrict s2);
char* strncat(char* restrict s1, const char* restrict s2, size_t n);

int memcmp(const void* s1, const void* s2, size_t n);
int strcmp(const char* s1, const char* s2);
int strcoll(const char* s1, const char* s2);
size_t strxfrm(char* restrict s1, const char* restrict s2, size_t n);

void* memchr(const void* s, int c, size_t n);
char* strchr(const char* s, int c);
size_t strcspn(const char* s1, const char* s2);
char* strpbrk(const char* s1, const char* s2);
char* strrchr(const char* s, int c);
size_t strspn(const char* s1, const char* s2);
char* strstr(const char* s1, const char* s2);
char* strtok(char* restrict s1, const char* restrict s2);

#if ((!defined(__cplusplus)) && (defined(__STDC_VERSION__) && __STDC_VERSION__ > 201710L))
#    define memchr(s, c, n) _STRING_SEARCH_QP(void, memchr, (s), (c), (n))
#    define strchr(s, c)    _STRING_SEARCH_QP(char, strchr, (s), (c))
#    define strpbrk(s1, s2) _STRING_SEARCH_QP(char, strpbrk, (s1), (s2))
#    define strrchr(s, c)   _STRING_SEARCH_QP(char, strrchr, (s), (c))
#    define strstr(s1, s2)  _STRING_SEARCH_QP(char, strstr, (s1), (s2))
#endif

void* memset(void* s, int c, size_t n);
void* memset_explicit(void* s, int c, size_t n);
char* strerror(int errnum);
size_t strlen(const char* s);

/* POSIX extensions */
size_t strnlen(const char* s, size_t maxlen);

/* GNU extensions */
void* mempcpy(void* s1, void const* s2, size_t n);
void* memrchr(void const* s, int c, size_t n);
void* memfrob(void* s, size_t n);

/* BSD extensions */
void explicit_bzero(void* s, size_t n);

__END_DECLS

#endif /* SOLC_STRING_H */
