#ifndef SOLC_ALLOCA_H
#define SOLC_ALLOCA_H

#ifdef alloca
#    undef alloca
#endif

/*
 * Since alloca is both architecture and OS specific,
 * we rely on the compiler and its builtin implementation.
 */

/*
 * The alloca function
 *  Synopsis
 * 1. ______________________________
 *    | #include <alloca.h>        |
 *    | // Or #include <stdlib.h>  |
 *    | void* alloca(size_t size); |
 *    |____________________________|
 *  Description
 * 2. The alloca() function allocates size bytes of space in the stack
 *    frame of the caller. This temporary space is automatically freed
 *    when the function that called alloca() returns to its caller.
 *  Returns
 * 3. The alloca() function returns a pointer to the beginning of the
 *    allocated space. If the allocation causes stack overflow,
 *    program behavior is undefined.
 */
#define alloca(size) __builtin_alloca(size)

#endif /* SOLC_ALLOCA_H */
