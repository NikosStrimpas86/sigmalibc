#ifndef SOLC_STDBIT_H
#define SOLC_STDBIT_H

/*
 * TODO: Implement type generic macros.
 * TODO: Add support for _BitInt in the type generic macros.
 */

#include <internal/types/size_t.h>
#include <stdint.h>
#include <sys/cdefs.h>

#define __STDC_VERSION_STDBIT_H__ 202311L

#ifndef __cplusplus
/* clang-format off */
#define __STDC_GENERIC_BIT_UTILITY(name, argument) \
    _Generic((argument)                            \
        , unsigned char: name ## uc                \
        , unsigned short: name ## us               \
        , unsigned int: name ## ui                 \
        , unsigned long: name ## ul                \
        , unsigned long long: name ## ull          \
    )(argument)
/* clang-format on */
#endif /* __cplusplus */

#ifdef __ORDER_LITTLE_ENDIAN__
#    define __STDC_ENDIAN_LITTLE__ __ORDER_LITTLE_ENDIAN__
#else
#    define __STDC_ENDIAN_LITTLE__ 1234
#endif
#ifdef __ORDER_BIG_ENDIAN__
#    define __STDC_ENDIAN_BIG__ __ORDER_BIG_ENDIAN__
#else
#    define __STDC_ENDIAN_BIG__ 4321
#endif
#ifdef __BYTE_ORDER__
#    define __STDC_ENDIAN_NATIVE__ __BYTE_ORDER__
#else
/* Assume little endian for the time being */
#    define __STDC_ENDIAN_NATIVE__ __STDC_ENDIAN_LITTLE__
#endif

__BEGIN_DECLS

int stdc_leading_zerosuc(unsigned char value);
int stdc_leading_zerosus(unsigned short value);
int stdc_leading_zerosui(unsigned int value);
int stdc_leading_zerosul(unsigned long value);
int stdc_leading_zerosull(unsigned long long value);
#ifndef __cplusplus
#    define stdc_leading_zeros(value) __STDC_GENERIC_BIT_UTILITY(stdc_leading_zeros, value)
#endif /* __cplusplus */

int stdc_leading_onesuc(unsigned char value);
int stdc_leading_onesus(unsigned short value);
int stdc_leading_onesui(unsigned int value);
int stdc_leading_onesul(unsigned long value);
int stdc_leading_onesull(unsigned long long value);
#ifndef __cplusplus
#    define stdc_leading_ones(value) __STDC_GENERIC_BIT_UTILITY(stdc_leading_ones, value)
#endif /* __cplusplus */

int stdc_trailing_zerosuc(unsigned char value);
int stdc_trailing_zerosus(unsigned short value);
int stdc_trailing_zerosui(unsigned int value);
int stdc_trailing_zerosul(unsigned long value);
int stdc_trailing_zerosull(unsigned long long value);
#ifndef __cplusplus
#    define stdc_trailing_zeros(value) __STDC_GENERIC_BIT_UTILITY(stdc_trailing_zeros, value)
#endif /* __cplusplus */

int stdc_trailing_onesuc(unsigned char value);
int stdc_trailing_onesus(unsigned short value);
int stdc_trailing_onesui(unsigned int value);
int stdc_trailing_onesul(unsigned long value);
int stdc_trailing_onesull(unsigned long long value);
#ifndef __cplusplus
#    define stdc_trailing_ones(value) __STDC_GENERIC_BIT_UTILITY(stdc_trailing_ones, value)
#endif /* __cplusplus */

int stdc_first_leading_zerouc(unsigned char value);
int stdc_first_leading_zerous(unsigned short value);
int stdc_first_leading_zeroui(unsigned int value);
int stdc_first_leading_zeroul(unsigned long value);
int stdc_first_leading_zeroull(unsigned long long value);
#ifndef __cplusplus
#    define stdc_first_leading_zero(value) __STDC_GENERIC_BIT_UTILITY(stdc_first_leading_zero, value)
#endif /* __cplusplus */

int stdc_first_leading_oneuc(unsigned char value);
int stdc_first_leading_oneus(unsigned short value);
int stdc_first_leading_oneui(unsigned int value);
int stdc_first_leading_oneul(unsigned long value);
int stdc_first_leading_oneull(unsigned long long value);
#ifndef __cplusplus
#    define stdc_first_leading_one(value) __STDC_GENERIC_BIT_UTILITY(stdc_first_leading_one, value)
#endif /* __cplusplus */

int stdc_first_trailing_zerouc(unsigned char value);
int stdc_first_trailing_zerous(unsigned short value);
int stdc_first_trailing_zeroui(unsigned int value);
int stdc_first_trailing_zeroul(unsigned long value);
int stdc_first_trailing_zeroull(unsigned long long value);
#ifndef __cplusplus
#    define stdc_first_trailing_zero(value) __STDC_GENERIC_BIT_UTILITY(stdc_first_trailing_zero, value)
#endif /* __cplusplus */

int stdc_first_trailing_oneuc(unsigned char value);
int stdc_first_trailing_oneus(unsigned short value);
int stdc_first_trailing_oneui(unsigned int value);
int stdc_first_trailing_oneul(unsigned long value);
int stdc_first_trailing_oneull(unsigned long long value);
#ifndef __cplusplus
#    define stdc_first_trailing_one(value) __STDC_GENERIC_BIT_UTILITY(stdc_first_trailing_one, value)
#endif /* __cplusplus */

int stdc_count_onesuc(unsigned char value);
int stdc_count_onesus(unsigned short value);
int stdc_count_onesui(unsigned int value);
int stdc_count_onesul(unsigned long value);
int stdc_count_onesull(unsigned long long value);
#ifndef __cplusplus
#    define stdc_count_ones(value) __STDC_GENERIC_BIT_UTILITY(stdc_count_ones, value)
#endif /* __cplusplus */

int stdc_count_zerosuc(unsigned char value);
int stdc_count_zerosus(unsigned short value);
int stdc_count_zerosui(unsigned int value);
int stdc_count_zerosul(unsigned long value);
int stdc_count_zerosull(unsigned long long value);
#ifndef __cplusplus
#    define stdc_count_zeros(value) __STDC_GENERIC_BIT_UTILITY(stdc_count_zeros, value)
#endif /* __cplusplus */

_Bool stdc_has_single_bituc(unsigned char value);
_Bool stdc_has_single_bitus(unsigned short value);
_Bool stdc_has_single_bitui(unsigned int value);
_Bool stdc_has_single_bitul(unsigned long value);
_Bool stdc_has_single_bitull(unsigned long long value);
#ifndef __cplusplus
#    define stdc_has_single_bit(value) __STDC_GENERIC_BIT_UTILITY(stdc_has_single_bit, value)
#endif /* __cplusplus */

int stdc_bit_widthuc(unsigned char value);
int stdc_bit_widthus(unsigned short value);
int stdc_bit_widthui(unsigned int value);
int stdc_bit_widthul(unsigned long value);
int stdc_bit_widthull(unsigned long long value);
#ifndef __cplusplus
#    define stdc_bit_width(value) __STDC_GENERIC_BIT_UTILITY(stdc_bit_width, value)
#endif /* __cplusplus */

unsigned char stdc_bit_flooruc(unsigned char value);
unsigned short stdc_bit_floorus(unsigned short value);
unsigned int stdc_bit_floorui(unsigned int value);
unsigned long stdc_bit_floorul(unsigned long value);
unsigned long long stdc_bit_floorull(unsigned long long value);
#ifndef __cplusplus
#    define stdc_bit_floor(value) __STDC_GENERIC_BIT_UTILITY(stdc_bit_floor, value)
#endif /* __cplusplus */

unsigned char stdc_bit_ceiluc(unsigned char value);
unsigned short stdc_bit_ceilus(unsigned short value);
unsigned int stdc_bit_ceilui(unsigned int value);
unsigned long stdc_bit_ceilul(unsigned long value);
unsigned long long stdc_bit_ceilull(unsigned long long value);
#ifndef __cplusplus
#    define stdc_bit_ceil(value) __STDC_GENERIC_BIT_UTILITY(stdc_bit_ceil, value)
#endif /* __cplusplus */

__END_DECLS

#endif /* SOLC_STDBIT_H */
