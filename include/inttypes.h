#ifndef SOLC_INTTYPES_H
#define SOLC_INTTYPES_H

#include <stdint.h>
#include <sys/cdefs.h>

__BEGIN_DECLS

#if UINTPTR_WIDTH == UINT64_WIDTH
#    define __PRIPTR_PREFIX "ll"
#else
#    define __PRIPTR_PREFIX "l"
#endif /* UINTPTR_WIDTH == UINT64_WIDTH */

/*
 * 7.8 Format conversion of integer types <inttypes.h>
 *
 * 1. The header <inttypes.h> includes the header <stdint.h> and extends it with additional
 *    facilities provided by hosted implementations
 * 2. It declares functions for manipulating greatest-width integers and converting numeric character
 *    strings to greatest-width integers, and it declares the type
 *    - imaxdiv_t
 *    which is a structure type that is the type of the value returned by the imaxdiv function.
 *    For each type declared int <stdint.h>, it defineds corresponding macros for conversion specifiers
 *    for use with the formatted input/output functions.
 *    Forward references: integer types <stdint.h> (7.20), formatted input/output functions (7.21.6),
 *    formatted wide character input/output functions (7.29.2).
 */

typedef struct {
    intmax_t quot;
    intmax_t rem;
} imaxdiv_t;

/*
 * 7.8.1 Macros for format specifiers
 * 1. Each of the following object-like macros expands to a character string literal containing a conversion
 *    specifier, possibly modified by a length modifier, suitable for use within the format argument of a
 *    formatted input/output function when converting the corresponding integer type. These macro names
 *    have the general form of PRI (character string literals for the fprintf and fwprintf family) or SCN
 *    (character string literals for the fascnaf and fwscanf family), followed by the conversion specifier,
 *    followed by a name corresponding to a similar type name in 7.20.1. In these names, N represents
 *    the width of the type as described in 7.20.1. For example, PRIdFAST32 can be used in a format string
 *    to print the value of an integer of type int_fast32_t.
 */
/*
 * 2. The fprintf macros for signed integers are:
 *      PRIdN  PRIdLEASTN  PRIdFASTN  PRIdMAX  PRIdPTR
 *      PRIiN  PRIiLEASTN  PRIiFASTN  PRIiMAX  PRIiPTR
 */
#define PRId8        "d"
#define PRId16       "d"
#define PRId32       "d"
#define PRId64       "lld"
#define PRIi8        "i"
#define PRIi16       "i"
#define PRIi32       "i"
#define PRIi64       "lli"

#define PRIdLEASTN8  "d"
#define PRIdLEASTN16 "d"
#define PRIdLEASTN32 "d"
#define PRIdLEASTN64 "lld"
#define PRIiLEASTN8  "i"
#define PRIiLEASTN16 "i"
#define PRIiLEASTN32 "i"
#define PRIiLEASTN64 "lli"

#define PRIdFAST8    "d"
#define PRIdFAST16   "d"
#define PRIdFAST32   "d"
#define PRIdFAST64   "lld"
#define PRIiFAST8    "i"
#define PRIiFAST16   "i"
#define PRIiFAST32   "i"
#define PRIiFAST64   "lli"

#define PRIdMAX      "lld"
#define PRIiMAX      "lli"
#define PRIdPTR      __PRIPTR_PREFIX "d"
#define PRIiPTR      __PRIPTR_PREFIX "i"
/*
 * 3. The fprintf macros for unsigned integers are:
 *      PRoIN  PRIoLEASTN  PRIoFAST  PRIoMAX  PRIoPTR
 *      PRuIN  PRIuLEASTN  PRIuFAST  PRIuMAX  PRuIPTR
 *      PRIxN  PRIxLEASTN  PRIxFAST  PRIxMAX  PRxIPTR
 *      PRIXN  PRIXLEASTN  PRIXFAST  PRIXMAX  PRIXPTR
 */
#define PRIo8        "o"
#define PRIo16       "o"
#define PRIo32       "o"
#define PRIo64       "llo"
#define PRIu8        "u"
#define PRIu16       "u"
#define PRIu32       "u"
#define PRIu64       "llu"
#define PRIx8        "x"
#define PRIx16       "x"
#define PRIx32       "x"
#define PRIx64       "llx"
#define PRIX8        "X"
#define PRIX16       "X"
#define PRIX32       "X"
#define PRIX64       "llX"

#define PRIoLEAST8   "o"
#define PRIoLEAST16  "o"
#define PRIoLEAST32  "o"
#define PRIoLEAST64  "llo"
#define PRIuLEAST8   "u"
#define PRIuLEAST16  "u"
#define PRIuLEAST32  "u"
#define PRIuLEAST64  "llu"
#define PRIxLEAST8   "x"
#define PRIxLEAST16  "x"
#define PRIxLEAST32  "x"
#define PRIxLEAST64  "llx"
#define PRIXLEAST8   "X"
#define PRIXLEAST16  "X"
#define PRIXLEAST32  "X"
#define PRIXLEAST64  "llX"

#define PRIoFAST8    "o"
#define PRIoFAST16   "o"
#define PRIoFAST32   "o"
#define PRIoFAST64   "llo"
#define PRIuFAST8    "u"
#define PRIuFAST16   "u"
#define PRIuFAST32   "u"
#define PRIuFAST64   "llu"
#define PRIxFAST8    "x"
#define PRIxFAST16   "x"
#define PRIxFAST32   "x"
#define PRIxFAST64   "llx"
#define PRIXFAST8    "X"
#define PRIXFAST16   "X"
#define PRIXFAST32   "X"
#define PRIXFAST64   "llX"

#define PRIoMAX      "llo"
#define PRIoPTR      __PRIPTR_PREFIX "o"
#define PRIuMAX      "llu"
#define PRIuPTR      __PRIPTR_PREFIX "u"
#define PRIxMAX      "llx"
#define PRIxPTR      __PRIPTR_PREFIX "x"
#define PRIXMAX      "llX"
#define PRIXPTR      __PRIPTR_PREFIX "X"
/*
 * 4. The fscanf macros for signed integers are:
 *      SCNdN  SCNdLEAST  SCNdFASTN  SCNdMAX  SCNdPTR
 *      SCNiN  SCNiLEAST  SCNiFASTN  SCNiMAX  SCNiPTR
 */
#define SCNd8        "hhd"
#define SCNd16       "hd"
#define SCNd32       "d"
#define SCNd64       "lld"
#define SCNi8        "hhi"
#define SCNi16       "hi"
#define SCNi32       "i"
#define SCNi64       "lli"
/*
 * 5. The fscanf macros for unsigned integers are:
 *      SCNoN  SCNoLEASTN  SCNoFASTN  SCNoMAX  SCNoPTR
 *      SCNuN  SCNuLEASTN  SCNuFASTN  SCNuMAX  SCNuPTR
 *      SCNxN  SCNxLEASTN  SCNxFASTN  SCNxMAX  SCNxPTR
 */
#define SCNdLEAST8   "hhd"
#define SCNdLEAST16  "hd"
#define SCNdLEAST32  "d"
#define SCNdLEAST64  "lld"
#define SCNiLEAST8   "hhi"
#define SCNiLEAST16  "hi"
#define SCNiLEAST32  "i"
#define SCNiLEAST64  "lli"

#define SCNdFAST8    "hhd"
#define SCNdFAST16   "hd"
#define SCNdFAST32   "d"
#define SCNdFAST64   "lld"
#define SCNiFAST8    "hhi"
#define SCNiFAST16   "hi"
#define SCNiFAST32   "i"
#define SCNiFAST64   "lli"

#define SCNdMAX      "lld"
#define SCNiMAX      "lli"
#define SCNdPTR      __PRIPTR_PREFIX "d"
#define SCNiPTR      __PRIPTR_PREFIX "i"

#define SCNo8        "hho"
#define SCNo16       "ho"
#define SCNo32       "o"
#define SCNo64       "llo"
#define SCNu8        "hhu"
#define SCNu16       "hu"
#define SCNu32       "u"
#define SCNu64       "llu"
#define SCNx8        "hhx"
#define SCNx16       "hx"
#define SCNx32       "x"
#define SCNx64       "llx"

#define SCNoLEAST8   "hho"
#define SCNoLEAST16  "ho"
#define SCNoLEAST32  "o"
#define SCNoLEAST64  "llo"
#define SCNuLEAST8   "hhu"
#define SCNuLEAST16  "hu"
#define SCNuLEAST32  "u"
#define SCNuLEAST64  "llu"
#define SCNxLEAST8   "hhx"
#define SCNxLEAST16  "hx"
#define SCNxLEAST32  "x"
#define SCNxLEAST64  "llx"

#define SCNoFAST8    "hho"
#define SCNoFAST16   "ho"
#define SCNoFAST32   "o"
#define SCNoFAST64   "llo"
#define SCNuFAST8    "hhu"
#define SCNuFAST16   "hu"
#define SCNuFAST32   "u"
#define SCNuFAST64   "llu"
#define SCNxFAST8    "hhx"
#define SCNxFAST16   "hx"
#define SCNxFAST32   "x"
#define SCNxFAST64   "llx"

#define SCNoMAX      "llo"
#define SCNuMAX      "llu"
#define SCNxMAX      "llx"
#define SCNoPTR      __PRIPTR_PREFIX "o"
#define SCNuPTR      __PRIPTR_PREFIX "u"
#define SCNxPTR      __PRIPTR_PREFIX "x"
/*
 * 6. For each type that the implementation provides in <stdint.h>, the corresponding fprintf macros
 *    shall be defined and the corresponding fscanf macros shall be defined unless the implementation
 *    does not have a suitable fscanf length modifier for the type.
 * 7. EXAMPLE
 *    ______________________________________________________________________
 *    | #include <inttypes.h>                                              |
 *    | #include <wchar.h>                                                 |
 *    | int main(void)                                                     |
 *    | {                                                                  |
 *    |     uintmax_t i = UINTMAX_MAX;      // this type always exists     |
 *    |     wprintf(L"The largest integer value is %020" PRIxMAX "\n", i); |
 *    |     return 0;                                                      |
 *    | }                                                                  |
 *    |____________________________________________________________________|
 */

/* 7.8.2 Functions for greatest-width integer types */

/*
 * 7.8.2.1 The imaxabs function
 *  Synopsis
 * 1. _________________________________
 *    | #include <inttypes.h>         |
 *    | intmax_t imaxabs(intmax_t j); |
 *    |_______________________________|
 *  Description
 * 2. The imaxabs function computes the absolute value of an integer j.
 *    If the result cannot be represented, the behaviout is undefined.
 *  Returns
 * 3. The imaxabs function returns the absolute value.
 */
intmax_t imaxabs(intmax_t j);

/*
 * 7.8.2.12 The imaxdiv function
 *  Synopsis
 * 1. _____________________________________________________
 *    | #include <inttypes.h>                              |
 *    | imaxdiv_t imaxdiv(intmax_t numer, intmax_t denom); |
 *    |____________________________________________________|
 *  Description
 * 2. The imaxdiv function computes numer / denom and numer % denom in a single operation.
 *  Returns
 * 3. The imaxdiv function returns a structure of type imaxdiv_t comprising both the quotient and the
 *    remainder. The structure shall contain (in either order) the members quot (the quotient) and rem
 *    (the remainder), each of which ahs type intmax_t. If either part of the result cannor be
 *    represented, the behaviour is undefined.
 */
imaxdiv_t imaxdiv(intmax_t numer, intmax_t denom);

/*
 * 7.8.2.3 The strtoimax and strtoumax functions
 *  Synopsis
 * 1. ____________________________________________________________________________________
 *    | #include <inttypes.h>                                                            |
 *    | intmax_t strtoimax(const char* restrict nptr, char** restrict endptr, intbase);  |
 *    | uintmax_t strtoumax(const char* restrict nptr, char** restrict endptr, intbase); |
 *    |__________________________________________________________________________________|
 *  Description
 * 2. The strtoimax and strtoumax functions are equivalent to the strtol, strtoll, strtoul, and
 *    strtoull functions, except that the initial portion of the string is converted to intmax_t
 *    and uintmax_t representation, respectively.
 *  Returns
 * 3. The strtoimax and strtoumax functions return the converted value, if any. If no conversion
 *    could be performed, zero is returned. If the correct value is outside the range of representable
 *    values, INTMAX_MAX, INTMAX_MIN, or UINTMAX_MAX is returned (according to the return type and
 *    sign of the value, if any), and the value of the macro ERANGE is stored in errno.
 *
 *    Forward references: the strtol, strtoll, strtoul, and strtoull functions (7.22.1.7).
 */

intmax_t strtoimax(const char* restrict nptr, char** restrict endptr, int base);
uintmax_t strtoumax(const char* restrict nptr, char** restrict endptr, int base);

/*
 * 7.8.2.4 The wcstoimax and wcstoumax functions
 *  Synopsis
 * 1. ___________________________________________________________________________________________
 *    | #include <stddef.h>     // for wchar_t                                                  |
 *    | #include <inttypes.h>                                                                   |
 *    | intmax_t wcstoimax(const wchar_t* restrict nptr, wchar_t** restrict endptr, int base);  |
 *    | uintmax_t wcstoumax(const wchar_t* restrict nptr, wchar_t** restrict endptr, int base); |
 *    |_________________________________________________________________________________________|
 *  Description
 * 2. The wcstoimax and wcstoumax functions are equivalent to the wcstol, wcstoll, wcstoul, and
 *    wcstoull functions except that the initial portion of the wide string is converted to intmax_t
 *    and uintmax_t representation, respectively.
 *  Returns
 * 3. The wcstoimax function returns the converted value, if any. If no conversion
 *    coud be performed, zero is returned. If the correct value is outside the range of representable
 *    values, INTMAX_MAX, INTMAX_MIN, or UINTMAX_MAX is returned (according to the return type and
 *    sign of the value, if any), and the value or the macro ERANGE is stored in errno.
 *
 *    Forward references: the wcstol, wcstoll, wcstoul, wcstoull functions (7.29.4.1.3).
 */
#include <stddef.h>
intmax_t wcstoimax(const wchar_t* restrict nptr, wchar_t** restrict endptr, int base);
uintmax_t wcstoumax(const wchar_t* restrict nptr, wchar_t** restrict endptr, int base);

__END_DECLS

#endif /* SOLC_INTTYPES_H */
