#ifndef SOLC_STDNORETURN_H
#define SOLC_STDNORETURN_H

/*
 * TODO: Add preprocessor checks in order to not define noreturn in C23 mode.
 */

/*
 * 7.23 _Noreturn <stdnoreturn.h>
 *
 * 1. The header <stdnoreturn.h> defines the macro
 * -    noreturn
 *    which expands to _Noreturn.
 * 2. The noreturn macro and the <stdnoreturn.h> header are obsolescent features.
 */

#ifndef __cplusplus
#define noreturn _Noreturn
#endif /* __cplusplus */

#endif /* SOLC_STDNORETURN_H */
