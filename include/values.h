#ifndef SOLC_VALUES_H
#define SOLC_VALUES_H

#include <float.h>
#include <limits.h>

/*
 * WARNING!!!
 * This interface is obsolete and should not be used.
 * Use <limits.h> and/or <float.h> instead.
 */

/*
 * The <values.h> header contains a set of manifest constants,
 * conditionally defined for particular processor architectures.
 * The model assumed for integers is binary representation
 * (one's or two's complement), where the sign is represented
 * by the value of the high-order bit.
 */

/* Number of bits in the specified data type. */
#define BITS(type)   (sizeof(type) * CHAR_BIT)
/* Number of bits per byte. */
#define BITSPERBYTE  CHAR_BIT
/* Number of bits for primitive types. */
#define CHARBITS     BITS(char)
#define SHORTBITS    BITS(short int)
#define INTBITS      BITS(int)
#define LONGBITS     BITS(long int)
#define PTRBITS      BITS(char*)
#define FLOATBITS    BITS(float)
#define DOUBLEBITS   BITS(double)

/* Short integer with only the high-order bit set. */
#define HIBITS       SHRT_MIN
/* Integer with only the high order bit set. */
#define HIBITI       INT_MIN
/* Long integer with only the high order bit set. */
#define HIBITL       LONG_MIN

/* Maximum value of a signed short integer. */
#define MAXSHORT     SHRT_MAX
/* Maximum value of a signed integer. */
#define MAXINT       INT_MAX
/* Maximum value of a signed long integer. */
#define MAXLONG      LONG_MAX

/* Minimum value of a signed short integer. */
#define MINSHORT     SHRT_MIN
/* Minimum value of a signed integer. */
#define MININT       INT_MIN
/* Minimum value of a signed long integer. */
#define MINLONG      LONG_MIN

/* Maximum value of a single-precision floating-point number. */
#define MAXFLOAT     FLT_MAX
/* Maximum value of a double-precision floating-point number. */
#define MAXDOUBLE    DBL_MAX

/* Minimum value of a single-precision floating-point number. */
#define MINFLOAT     FLT_MIN
/* Minimum value of a double-precision floating-point number. */
#define MINDOUBLE    DBL_MIN

/* Natural logarithm of the MAXFLOAT value. */
#define LN_MAXDOUBLE 88.72283905206835305365817656031404273f
/* Natural logarithm of the MAXDOUBLE value. */
#define LN_MAXFLOAT  709.782712893383996732223389910657145505

/* Natural logarithm of the MINFLOAT value. */
#define LN_MINDOUBLE -87.336544750553108986571247303730247576f
/* Natural logarithm of the MAXFLOAT value. */
#define LN_MINFLOAT  -708.396418532264106224411228130256452575

/* Maximum exponent of a single-precision floating-point number. */
#define FMAXEXP      FLT_MAX_EXP
/* Maximum exponent of a double-precision floating-point number. */
#define DMAXEXP      DBL_MAX_EXP

/* Minimum exponent of a single-precision floating-point number. */
#define FMINEXP      FLT_MIN_EXP
/* Minimum exponent of a double-precision floating-point number. */
#define DMINEXP      DBL_MIN_EXP

/* Largest power of two that can be exactly represented as a single-precision floating-point number. */
#define FMAXPOWTWO   8388608.0f
/* Largest power of two that can be exactly represented as a double-precision floating-point number. */
#define DMAXPOWTWO   83076749736557242056487941267521536.0

/* Number of significant bits in the mantissa of a single-precision floating-point number. */
#define FSIGNIF      FLT_MANT_DIG
/* Number of significant bits in the mantissa of a double-precision floating-point number. */
#define DSIGNIF      DBL_MANT_DIG

/* Base to which exponent applies. */
#define _LENBASE     FLT_RADIX
/* Number of bits in the exponent of a single-precision floating-point number. */
#define _FEXPLEN     8
/* Number of bits in the exponent of a double-precision floating-point number. */
#define _DEXPLEN     11

#endif /* SOLC_VALUES_H */
