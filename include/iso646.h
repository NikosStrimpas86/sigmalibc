#ifndef SOLC_ISO646_H
#define SOLC_ISO646_H

/*
 * 7.9 Alternative spellings <iso646.h>
 *
 * 1. The header <iso646.h> defines the following eleven macros (on the left) that
 *    expand to the corresponding tokens (on the right):
 *    -     and     &&
 *    -     and_eq  &=
 *    -     bitand  &
 *    -     bitor   |
 *    -     compl   ~
 *    -     not     !
 *    -     not_eq  !=
 *    -     or      ||
 *    -     or_eq   |=
 *    -     xor     ^
 *    -     xor_eq  ^=
 *
 */

#ifndef __cplusplus

#    define and    &&
#    define and_eq &=
#    define bitand &
#    define bitor  |
#    define compl  ~
#    define not    !
#    define not_eq !=
#    define or     ||
#    define or_eq  |=
#    define xor    ^
#    define xor_eq ^=

#endif /* __cplusplus */

#endif /* SOLC_ISO646_H */
