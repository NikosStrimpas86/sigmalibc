#ifndef SOLC_ASSERT_H
#define SOLC_ASSERT_H

#include <sys/cdefs.h>

__BEGIN_DECLS

/*
 * TODO: Implement assert macro. (It needs abort function)
 */
#ifdef NDEBUG
#    define assert(ignore) ((void)0)
#endif /* NDEBUG */

#if !defined(__cplusplus) && defined(__STDC_VERSION__) && ((__STDC_VERSION__ >= 201112L) && (__STDC_VERSION__ <= 201710L))
#    define static_assert _Static_assert
#endif /* __cplusplus */

__END_DECLS

#endif /* SOLC_ASSERT_H */
