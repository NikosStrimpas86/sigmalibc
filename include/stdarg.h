#ifndef SOLC_STDARG_H
#define SOLC_STDARG_H

#include <sys/cdefs.h>

__BEGIN_DECLS

typedef __builtin_va_list va_list;

#define va_arg(ap, type)    __builtin_va_arg(ap, type)
#define va_copy(dest, src)  __builtin_va_copy(dest, src)
#define va_end(ap)          __builtin_va_end(ap)
/* Currently only gcc (maybe 13) supports the relaxed requirements for va_start */
#if defined(__STDC_VERSION__) && (__STDC_VERSION__ > 201710L) && defined(__GNUC__) && !defined(__clang__)
#define va_start(ap, ...) __builtin_va_start(ap, 0)
#else
#define va_start(ap, parmN) __builtin_va_start(ap, parmN)
#endif


__END_DECLS

#endif /* SOLC_STDARG_H */
