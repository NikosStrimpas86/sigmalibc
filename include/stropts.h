#ifndef SOLC_STROPTS_H
#define SOLC_STROPTS_H

#include <sys/cdefs.h>

__BEGIN_DECLS

struct bandinfo {
    int bi_flag;
    unsigned char bi_pri;
};

struct strbuf {
    char* buf;
    int len;
    int maxlen;
};

typedef __INT_LEAST32_TYPE__ t_scalar_t;
typedef __UINT_LEAST32_TYPE__ t_uscalar_t;

struct strpeek {
    struct strbuf ctlbuf;
    struct strbuf databuf;
    t_uscalar_t flags;
};

struct strfdinsert {
    struct strbuf ctlbuf;
    struct strbuf databuf;
    int fildes;
    t_uscalar_t flags;
    int offset;
};

struct strioctl {
    int ic_cmd;
    char* ic_dp;
    int ic_len;
    int ic_timout;
};

/*
 * TODO: These should be elsewhere
 */
typedef unsigned gid_t;
typedef unsigned uid_t;

struct strrecvfd {
    int fd;
    gid_t gid;
    uid_t uid
};

/* FIXME: Find a good value for this */
#define FMNAMESZ 8

struct str_mlist {
    char l_name[FMNAMESZ + 1];
};

struct str_list {
    struct str_mlist* sl_modlist;
    int sl_nmods;
};

__END_DECLS

#endif /* SOLC_STROPTS_H */
