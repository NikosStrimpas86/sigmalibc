#ifndef SOLC_SEARCH_H
#define SOLC_SEARCH_H

#include <sys/cdefs.h>

__BEGIN_DECLS

typedef struct entry {
    char* key;
    void* data;
} ENTRY;

typedef enum {
    FIND,
    ENTER
} ACTION;

typedef enum {
    preorder,
    postorder,
    endorder,
    leaf
} VISIT;

#include <internal/types/size_t.h>

void insque(void* element, void* pred);
void remque(void* element);

int hcreate(size_t nel);
void hdestroy(void);
ENTRY* hsearch(ENTRY item, ACTION action);

void* lsearch(const void* key, void* base, size_t* nelp, size_t width, int (*compar)(const void*, const void*));
void* lfind(const void* key, const void* base, size_t* nelp, size_t width, int (*compar)(const void*, const void*));

void* tdelete(const void* restrict key, void** restrict rootp, int (*compar)(const void*, const void*));
void* tfind(const void* key, void* const* rootp, int (*compar)(const void*, const void*));
void* tsearch(const void* key, void** rootp, int (*compar)(const void*, const void*));
void twalk(const void* root, void (*action)(const void*, VISIT, int));

__END_DECLS

#endif /* SOLC_SEARCH_H */
