#ifndef SOLC_THREADS_H
#define SOLC_THREADS_H

#include <sys/cdefs.h>

__BEGIN_DECLS

#if (!(defined(__cplusplus)) && defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 201112L) && (__STDC_VERSION__ <= 201710L))
#define thread_local _Thread_local
#endif

#define ONCE_FLAG_INIT
#define TSS_DTOR_ITERATIONS

/* clang-format off */
#ifndef __SOLC_TSS_DTOR_T_DEFINED
#define __SOLC_TSS_DTOR_T_DEFINED
typedef void (*tss_dtor_t)(void*);
#endif /* __SOLC_TSS_DTOR_T_DEFINED */

#ifndef __SOLC_THRD_START_T_DEFINED
#define __SOLC_THRD_START_T_DEFINED
typedef int (*thrd_start_t)(void*);
#endif /* __SOLC_THRD_START_T_DEFINED */
/* clang-format on */

enum {
    mtx_plain = 0,
    mtx_recursive = 1,
    mtx_timed = 2,
};

enum {
    thrd_success = 0,
    thrd_timedout = 1,
    thrd_busy = 2,
    thrd_error = 3,
    thrd_nomem = 4,
};

__END_DECLS

#endif /* SOLC_THREADS_h */
