#ifndef SOLC_NDBM_H
#define SOLC_NDBM_H

#include <internal/types/size_t.h>
#include <sys/cdefs.h>

__BEGIN_DECLS

typedef struct {
    void* dptr;
    size_t dsize;
} datum;

struct DBM;

#define DBM_INSERT  0
#define DBM_REPLACE 1

/*
 * TODO: More research on this type.
 * For the time being let's use this definition: https://github.com/LuaDist/libgdbm/blob/master/ndbm.h
 * Windows seems to have its own definition of DBM, but it is a little too complicated.
 * Macos also has its own definition for this type, but it seems like __magic, so I can't
 * really trust it for the time being. Unfortunately I can't find information on the structure
 * of these files, so I can't see myself what the requirements are. If anyone is seeing this,
 * some help would be invaluable.
 */
typedef struct {
    int dummy[10];
} DBM;

typedef __UINT16_TYPE__ mode_t;

int dbm_clearerr(DBM* db);
void dbm_close(DBM* db);
int dbm_delete(DBM* db, datum key);
int dbm_error(DBM* db);
datum dbm_fetch(DBM* db, datum key);
datum dbm_firstkey(DBM* db);
datum dbm_nextkey(DBM* db);
DBM* dbm_open(const char* file, int open_flags, mode_t file_mode);
int dbm_store(DBM* db, datum key, datum content, int store_mode);

__END_DECLS

#endif /* SOLC_NDBM_H */
