#ifndef SOLC_STDATOMIC_H
#define SOLC_STDATOMIC_H

/*
 * TODO: Research about whether or not we need this header, since gcc provides its own.
 * TODO: Guard char8_t atomic variant.
 * TODO: Implement the macros (probably using '__c11' and '__' intrinsics.
 * FIXME: We don't have to pull functions from <uchar.h> here.
 */

#include <stddef.h>
#include <stdint.h>
#include <sys/cdefs.h>
#include <uchar.h>

#define __STDC_VERSION_STDATOMIC_H__ 202311L

__BEGIN_DECLS

/* https://clang.llvm.org/docs/LanguageExtensions.html#c11-atomic-operations */
#if defined(__clang__)
#    define __SOLC_ATOMICS_CLANG 1
#elif defined(__GNUC__) && !defined(__clang__)
#    define __SOLC_ATOMICS_GCC 1
#else
#    error "The header <stdatomic.h> has not been implemented for this compiler"
#endif

/* FIXME: Turn this path on when gcc lands support for C23 bool */
#if ((defined(__cplusplus)) || ((defined(__STDC_VERSION__)) && __STDC_VERSION__ > 201710L && 0))
#    define _CHOOSE_BOOL bool
#else
#    define _CHOOSE_BOOL _Bool
#endif

#ifdef __CLANG_ATOMIC_BOOL_LOCK_FREE
#    define ATOMIC_BOOL_LOCK_FREE __CLANG_ATOMIC_BOOL_LOCK_FREE
#    define ATOMIC_CHAR_LOCK_FREE __CLANG_ATOMIC_CHAR_LOCK_FREE
#    ifdef __CLANG_ATOMIC_CHAR8_T_LOCK_FREE
#        define ATOMIC_CHAR8_T_LOCK_FREE __CLANG_ATOMIC_CHAR8_T_LOCK_FREE
#    endif
#    define ATOMIC_CHAR16_T_LOCK_FREE __CLANG_ATOMIC_CHAR16_T_LOCK_FREE
#    define ATOMIC_CHAR32_T_LOCK_FREE __CLANG_ATOMIC_CHAR32_T_LOCK_FREE
#    define ATOMIC_WCHAR_T_LOCK_FREE  __CLANG_ATOMIC_WCHAR_T_LOCK_FREE
#    define ATOMIC_SHORT_LOCK_FREE    __CLANG_ATOMIC_SHORT_LOCK_FREE
#    define ATOMIC_INT_LOCK_FREE      __CLANG_ATOMIC_INT_LOCK_FREE
#    define ATOMIC_LONG_LOCK_FREE     __CLANG_ATOMIC_LONG_LOCK_FREE
#    define ATOMIC_LLONG_LOCK_FREE    __CLANG_ATOMIC_LLONG_LOCK_FREE
#    define ATOMIC_POINTER_LOCK_FREE  __CLANG_ATOMIC_POINTER_LOCK_FREE
#else
#    define ATOMIC_BOOL_LOCK_FREE __GCC_ATOMIC_BOOL_LOCK_FREE
#    define ATOMIC_CHAR_LOCK_FREE __GCC_ATOMIC_CHAR_LOCK_FREE
#    ifdef __GCC_ATOMIC_CHAR8_T_LOCK_FREE
#        define ATOMIC_CHAR8_T_LOCK_FREE __GCC_ATOMIC_CHAR8_T_LOCK_FREE
#    endif
#    define ATOMIC_CHAR16_T_LOCK_FREE __GCC_ATOMIC_CHAR16_T_LOCK_FREE
#    define ATOMIC_CHAR32_T_LOCK_FREE __GCC_ATOMIC_CHAR32_T_LOCK_FREE
#    define ATOMIC_WCHAR_T_LOCK_FREE  __GCC_ATOMIC_WCHAR_T_LOCK_FREE
#    define ATOMIC_SHORT_LOCK_FREE    __GCC_ATOMIC_SHORT_LOCK_FREE
#    define ATOMIC_INT_LOCK_FREE      __GCC_ATOMIC_INT_LOCK_FREE
#    define ATOMIC_LONG_LOCK_FREE     __GCC_ATOMIC_LONG_LOCK_FREE
#    define ATOMIC_LLONG_LOCK_FREE    __GCC_ATOMIC_LLONG_LOCK_FREE
#    define ATOMIC_POINTER_LOCK_FREE  __GCC_ATOMIC_POINTER_LOCK_FREE
#endif

typedef enum {
    memory_order_relaxed = __ATOMIC_RELAXED,
    memory_order_consume = __ATOMIC_CONSUME,
    memory_order_acquire = __ATOMIC_ACQUIRE,
    memory_order_release = __ATOMIC_RELEASE,
    memory_order_acq_rel = __ATOMIC_ACQ_REL,
    memory_order_seq_cst = __ATOMIC_SEQ_CST,
} memory_order;

#if defined(__SOLC_ATOMICS_CLANG)
typedef struct {
    _Atomic(_CHOOSE_BOOL) _m_flag;
} atomic_flag;
#elif defined(__SOLC_ATOMICS_GCC)
typedef _Atomic struct {
    _CHOOSE_BOOL _m_flag;
} atomic_flag;
#else
#    if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 202311L
#        warning "atomic_flag does not work with this compiler, you have been warned!"
#    endif
typedef _Atomic struct {
    _CHOOSE_BOOL _m_flag;
} atomic_flag;
#endif

#define ATOMIC_FLAG_INIT \
    {                    \
        0                \
    }

#if !(defined __STDC_VERSION__ && __STDC_VERSION__ > 201710L)
#    define ATOMIC_VAR_INIT(value) (value)
#endif

#if defined(__SOLC_ATOMICS_CLANG)
#    define atomic_init(obj, value) __c11_atomic_init(obj, value)
#elif defined(__SOLC_ATOMICS_GCC)
#    define atomic_init(obj, value) __atomic_store_n(obj, value, __ATOMIC_RELAXED)
#endif

#define kill_dependency(y) (y)

static inline void atomic_thread_fence(memory_order order)
{
#if defined(__SOLC_ATOMICS_CLANG)
    __c11_atomic_thread_fence(order);
#elif defined(__SOLC_ATOMICS_GCC)
    __atomic_thread_fence(order);
#else
#    if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 202311L
#        warning "atomic_thread_fence does not work with this compiler, you have been warned!"
#    endif
    (void)order;
#endif
}

static inline void atomic_signal_fence(memory_order order)
{
#if defined(__SOLC_ATOMICS_CLANG)
    __c11_atomic_signal_fence(order);
#elif defined(__SOLC_ATOMICS_GCC)
    __atomic_signal_fence(order);
#else
#    if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 202311L
#        warning "atomic_signal_fence does not work with this compiler, you have been warned!"
#    endif
#    ifdef __GNUC__
    (void)order;
    __asm__ volatile("" ::
                         : "memory");
#    else
    (void)order;
#    endif
#endif
}

#if defined(__SOLC_ATOMICS_CLANG)
#    define atomic_is_lock_free(obj) __c11_atomic_is_lock_free(sizeof(*(obj)))
#elif defined(__SOLC_ATOMICS_GCC)
#    define atomic_is_lock_free(obj) __atomic_is_lock_free(sizeof(*(obj)), (obj))
#else
#    define atomic_is_lock_free(obj) ((void)(obj), sizeof(*(obj)) <= sizeof(void*))
#endif

/* FIXME: Turn this path on when gcc lands support for C23 bool */
#if ((defined(__cplusplus)) || ((defined(__STDC_VERSION__)) && __STDC_VERSION__ > 201710L && 0))
typedef _Atomic(bool) atomic_bool;
#else
typedef _Atomic(_Bool) atomic_bool;
#endif
typedef _Atomic(char) atomic_char;
typedef _Atomic(signed char) atomic_schar;
typedef _Atomic(unsigned char) atomic_uchar;
typedef _Atomic(short) atomic_short;
typedef _Atomic(unsigned short) atomic_ushort;
typedef _Atomic(int) atomic_int;
typedef _Atomic(unsigned int) atomic_uint;
typedef _Atomic(long) atomic_long;
typedef _Atomic(unsigned long) atomic_ulong;
typedef _Atomic(long long) atomic_llong;
typedef _Atomic(unsigned long long) atomic_ullong;
typedef _Atomic(char8_t) atomic_char8_t;
typedef _Atomic(char16_t) atomic_char16_t;
typedef _Atomic(char32_t) atomic_char32_t;
typedef _Atomic(wchar_t) atomic_wchar_t;
typedef _Atomic(int_least8_t) atomic_int_least8_t;
typedef _Atomic(uint_least8_t) atomic_uint_least8_t;
typedef _Atomic(int_least16_t) atomic_int_least16_t;
typedef _Atomic(uint_least16_t) atomic_uint_least16_t;
typedef _Atomic(int_least32_t) atomic_int_least32_t;
typedef _Atomic(uint_least32_t) atomic_uint_least32_t;
typedef _Atomic(int_least64_t) atomic_int_least64_t;
typedef _Atomic(uint_least64_t) atomic_uint_least64_t;
typedef _Atomic(int_fast8_t) atomic_int_fast8_t;
typedef _Atomic(uint_fast8_t) atomic_uint_fast8_t;
typedef _Atomic(int_fast16_t) atomic_int_fast16_t;
typedef _Atomic(uint_fast16_t) atomic_uint_fast16_t;
typedef _Atomic(int_fast32_t) atomic_int_fast32_t;
typedef _Atomic(uint_fast32_t) atomic_uint_fast32_t;
typedef _Atomic(int_fast64_t) atomic_int_fast64_t;
typedef _Atomic(uint_fast64_t) atomic_uint_fast64_t;
typedef _Atomic(intptr_t) atomic_inptr_t;
typedef _Atomic(uintptr_t) atomic_uintptr_t;
typedef _Atomic(size_t) atomic_size_t;
typedef _Atomic(ptrdiff_t) atomic_ptrdiff_t;
typedef _Atomic(intmax_t) atomic_intmax_t;
typedef _Atomic(uintmax_t) atomic_uintmax_t;

#if defined(__SOLC_ATOMICS_CLANG)

#    define atomic_store(object, desired)                    __c11_atomic_store(object, desired, __ATOMIC_SEQ_CST)
#    define atomic_store_explicit(object, desired, order)    __c11_atomic_store(object, desired, order)

#    define atomic_load(object)                              __c11_atomic_load(object, __ATOMIC_SEQ_CST)
#    define atomic_load_explicit(object, order)              __c11_atomic_load(object, order)

#    define atomic_exchange(object, desired)                 __c11_atomic_exchange(object, desired, __ATOMIC_SEQ_CST)
#    define atomic_exchange_explicit(object, desired, order) __c11_atomic_exchange(object, desired, order)

#    define atomic_compare_exchange_strong(object, expected, desired) \
        __c11_atomic_exchange_strong(object, expected, desired, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST)
#    define atomic_compare_exchange_strong_explicit(object, expected, desired, success, failure) \
        __c11_atomic_exchange_strong(object, expected, desired, success, failure)

#    define atomic_compare_exchange_weak(object, expected, desired) \
        __c11_atomic_exchange_weak(object, expected, desired, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST)
#    define atomic_compare_exchange_weak_explicit(object, expected, desired, success, failure) \
        __c11_atomic_exchange_weak(object, expected, desired, success, failure)

#    define atomic_fetch_add(object, operand)                 __c11_atomic_fetch_add(object, operand, __ATOMIC_SEQ_CST)
#    define atomic_fetch_add_explicit(object, operand, order) __c11_atomic_fetch_add(object, operand, order)

#    define atomic_fetch_sub(object, operand)                 __c11_atomic_fetch_sub(object, operand, __ATOMIC_SEQ_CST)
#    define atomic_fetch_sub_explicit(object, operand, order) __c11_atomic_fetch_sub(object, operand, order)

#    define atomic_fetch_or(object, operand)                  __c11_atomic_fetch_or(object, operand, __ATOMIC_SEQ_CST)
#    define atomic_fetch_or_explicit(object, operand, order)  __c11_atomic_fetch_or(object, operand, order)

#    define atomic_fetch_xor(object, operand)                 __c11_atomic_fetch_xor(object, operand, __ATOMIC_SEQ_CST)
#    define atomic_fetch_xor_explicit(object, operand, order) __c11_atomic_fetch_xor(object, operand, order)

#    define atomic_fetch_and(object, operand)                 __c11_atomic_fetch_and(object, operand, __ATOMIC_SEQ_CST)
#    define atomic_fetch_and_explicit(object, operand, order) __c11_atomic_fetch_and(object, operand, order)

#elif defined(__SOLC_ATOMICS_GCC)

#    define atomic_store(object, desired)                    __atomic_store_n(object, desired, __ATOMIC_SEQ_CST)
#    define atomic_store_explicit(object, desired, order)    __atomic_store_n(object, desired, order)

#    define atomic_load(object)                              __atomic_load_n(object, __ATOMIC_SEQ_CST)
#    define atomic_load_explicit(object, order)              __atomic_load_n(object, order)

#    define atomic_exchange(object, desired)                 __atomic_exchange_n(object, desired, __ATOMIC_SEQ_CST)
#    define atomic_exchange_explicit(object, desired, order) __atomic_exchange_n(object, desired, order)

#    define atomic_compare_exchange_strong(object, expected, desired) \
        __atomic_compare_exchange_n(object, expected, desired, 0, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST)
#    define atomic_compare_exchange_strong_explicit(object, expected, desired, success, failure) \
        __atomic_compare_exchange_n(object, expected, desired, 0, success, failure)
#    define atomic_compare_exchange_weak(object, expected, desired) \
        __atomic_compare_exchange_n(object, expected, desired, 1, __ATOMIC_SEQ_CST, __ATOMIC_SEQ_CST)
#    define atomic_compare_exchange_weak_explicit(object, expected, desired, success, failure) \
        __atomic_compare_exchange_n(object, expected, desired, 1, success, failure)

#    define atomic_fetch_add(object, operand)                 __atomic_fetch_add(object, operand, __ATOMIC_SEQ_CST)
#    define atomic_fetch_add_explicit(object, operand, order) __atomic_fetch_add(object, operand, order)

#    define atomic_fetch_sub(object, operand)                 __atomic_fetch_sub(object, operand, __ATOMIC_SEQ_CST)
#    define atomic_fetch_sub_explicit(object, operand, order) __atomic_fetch_sub(object, operand, order)

#    define atomic_fetch_or(object, operand)                  __atomic_fetch_or(object, operand, __ATOMIC_SEQ_CST)
#    define atomic_fetch_or_explicit(object, operand, order)  __atomic_fetch_or(object, operand, order)

#    define atomic_fetch_xor(object, operand)                 __atomic_fetch_xor(object, operand, __ATOMIC_SEQ_CST)
#    define atomic_fetch_xor_explicit(object, operand, order) __atomic_fetch_xor(object, operand, order)

#    define atomic_fetch_and(object, operand)                 __atomic_fetch_and(object, operand, __ATOMIC_SEQ_CST)
#    define atomic_fetch_and_explicit(object, operand, order) __atomic_fetch_and(object, operand, order)

#else
/* TODO: Implement dummy macros */
#    error "No atomic generic functions found"
#endif

static inline _CHOOSE_BOOL atomic_flag_test_and_set(volatile atomic_flag* object)
{
#if defined(__SOLC_ATOMICS_CLANG)
    return __c11_atomic_exchange(&object->_m_flag, 1, __ATOMIC_SEQ_CST);
#elif defined(__SOLC_ATOMICS_GCC)
    return __atomic_test_and_set(object, __ATOMIC_SEQ_CST);
#else
#    if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 202311L
#        warning "atomic_flag_test_and_set does not work with this compiler, you have been warned!"
#    endif
#    ifdef atomic_exchange
    atomic_exchange(&object->_m_flag, 1);
#    else
    object->_m_flag = 1;
    (void)object;
    return 1;
#    endif
#endif
}

static inline _CHOOSE_BOOL atomic_flag_test_and_set_explicit(volatile atomic_flag* object, memory_order order)
{
#if defined(__SOLC_ATOMICS_CLANG)
    return __c11_atomic_exchange(&object->_m_flag, 1, order);
#elif defined(__SOLC_ATOMICS_GCC)
    return __atomic_test_and_set(object, order);
#else
#    if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 202311L
#        warning "atomic_flag_test_and_set_explicit does not work with this compiler, you have been warned!"
#    endif
#    ifdef atomic_exchange_explicit
    atomic_exchange_explicit(&object->_m_flag, 1, order);
#    else
    object->_m_flag = 1;
    (void)object;
    return 1;
#    endif
#endif
}

static inline void atomic_flag_clear(volatile atomic_flag* object)
{
#if defined(__SOLC_ATOMICS_CLANG)
    __c11_atomic_exchange(&object->_m_flag, 0, __ATOMIC_SEQ_CST);
#elif defined(__SOLC_ATOMICS_GCC)
    __atomic_clear(object, __ATOMIC_SEQ_CST);
#else
#    if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 202311L
#        warning "atomic_flag_clear does not work with this compiler, you have been warned!"
#    endif
#    ifdef atomic_exchange
    atomic_exchange(&object->_m_flag, 0, __ATOMIC_SEQ_CST);
#    else
    object->_m_flag = 0;
    (void)object;
#    endif
#endif
}

static inline void atomic_flag_clear_explicit(volatile atomic_flag* object, memory_order order)
{
#if defined(__SOLC_ATOMICS_CLANG)
    __c11_atomic_exchange(&object->_m_flag, 0, order);
#elif defined(__SOLC_ATOMICS_GCC)
    __atomic_clear(object, order);
#else
#    if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 202311L
#        warning "atomic_flag_clear_explicit does not work with this compiler, you have been warned!"
#    endif
#    ifdef atomic_exchange_explicit
    atomic_exchange_explicit(&object->_m_flag, 0, order);
#    else
    object->_m_flag = 0;
    (void)object;
#    endif
#endif
}

__END_DECLS

#endif /* SOLC_STDATOMIC_H */
