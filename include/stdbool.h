#ifndef SOLC_STDBOOL_H
#define SOLC_STDBOOL_H

/*
 * TODO: Add preprocessor checks, in order to not define bool, true, false in C23 mode.
 */

/*
 * 7.18 Boolean type and values <stdbool.h>
 *
 * 1. The header <stdbool.h> defines four macros
 * 2. The macro
 *    -     bool
 *    expands to _Bool
 * 3. The remaining three macros are suitable for use in #if preprocessing directives. They are
 *    -     true
 *    which expands to the integer constant ((_Bool)+1u),
 *    -     false
 *    which expands to the integer constant ((_Bool)+0u), and
 *    -     __bool_true_false_are_defined
 *    which expands to the integer constant 1.
 * 4. Notwithstanding the provisions of 7.1.3, a program may undefine and perhaps then redefine
 *    the macros bool, true, and false
 *
 */

#ifndef __cplusplus

#    if defined(__STDC_VERSION__) && (__STDC_VERSION__ < 202311L)
#        define bool  _Bool
#        define true  ((_Bool)+1u)
#        define false ((_Bool)+0u)
#    endif

#endif /* __cplusplus */

#define __bool_true_false_are_defined 1

#endif /* SOLC_STDBOOL_H */
