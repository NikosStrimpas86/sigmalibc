#ifndef SOLC_IF_H
#define SOLC_IF_H

struct if_nameindex {
    unsigned if_index;
    char* if_name;
};

#define IF_NAMESIZE 16

void if_freenameindex(struct if_nameindex* ptr);
char* if_indextoname(unsigned ifindex, char* ifname);
struct if_nameindex* if_nameindex(void);
unsigned if_nametoindex(const char* ifname);

#endif /* SOLC_IF_H */
