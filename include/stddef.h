#ifndef SOLC_STDDEF_H
#define SOLC_STDDEF_H

#include <internal/defines/null.h>
#include <internal/types/size_t.h>
#include <internal/types/wchar_t.h>

/*
 * 7.19 Common definitions <stddef.h>
 *
 * 1. The header <stddef.h> defines the following macros and declares the following types.
 *    Some are also defined in other headers , as noted in their respective subclauses
 *
 * 2. The types are
 *    -     ptrdiff_t
 *    which is the signed integer type of the result of subtracting two pointers;
 *    -     size_t
 *    which is the unsigned integer type of the result of the sizeof operator
 *    -     max_align_t
 *    which is an object type whose alignment is the greatest fundamental alignment; and
 *    -     wchar_t
 *    which is an integer type whose range of values can represent distinct codes for all members of
 *    the largest extended character set specified among the supported locales; the null character
 *    shall have the code value zero. Each member of the basic character set shall have a code
 *    value equal to its value when used as the lone character in an integer character constant if
 *    an implementation does not define __STDC_MB_MIGHT_NEQ_WC__.
 *
 * 3. The macros are
 *    -     NULL
 *    which expands to an implementation-defined null pointer constant; and
 *    -     offsetof(type, member-designator)
 *    which expands to an integer constant expression that has type size_t, the value of which is
 *    the offset in bytes, to the --structure member-- **subobject** (designated by
 *    member-designator), from the beginning of --its structure (designated by-- **any object of
 *    type** type). The type and member designator shall be such that given
 *    -     static type t;
 *    then the expression &(t.member-designator) evaluates to an address constant. --(--If the
 *    specified **type defines a new type or if the specified** member is a bit-field, the
 *    behavior is undefined.--)--
 *
 *    Recommended practice
 * 4. The types used for size_t and ptrdiff_t should not have an integer conversion rand greater
 *    than that of signed long int unless the implementation supports objects large enough to make
 *    this necessary.
 *
 */

typedef __PTRDIFF_TYPE__ ptrdiff_t;
typedef struct {
    long double __max_align_ld;
    long long int __max_align_ll;
} max_align_t; /* Reversed engineered from msvcrt. */

#ifdef __builtin_offsetof
#define offsetof(type, member) __builtin_offsetof(type, member)
#else
/*
 * To make it more clear:
 * Given an object of type type on address zero,
 * subtract the address of the member from the address of the object itself
 * Graphic example for an object of type type called obj and the member m_m
 * Memory           ______
 *                 | obj |  <-- 0x0
 *                 -------
 *                 |  !  |
 *                 -------
 *                 |  !  |
 *                 -------
 *                 | m_m |  <-- 0x3
 *                 -------
 *                 |  ?  |
 *                 -------
 *                 |__?__|
 *
 *  offsetof = (size_t)(0x3 - 0x0)= (size_t)0x3 = 3
 */
#define offsetof(type, member) \
    ((size_t)((char*)&(((type*)0)->member) - (char*)0))

#endif /* __builtin_offsetof */

/* TODO: Provide a solc implementation too. */
#ifndef __cplusplus
#define unreachable() __builtin_unreachable()
#endif

/* TODO: Unlock the definition when C23 support lands. */
#if (defined(__clusplus) && __cplusplus >= 201103L)
typedef decltype(nullptr) nullptr_t;
#endif
#if (defined (__STDC_VERSION__) && __STDC_VERSION__ > 201710L) && 0
typedef typeof_unqual(nullptr) nullptr_t;
#endif


#endif /* SOLC_STDDEF_H */
