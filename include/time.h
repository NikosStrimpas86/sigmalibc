#ifndef SOLC_TIME_H
#define SOLC_TIME_H

#include <sys/cdefs.h>

#define __STDC_VERSION_TIME_H__ 202111L

__BEGIN_DECLS

#include <internal/defines/null.h>
#define CLOCKS_PER_SEC ((clock_t)1000000)
#define TIME_UTC       1

#include <internal/types/size_t.h>
typedef __UINT32_TYPE__ clock_t;
typedef __INT64_TYPE__ time_t;

struct timespec {
    time_t tv_sec; /* whole seconds -- >= 0 */
    long tv_nsec;  /* Nanoseconds -- [0, 999999999] */
};

struct tm {
    int tm_sec;   /* seconds after the minute -- [0, 60] */
    int tm_min;   /* minutes after the hour -- [0, 59] */
    int tm_hour;  /* hours since midnight -- [0, 23] */
    int tm_mday;  /* day of the month -- [1, 31] */
    int tm_mon;   /* months since January -- [0, 11] */
    int tm_year;  /* years since 1900 */
    int tm_wday;  /* days since Sunday -- [0, 6] */
    int tm_yday;  /* days since January 1 -- [0, 365] */
    int tm_isdst; /* Daylight Saving Time flag -- {-1, 0, 1} */

    /*    long tm_gmtoff;
        const char* tm_zone; */
};

clock_t clock(void);
double difftime(time_t time1, time_t time0);
time_t mktime(struct tm* timeptr);
time_t time(time_t* timer);
int timespec_get(struct timespec* ts, int base);
int timespec_getres(struct timespec* ts, int base);

[[deprecated]] char* asctime(const struct tm* timeptr);
char* asctime_r(const struct tm* timeptr, char* buf);

[[deprecated]] char* ctime(const time_t* timer);
char* ctime_r(const time_t* timer, char* buf);

struct tm* gmtime(const time_t* timer);
struct tm* gmtime_r(const time_t* timer, struct tm* buf);

struct tm* localtime(const time_t* timer);
struct tm* localtime_r(const time_t* timer, struct tm* buf);

size_t strftime(char* restrict s, size_t maxsize, const char* restrict format, const struct tm* restrict timeptr);

__END_DECLS

#endif /* SOLC_TIME_h */
