#ifndef SOLC__CONTAINEROF_H
#define SOLC__CONTAINEROF_H

typedef __UINTPTR_TYPE__ _uintptr;

#ifdef __builtin_offsetof
#define _offsetof(type, member) __builtin_offsetof(type, member)
#else
#define _offsetof(type, member) \
    ((size_t)((char*)&(((type*)0)->member) - (char*)0))
#endif /* __builtin_offsetof */

#define _containerof(ptr, type, name) \
    (type*)((void*)((_uintptr)(ptr) - (_uintptr)_offsetof(type, name)))

#endif /* SOLC__CONTAINEROF_H */
