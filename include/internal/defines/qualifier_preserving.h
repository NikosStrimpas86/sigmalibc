#ifndef SOLC_QUALIFIER_PRESERVING_H
#define SOLC_QUALIFIER_PRESERVING_H

/* clang-format off */
#ifndef __cplusplus
#define _IS_POINTER_CONST(P)      \
    _Generic(1 ? (P) : (void*)(P) \
        , void const*: 1          \
        , default: 0              \
    )

#define _STATIC_IF(P, T, E)           \
    _Generic(&(char [!!(P) + 1]){ 0 } \
        , char(*)[2]: T               \
        , char(*)[1]: E               \
    )

#define _STRING_SEARCH_QP(TYPE, FUNC, BUFFER, ...) \
    _STATIC_IF(                                    \
        _IS_POINTER_CONST((BUFFER))                \
      , (TYPE const*)(FUNC)((BUFFER), __VA_ARGS__) \
      , (TYPE*)(FUNC)((BUFFER), __VA_ARGS__)       \
    )

#define _BINARY_SEARCH_QP(TYPE, FUNC, KEY, BASE, MEMBERS, SIZE, COMPAREFUNC) \
    _STATIC_IF(                                                              \
        _IS_POINTER_CONST((BASE))                                            \
      , (TYPE const*)(FUNC)((KEY), (BASE), (MEMBERS), (SIZE), (COMPAREFUNC)) \
      , (TYPE*)(FUNC)((KEY), (BASE), (MEMBERS), (SIZE), (COMPAREFUNC))       \
    )

#endif
/* clang-format on */

#endif /* SOLC_QUALIFIER_PRESERVING_H */
