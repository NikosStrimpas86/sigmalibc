#ifndef SOLC_MBSTATE_T_H
#define SOLC_MBSTATE_T_H

typedef struct {
    unsigned char _incomplete[4];
    unsigned int _bytes;
    unsigned short _state;
} mbstate_t;

#endif /* SOLC_MBSTATE_T_H */
