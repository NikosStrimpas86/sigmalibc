#ifndef SOLC_SIGNAL_H
#define SOLC_SIGNAL_H

#include <sys/cdefs.h>

__BEGIN_DECLS

/*
 * FIXME: This needs a kernel.
 */
typedef __SIG_ATOMIC_TYPE__ sig_atomic_t;

#define SIG_DFL 0
#define SIG_ERR -1
#define SIG_IGN 1

void (*signal(int sig, void (*func)(int)))(int);

int raise(int sig);

__END_DECLS

#endif /* SOLC_SIGNAL_H */
