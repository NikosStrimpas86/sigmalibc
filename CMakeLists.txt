cmake_minimum_required(VERSION 3.21)
project(SOLC)

set(CMAKE_C_STANDARD 23)

add_compile_definitions(__SOLC__=1)
add_compile_options(-Wall -pedantic -Wextra -Wvla -Walloca)
if (CMAKE_C_COMPILER_ID STREQUAL "GNU")
    add_compile_options(-fanalyzer)
endif ()
add_link_options(-nolibc)

include_directories(include)
set(SOLC_SRC_DIR src)
include_directories(${SOLC_SRC_DIR}/solcmath)



add_library(SOLC SHARED
        ${SOLC_SRC_DIR}/complex/cimag.c
        ${SOLC_SRC_DIR}/complex/conj.c
        ${SOLC_SRC_DIR}/complex/cproj.c
        ${SOLC_SRC_DIR}/complex/creal.c
        ${SOLC_SRC_DIR}/ctype/ctype.c
        ${SOLC_SRC_DIR}/errno/errno.c
        ${SOLC_SRC_DIR}/inttypes/imaxabs.c
        ${SOLC_SRC_DIR}/inttypes/imaxdiv.c
        ${SOLC_SRC_DIR}/locale/localeconv.c
        ${SOLC_SRC_DIR}/locale/setlocale.c
        ${SOLC_SRC_DIR}/math/_fpclassify.c
        ${SOLC_SRC_DIR}/math/_issignaling.c
        ${SOLC_SRC_DIR}/math/_signbit.c
        ${SOLC_SRC_DIR}/math/ceil.c
        ${SOLC_SRC_DIR}/math/copysign.c
        ${SOLC_SRC_DIR}/math/fabs.c
        ${SOLC_SRC_DIR}/math/fadd.c
        ${SOLC_SRC_DIR}/math/fdim.c
        ${SOLC_SRC_DIR}/math/fdiv.c
        ${SOLC_SRC_DIR}/math/ffma.c
        ${SOLC_SRC_DIR}/math/floor.c
        ${SOLC_SRC_DIR}/math/fma.c
        ${SOLC_SRC_DIR}/math/fmax.c
        ${SOLC_SRC_DIR}/math/fmin.c
        ${SOLC_SRC_DIR}/math/fmul.c
        ${SOLC_SRC_DIR}/math/fsub.c
        ${SOLC_SRC_DIR}/math/getpayload.c
        ${SOLC_SRC_DIR}/math/ilogb.c
        ${SOLC_SRC_DIR}/math/llogb.c
        ${SOLC_SRC_DIR}/math/nan.c
        ${SOLC_SRC_DIR}/math/trunc.c
        ${SOLC_SRC_DIR}/search/insremque.c
        ${SOLC_SRC_DIR}/stdbit/stdc_bit_ceil.c
        ${SOLC_SRC_DIR}/stdbit/stdc_bit_floor.c
        ${SOLC_SRC_DIR}/stdbit/stdc_bit_width.c
        ${SOLC_SRC_DIR}/stdbit/stdc_count_ones.c
        ${SOLC_SRC_DIR}/stdbit/stdc_count_zeros.c
        ${SOLC_SRC_DIR}/stdbit/stdc_first_leading_one.c
        ${SOLC_SRC_DIR}/stdbit/stdc_first_leading_zero.c
        ${SOLC_SRC_DIR}/stdbit/stdc_first_trailing_one.c
        ${SOLC_SRC_DIR}/stdbit/stdc_first_trailing_zero.c
        ${SOLC_SRC_DIR}/stdbit/stdc_has_single_bit.c
        ${SOLC_SRC_DIR}/stdbit/stdc_leading_ones.c
        ${SOLC_SRC_DIR}/stdbit/stdc_leading_zeros.c
        ${SOLC_SRC_DIR}/stdbit/stdc_trailing_ones.c
        ${SOLC_SRC_DIR}/stdbit/stdc_trailing_zeros.c
        ${SOLC_SRC_DIR}/stdckdint/stdckdint.c
        ${SOLC_SRC_DIR}/stdlib/abs.c
        ${SOLC_SRC_DIR}/stdlib/bsearch.c
        ${SOLC_SRC_DIR}/stdlib/div.c
        ${SOLC_SRC_DIR}/stdlib/qsort.c
        ${SOLC_SRC_DIR}/stdlib/rand.c
        ${SOLC_SRC_DIR}/string/explicit_bzero.c
        ${SOLC_SRC_DIR}/string/memccpy.c
        ${SOLC_SRC_DIR}/string/memchr.c
        ${SOLC_SRC_DIR}/string/memcmp.c
        ${SOLC_SRC_DIR}/string/memcpy.c
        ${SOLC_SRC_DIR}/string/memfrob.c
        ${SOLC_SRC_DIR}/string/memmove.c
        ${SOLC_SRC_DIR}/string/mempcpy.c
        ${SOLC_SRC_DIR}/string/memrchr.c
        ${SOLC_SRC_DIR}/string/memset.c
        ${SOLC_SRC_DIR}/string/memset_explicit.c
        ${SOLC_SRC_DIR}/string/strcat.c
        ${SOLC_SRC_DIR}/string/strchr.c
        ${SOLC_SRC_DIR}/string/strcmp.c
        ${SOLC_SRC_DIR}/string/strcoll.c
        ${SOLC_SRC_DIR}/string/strcpy.c
        ${SOLC_SRC_DIR}/string/strerror.c
        ${SOLC_SRC_DIR}/string/strlen.c
        ${SOLC_SRC_DIR}/string/strncat.c
        ${SOLC_SRC_DIR}/string/strncpy.c
        ${SOLC_SRC_DIR}/string/strnlen.c
        ${SOLC_SRC_DIR}/string/strrchr.c
        ${SOLC_SRC_DIR}/strings/ffs.c
        ${SOLC_SRC_DIR}/strings/fls.c
        ${SOLC_SRC_DIR}/strings/strcasecmp.c
        ${SOLC_SRC_DIR}/strings/strings.c
        ${SOLC_SRC_DIR}/wchar/btowc.c
        ${SOLC_SRC_DIR}/wchar/mbsinit.c
        ${SOLC_SRC_DIR}/wchar/wcpcpy.c
        ${SOLC_SRC_DIR}/wchar/wcscat.c
        ${SOLC_SRC_DIR}/wchar/wcschr.c
        ${SOLC_SRC_DIR}/wchar/wcscpy.c
        ${SOLC_SRC_DIR}/wchar/wcslen.c
        ${SOLC_SRC_DIR}/wchar/wcsncat.c
        ${SOLC_SRC_DIR}/wchar/wcsncpy.c
        ${SOLC_SRC_DIR}/wchar/wcsnlen.c
        ${SOLC_SRC_DIR}/wchar/wcsrchr.c
        ${SOLC_SRC_DIR}/wchar/wctob.c
        ${SOLC_SRC_DIR}/wchar/wmemchr.c
        ${SOLC_SRC_DIR}/wchar/wmemcmp.c
        ${SOLC_SRC_DIR}/wchar/wmemcpy.c
        ${SOLC_SRC_DIR}/wchar/wmemmove.c
        ${SOLC_SRC_DIR}/wchar/wmempcpy.c
        ${SOLC_SRC_DIR}/wchar/wmemset.c
        ${SOLC_SRC_DIR}/wctype/wctype.c src/math/__iseqsig.c)

